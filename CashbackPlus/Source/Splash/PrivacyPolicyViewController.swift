//
//  PolicyPrivacyViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 05/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

final class PrivacyPolicyViewController: BaseViewController<BaseViewModel> {
    
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var gradientView: GradientView!
    @IBOutlet weak var acceptButton: PrimaryButton!
    @IBOutlet weak var bottomMargin: NSLayoutConstraint!
    
    @IBOutlet weak var versionLabel: UILabel!
    
    var delegate: PrivacyPolicyDelegate?
    var isButtonShowing: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupVersion()
        self.setupConfirmation()
        self.setupText()
    }
    
    private func setupVersion() {
        guard
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            else { return }
        
        self.versionLabel.text = L10n.PrivacyPolicy.version(appVersion)
    }
    
    private func setupConfirmation() {
        if self.isButtonShowing {
            self.gradientView.isHidden = false
            self.acceptButton.isHidden = false
            self.bottomMargin.constant = self.gradientView.bounds.height
        } else {
            self.gradientView.isHidden = true
            self.acceptButton.isHidden = true
            self.bottomMargin.constant = 0
        }
    }
    
    private func setupText() {
        guard let headerFont = StyleConstants.PolicyPrivacy.headerFont else { return }
        
        let attributedString = NSMutableAttributedString(
            string: L10n.license.description.replacingOccurrences(of: Constants.Localizables.headerSeparator, with: "")
        )
        
        for range in L10n.license.description.getRanges(of: Constants.Localizables.headerSeparator) {
            attributedString.addAttribute(
                .font,
                value: headerFont,
                range: range
            )
        }
        
        self.textLabel.attributedText = attributedString
    }
    
    @IBAction func onAcceptAction(_ sender: Any) {
        self.viewModel.storage.hasSeenPrivacyPolicy = true
        self.delegate?.onPrivacyPolicyAccept(self)
    }
    
}
