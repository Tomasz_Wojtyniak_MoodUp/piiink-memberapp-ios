//
//  SplashViewModel.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 03/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay

final class SplashViewModel : BaseViewModel {
    
    private var cachedRegions: Regions
    private var remoteRegions: Regions
    
    lazy var initalizing = BehaviorRelay<Bool>(value: true)
    
    func chooseRegion(_ region: Region) {
        self.storage.store(chosenRegion: region)
    }
    
    enum InitErrors : Error {
        case undefined
    }
    
    func setupApp() -> Completable {
        return self.setDefaultRegion()
    }
    
    private func setDefaultRegion() -> Completable {
        return Completable.create { completable in
            return self.storage.currentRegion
                .flatMap { [weak self] region -> Observable<Region?> in
                    guard let context = self else { return .error(InitErrors.undefined) }
                    
                    if let region = region {
                        return .just(region)
                    } else {
                        return context.remoteRegions.getRegions()
                            .asObservable()
                            .flatMap { [weak self] regions -> Observable<Region?> in
                                self?.storage.store(regions: regions)
                                
                                return .just(regions.first(where: { $0.slug == Constants.Location.initRegionSlug }) ?? regions.first)
                        }
                    }
                }
                .observeOn(MainScheduler.asyncInstance)
                .subscribe(onNext: { [weak self] region in
                    guard let context = self else {
                        completable(.error(InitErrors.undefined))
                        return
                    }
                    
                    if let region = region {
                        context.storage.store(chosenRegion: region)
                    }
                    
                    completable(.completed)
                })
        }
    }
    
    init(storage: Storage, cachedRegions: Regions, remoteRegions: Regions) {
        self.remoteRegions = remoteRegions
        self.cachedRegions = cachedRegions
        
        super.init(storage: storage)
    }
}
