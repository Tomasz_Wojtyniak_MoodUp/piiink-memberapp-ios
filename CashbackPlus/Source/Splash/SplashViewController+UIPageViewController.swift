//
//  SplashViewController+UIPageViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 22/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

extension SplashViewController : UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard
            let slide = viewController as? SlideViewController,
            let slideIndex = self.sections[self.currentSectionIndex].firstIndex(of: slide)
            else { return nil}
        
        let prevIndex = slideIndex - 1
        guard prevIndex >= 0 else { return nil }
        return self.sections[self.currentSectionIndex][prevIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard
            let slide = viewController as? SlideViewController,
            let slideIndex = self.sections[self.currentSectionIndex].firstIndex(of: slide)
            else { return nil}
        
        let nextIndex = slideIndex + 1
        guard nextIndex < self.sections[self.currentSectionIndex].count else { return nil }
        return self.sections[self.currentSectionIndex][nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard
            let slide = pageViewController.viewControllers?.first as? SlideViewController,
            let slideIndex = self.sections[self.currentSectionIndex].firstIndex(of: slide)
            else { return }
        
        self.setupPageControl(sectionIndex: self.currentSectionIndex, slideIndex: slideIndex)
        self.setupSkipButton(sectionIndex: self.currentSectionIndex, slideIndex: slideIndex)
        self.currentSlideIndex = slideIndex
    }
    
}
