//
//  SlideViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 22/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

final class SlideViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    var slide: Slide?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    func setup() {
        guard let slide = self.slide else { return }
        
        self.imageView.image = slide.image
        self.titleLabel.text = slide.title
        self.textLabel.attributedText = slide.description
    }

}
