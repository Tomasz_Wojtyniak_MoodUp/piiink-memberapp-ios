//
//  SplashViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 29/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import RxSwift

struct Slide {
    let image: UIImage
    let title: String
    let description: NSAttributedString
}

//TODO rename to WelcomeViewController
class SplashViewController: BaseViewController<SplashViewModel>, PrivacyPolicyDelegate {

    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var boxView: GenericBoxView!
    @IBOutlet weak var containerView: UIView!
    
    var sections: [[SlideViewController]] = []
    
    var currentSectionIndex = 0
    var currentSlideIndex = 0
    
    private var appSetup = false
    private var finished = false
    
    var pageViewController: UIPageViewController?
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let controller = segue.destination as? UIPageViewController {
            self.pageViewController = controller
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupApp()
        self.setupSections()
        self.setupPageViewController()
        self.initSection(0)
    }
    
    private func setupApp() {
        self.viewModel.setupApp()
            .subscribe(onCompleted: { [weak self] in
                self?.appSetup = true
                self?.finish()
            })
            .disposed(by: self.disposeBag)
    }
    
    private func setupSections() {
        self.sections = [
            [self.createSlide(Splashes.welcome)],
            [
                self.createSlide(Splashes.shoping),
                self.createSlide(Splashes.charity)
            ]
        ]
    }
    
    private func setupPageViewController() {
        self.pageViewController?.dataSource = self
        self.pageViewController?.delegate = self
    }
    
    private func createSlide(_ slide: Slide) -> SlideViewController {
        let controller = self.instantiate(SlideViewController.self)
        controller.slide = slide
        return controller
    }
    
    func setupPageControl(sectionIndex: Int, slideIndex: Int) {
        self.pageControl.numberOfPages = self.sections[sectionIndex].count
        self.pageControl.currentPage = slideIndex
    }
    
    func setupSkipButton(sectionIndex: Int, slideIndex: Int) {
        if self.isLastSlide(sectionIndex: sectionIndex, slideIndex: slideIndex) {
            self.skipButton.isHidden = true
        } else {
            self.skipButton.isHidden = false
        }
    }
    
    private func isLastSlide(sectionIndex: Int, slideIndex: Int) -> Bool {
        return slideIndex + 1 == self.sections[sectionIndex].count
    }
    
    private func isLastSection(_ sectionIndex: Int) -> Bool {
        return sectionIndex + 1 == self.sections.count
    }
    
    private func initSection(_ section: Int) {
        guard let firstSlide = self.sections[section].first else { return }
        self.pageViewController?.setViewControllers([firstSlide], direction: .forward, animated: false, completion: nil)
        self.setupPageControl(sectionIndex: section, slideIndex: 0)
        self.setupSkipButton(sectionIndex: section, slideIndex: 0)
        self.currentSectionIndex = section
        self.currentSlideIndex = 0
    }
    
    func changeSection(_ section: Int) {
        UIView.animate(withDuration: StyleConstants.Splashes.fadeAnimationDuration, animations: {
            self.boxView.alpha = 0
        }, completion: { _ in
            self.initSection(section)

            UIView.animate(withDuration: StyleConstants.Splashes.fadeAnimationDuration) {
                self.boxView.alpha = 1
            }
        })
    }
    
    private func finish() {
        guard self.finished && self.appSetup else { return }
        
        UIView.animate(withDuration: StyleConstants.Splashes.fadeAnimationDuration, animations: {
            self.boxView.alpha = 0
        }, completion: { _ in
            self.showPolicyPrivacy()
        })
    }
    
    private func showPolicyPrivacy() {
        let controller = self.instantiate(PrivacyPolicyViewController.self, storyboard: .main)
        let nav = UINavigationController(navigationBarClass: AppNavigationBar.self, toolbarClass: nil)
        nav.viewControllers = [controller]
        nav.modalPresentationStyle = .fullScreen
        controller.title = L10n.PrivacyPolicy.title
        controller.delegate = self
        self.show(nav, sender: self)
    }
    
    func onPrivacyPolicyAccept(_ controller: PrivacyPolicyViewController) {
        controller.performSegue(withIdentifier: Constants.Segues.privacyPolicyToHome, sender: controller)
    }
    
    func setNextSlide() {
        let nextSlideIndex = self.currentSlideIndex + 1
        let nextSlide = self.sections[self.currentSectionIndex][nextSlideIndex]
        self.pageViewController?.setViewControllers([nextSlide], direction: .forward, animated: true, completion: nil)
        self.setupPageControl(sectionIndex: self.currentSectionIndex, slideIndex: nextSlideIndex)
        self.setupSkipButton(sectionIndex: self.currentSectionIndex, slideIndex: nextSlideIndex)
        self.currentSlideIndex = nextSlideIndex
    }
    
    func setNextSection() {
        if self.isLastSection(self.currentSectionIndex) {
            self.finished = true
            self.finish()
        } else {
            self.changeSection(self.currentSectionIndex + 1)
        }
    }

    @IBAction func onContinueAction(_ sender: Any) {
        if self.isLastSlide(sectionIndex: self.currentSectionIndex, slideIndex: self.currentSlideIndex) {
           self.setNextSection()
        } else {
            self.setNextSlide()
        }
    }
    
    @IBAction func onSkipAction(_ sender: Any) {
        self.setNextSection()
    }
    
}
