//
//  LauncherViewController.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 12/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

final class LaunchViewController: BaseViewController<BaseViewModel> {
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.viewModel.storage.hasSeenPrivacyPolicy {
            self.performSegue(withIdentifier: Constants.Segues.launchToHome, sender: self)
        } else {
            self.performSegue(withIdentifier: Constants.Segues.launchToSplash, sender: self)
        }
    }
}
