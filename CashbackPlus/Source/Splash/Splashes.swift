//
//  Splashes.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 29/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

enum Splashes {
    
    private static func format(_ text: String) -> NSAttributedString {
        guard let boldFont = StyleConstants.Splashes.boldFont else { return NSAttributedString() }
        
        let attributedString = NSMutableAttributedString(
            string: text.replacingOccurrences(of: Constants.Localizables.boldSeparator, with: "")
        )
        
        for range in text.getRanges(of: Constants.Localizables.boldSeparator) {
            attributedString.addAttribute(
                .font,
                value: boldFont,
                range: range
            )
        }
        
        return attributedString
    }
    
    static let welcome = Slide(
        image: Asset.logo.image,
        title: L10n.Splash.Welcome.title,
        description: format(L10n.Splash.Welcome.description)
    )
    
    static let shoping = Slide(
        image: Asset.shoppingIcon.image,
        title: L10n.Splash.Shoping.title,
        description: format(L10n.Splash.Shoping.description)
    )
    
    static let charity = Slide(
        image: Asset.charityIcon.image,
        title: L10n.Splash.Charity.title,
        description: format(L10n.Splash.Charity.description)
    )
    
}
