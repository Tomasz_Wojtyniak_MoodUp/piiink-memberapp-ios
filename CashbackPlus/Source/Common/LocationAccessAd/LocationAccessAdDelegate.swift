//
//  LocationAccessAdDelegate.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 30/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

protocol LocationAccessAdDelegate {
    
    func onLocationAccessAdButtonAction(_ ad: LocationAccessAd)
    
}
