//
//  LocationAccessAd.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 30/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

@IBDesignable
final class LocationAccessAd: UIView {
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var button: PrimaryButton!
    
    var delegate: LocationAccessAdDelegate?
    
    let roundEffect = RoundEffect()
    let shadowEffect = ShadowEffect()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadXIB()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.loadXIB()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.loadXIB()
        self.contentView.prepareForInterfaceBuilder()
    }
    
    private func loadXIB() {
        let bundle = Bundle(for: LocationAccessAd.self)
        bundle.loadNibNamed(Constants.XIBs.locationAccessAd, owner: self, options: nil)
        
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        self.addSubview(self.contentView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundEffect.add(to: self.contentView)
        self.shadowEffect.pathType = .roundRect
        self.shadowEffect.add(to: self.contentView)
    }
    
    @IBAction private func onButtonAction(_ sender: UIButton) {
        self.delegate?.onLocationAccessAdButtonAction(self)
    }
    
}
