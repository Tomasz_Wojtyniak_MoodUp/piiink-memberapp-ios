//
//  HeaderView.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 19/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import UIKit

final class HeaderView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
        self.contentView.prepareForInterfaceBuilder()
    }
    
    private func setup() {
        self.loadXIB()
    }
    
    private func loadXIB() {
        let bundle = Bundle(for: HeaderView.self)
        bundle.loadNibNamed(Constants.XIBs.headerView, owner: self, options: nil)
        
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        self.addSubview(self.contentView)
    }
}
