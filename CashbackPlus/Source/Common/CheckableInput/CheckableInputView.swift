//
//  CheckableInputView.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 09/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

@IBDesignable
final class CheckableInputView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var inputIconImageView: UIImageView!
    @IBOutlet weak var checkmarkIconImageView: UIImageView!
    
    @IBOutlet weak var iconWrapper: UIView!
    
    @IBInspectable
    var placeholder: String? {
        get {
            return self.inputField.placeholder
        }
        set {
            self.inputField.placeholder = newValue
        }
    }
    
    @IBInspectable
    var secureTextEntry: Bool {
        get {
            return self.inputField.isSecureTextEntry
        }
        set {
            self.inputField.isSecureTextEntry = newValue
        }
    }
    
    var correctStatus: Bool? {
        didSet {
            if correctStatus == nil {
                self.checkmarkIconImageView.image = nil
            } else if correctStatus == true {
                self.checkmarkIconImageView.image = Asset.statusIconCorrect.image
            } else {
                self.checkmarkIconImageView.image = Asset.statusIconWrong.image
            }
        }
    }
    
    @IBInspectable
    var icon: UIImage? {
        didSet {
            if let icon = self.icon {
                self.iconWrapper.isHidden = false
                self.inputIconImageView.image = icon
            } else {
                self.iconWrapper.isHidden = true
            }
        }
    }
    
    let roundEffect = RoundEffect()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
        self.contentView.prepareForInterfaceBuilder()
    }
    
    private func setup() {
        self.loadXIB()
        self.setupStyle()
        
        self.roundEffect.add(to: self.contentView)
    }
    
    private func setupStyle() {
        self.contentView.backgroundColor = Asset.blueBackgroundColor.color
        
        self.inputField.attributedPlaceholder = NSAttributedString(
            string: self.placeholder ?? "",
            attributes: [NSAttributedString.Key.foregroundColor: StyleConstants.Inputs.placeholderTextColor]
        )
        
        self.icon = nil
        self.correctStatus = nil
        self.inputField.placeholder = self.placeholder
    }
    
    private func loadXIB() {
        let bundle = Bundle(for: CheckableInputView.self)
        bundle.loadNibNamed(Constants.XIBs.checkableInputView, owner: self, options: nil)
        
        self.contentView.frame = self.frame
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        self.addSubview(self.contentView)
    }
    
    @IBAction func onTapAction(_ sender: Any) {
        if !self.inputField.isFirstResponder {
            self.inputField.becomeFirstResponder()
        }
    }
    
    
}
