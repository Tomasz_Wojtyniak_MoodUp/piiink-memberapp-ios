//
//  ModalViewController+UIViewAnimatedTransitioning.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 26/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

extension ModalViewController: UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return StyleConstants.CardViewModal.animationDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let toView = transitionContext.viewController(forKey: .to)?.view
            else { return }
        
        let presenting = toView == self.view
        
        if presenting {
            self.initialCenterY = self.boxView.center.y
            transitionContext.containerView.addSubview(self.view)
            self.view.backgroundColor = Color.clear
            self.boxView.center = CGPoint(x: self.boxView.center.x, y: self.bottomBorder)
        }
        
        UIView.animate(withDuration: self.transitionDuration(using: transitionContext), animations: {
            if presenting {
                self.boxView.center = CGPoint(x: self.boxView.center.x, y: self.initialCenterY)
                self.view.backgroundColor = Color.black.withAlphaComponent(StyleConstants.Modal.overlayOpacity)
            } else {
                self.boxView.center = CGPoint(x: self.boxView.center.x, y: self.bottomBorder)
                self.view.backgroundColor = Color.clear
            }
        }, completion: { success in
            if !presenting {
                self.view.removeFromSuperview()
            }
            transitionContext.completeTransition(success)
        })
    }
    
}
