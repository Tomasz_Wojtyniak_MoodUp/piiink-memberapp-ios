//
//  ModalViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 23/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

final class ModalViewController: BaseViewController<BaseViewModel> {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var boxView: GenericBoxView!
    
    var initialCenterY = CGFloat()
    var bottomBorder = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bottomBorder = self.view.bounds.maxY + self.view.bounds.height
    }
    
    func setupModal() {
        self.modalPresentationStyle = .custom
        self.definesPresentationContext = true
        self.transitioningDelegate = self
    }
    
    func createView<ViewController: UIViewController>(viewController: ViewController.Type) -> ViewController {
        
        let controller = self.instantiate(viewController)
        self.addChild(controller)
        
        self.containerView.addSubview(controller.view)
        controller.view.frame = self.containerView.frame
        controller.didMove(toParent: self)
        
        return controller
    }
    
    @IBAction func onPanGesture(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.view)
        
        if recognizer.state == .began {
            self.initialCenterY = self.boxView.center.y
        }
        
        self.boxView.center = CGPoint(x: self.boxView.center.x,
                                      y: max(self.initialCenterY + translation.y, self.initialCenterY))
        
        
        let diffToBottom = self.bottomBorder - self.boxView.center.y
        let alpha = StyleConstants.Modal.overlayOpacity * diffToBottom / (self.bottomBorder - self.initialCenterY)
        self.view.backgroundColor = Color.black.withAlphaComponent(alpha)
        
        if recognizer.state == .ended {
            let diff = self.boxView.center.y - self.initialCenterY
            let breakpoint = self.boxView.bounds.height / StyleConstants.CardViewModal.breakpointHeightDivider
            
            if diff > breakpoint {
                self.hideModal()
            } else {
                UIView.animate(withDuration: StyleConstants.CardViewModal.animationDuration, animations: {
                    self.boxView.center = CGPoint(x: self.boxView.center.x, y: self.initialCenterY)
                })
            }
        }
    }
    
    func hideModal() {
        self.dismiss(animated: true)
    }
    
}
