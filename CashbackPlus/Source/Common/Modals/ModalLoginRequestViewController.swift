//
//  LoginRequestModalViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 23/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

final class ModalLoginRequestViewController: UIViewController {
    
    var onLoginHandler: (() -> Void)?
    var onBackHandler: (() -> Void)?
    
    @IBAction func onLoginAction(_ sender: Any) {
        self.onLoginHandler?()
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        self.onBackHandler?()
    }
    
}
