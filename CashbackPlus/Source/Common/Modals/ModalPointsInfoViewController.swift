//
//  ModalPointsInfoViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 26/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

final class ModalPointsInfoViewController: UIViewController {
    
    var onAcceptHandler: (() -> Void)?
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        
        let attributedString = NSMutableAttributedString(
            string: L10n.Modal.MemberInfoPoints.description,
            attributes: [
                .font: StyleConstants.Modal.regularFont as Any,
                .foregroundColor: StyleConstants.Modal.textColor,
                .paragraphStyle: paragraph
            ]
        )
        
        attributedString.addAttribute(.font, value: StyleConstants.Modal.semiboldFont as Any, range: NSRange(location: 32, length: 69))
        attributedString.addAttribute(.font, value: StyleConstants.Modal.semiboldFont as Any, range: NSRange(location: 132, length: 12))
        
        self.descriptionLabel.attributedText = attributedString
    }
    
    @IBAction func onAcceptAction(_ sender: Any) {
        self.onAcceptHandler?()
    }
    
}
