//
//  ModalPrizeInfoViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 09/09/2020.
//  Copyright © 2020 Maciej Olejnik. All rights reserved.
//

import UIKit

final class ModalPrizeInfoViewController: UIViewController {
    
    var onAcceptHandler: (() -> Void)?
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        
        let attributedDesc = NSMutableAttributedString(
            string: L10n.PrizeInfo.desc,
            attributes: [
                .font: StyleConstants.Modal.regularFont as Any,
                .foregroundColor: StyleConstants.Modal.textColor,
                .paragraphStyle: paragraph
            ]
        )
        
        self.titleLabel.text = L10n.PrizeInfo.title
        self.descriptionLabel.attributedText = attributedDesc
    }
    
    @IBAction func onAcceptAction(_ sender: Any) {
        self.onAcceptHandler?()
    }
    
}
