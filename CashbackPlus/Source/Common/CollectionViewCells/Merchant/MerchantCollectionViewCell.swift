//
//  MerchantCollectionViewCell.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 17/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

class MerchantCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var offerBoxView: OfferBox!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func setup(model: Merchant) {
        if let encodedUrl = model.thumbnail.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let url = URL(string: encodedUrl) {
            self.offerBoxView.setImage(url: url)
        } else {
            self.offerBoxView.image = nil
        }
        
        self.offerBoxView.title = model.title
        
        guard let discount = model.discount, !discount.isEmpty else {
            self.offerBoxView.discountText = L10n.Offerbox.discountText("0")
            return
        }
        self.offerBoxView.discountText = L10n.Offerbox.discountText(discount)
    }
}
