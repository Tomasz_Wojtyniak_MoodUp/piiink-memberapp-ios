//
//  CategoryIcon.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 23/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

final class CategoryIcon: UIImageView {
    
    var category: Category.CategoryType? {
        didSet {
            guard let category = self.category else { return }
            self.image = CategoryIcon.getImageAsset(of: category).image
        }
    }
    
    private static func getImageAsset(of: Category.CategoryType) -> ImageAsset {
        switch of {
        case .all:
            return Asset.allIcon
        case .accomodation:
            return Asset.accomodationIcon
        case .activitiesEntertaiment:
            return Asset.activityIcon
        case .automotive:
            return Asset.carIcon
        case .bakery:
            return Asset.bakery
        case .butcher:
            return Asset.butcheryIcon
        case .clothing:
            return Asset.clothesIcon
        case .clubs:
            return Asset.clubsIcon
        case .coffeCafe:
            return Asset.coffeeIcon
        case .dentistHygenist:
            return Asset.dentistIcon
        case .education:
            return Asset.educationIcon
        case .electronics:
            return Asset.electronicsIcon
        case .finance:
            return Asset.financeIcon
        case .foodAndBeverage:
            return Asset.foodIcon
        case .fruitAndVeg:
            return Asset.fruitIcon
        case .hairAndBeauty:
            return Asset.hairdresserIcon
        case .hardware:
            return Asset.hardwareIcon
        case .healthAndFitness:
            return Asset.healthIcon
        case .hire:
            return Asset.hireIcon
        case .houseAndGarden:
            return Asset.houseIcon
        case .it:
            return Asset.itIcon
        case .medical:
            return Asset.medicalIcon
        case .pets:
            return Asset.petsIcon
        case .printing:
            return Asset.printingIcon
        case .pub:
            return Asset.pubsIcon
        case .publicServices:
            return Asset.publicIcon
        case .recreation:
            return Asset.recreationIcon
        case .restaurants:
            return Asset.restaurantIcon
        case .retail:
            return Asset.retailIcon
        case .services:
            return Asset.servicesIcon
        case .signWriting:
            return Asset.signIcon
        case .sports:
            return Asset.sportIcon
        case .takeawayFood:
            return Asset.takeawayIcon
        case .tradies:
            return Asset.tradiesIcon
        case .unknown:
            return Asset.allIcon
        }
    }
}
