//
//  SearchInput.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 24/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

@IBDesignable
final class SearchInput: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var inputField: UITextField!
    
    @IBInspectable
    var placeholder: String? {
        get {
            return self.inputField.placeholder
        }
        set {
            self.inputField.placeholder = newValue
        }
    }
    
    let roundEffect = RoundEffect()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
        self.contentView.prepareForInterfaceBuilder()
    }
    
    private func setup() {
        self.loadXIB()
        self.setupStyle()
        
        self.roundEffect.add(to: self.contentView)
    }
    
    private func setupStyle() {
        self.contentView.backgroundColor = .white
        
        self.inputField.attributedPlaceholder = NSAttributedString(
            string: self.placeholder ?? "",
            attributes: [NSAttributedString.Key.foregroundColor: StyleConstants.Inputs.placeholderTextColor]
        )
    }
    
    private func loadXIB() {
        let bundle = Bundle(for: SearchInput.self)
        bundle.loadNibNamed(Constants.XIBs.searchInput, owner: self, options: nil)
        
        self.contentView.frame = self.frame
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        self.addSubview(self.contentView)
    }

    @IBAction func onTapAction(_ sender: Any) {
        if !self.inputField.isFirstResponder {
            self.inputField.becomeFirstResponder()
        }
    }
}
