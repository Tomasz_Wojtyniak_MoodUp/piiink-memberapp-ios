//
//  OfferBox.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 22/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import SDWebImage

@IBDesignable
final class OfferBox: UIView {
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var discountLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var placeholderImage: UIImageView!
    
    let roundEffect = RoundEffect()
    let shadowEffect = ShadowEffect()
    
    var isPlaceholder = false {
        didSet {
            self.updatePlaceholderView()
        }
    }
    
    var image: UIImage? {
        get {
            return self.imageView.image
        }
        
        set {
            self.imageView.image = newValue
        }
    }
    
    func setImage(url: URL) {
        self.imageView.sd_setImage(with: url)
    }
    
    var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
    
    var discountText: String? {
        get {
            return self.discountLabel.text
        }
        
        set {
            self.discountLabel.text = newValue
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
        self.contentView.prepareForInterfaceBuilder()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.addEffects()
    }
    
    private func setup() {
        self.loadXIB()
    }
    
    private func addEffects() {
        self.roundEffect.add(to: self.contentView)
        self.shadowEffect.pathType = .roundRect
        self.shadowEffect.add(to: self)
    }

    private func loadXIB() {
        let bundle = Bundle(for: OfferBox.self)
        bundle.loadNibNamed(Constants.XIBs.offerBox, owner: self, options: nil)
        
        self.contentView.frame = self.bounds
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        self.addSubview(self.contentView)
    }
    
    private func updatePlaceholderView() {
        if self.isPlaceholder {
            self.discountLabel.text = "..."
            self.titleLabel.text = ""
            self.imageView.image = nil
            self.activityIndicator.startAnimating()
            self.placeholderImage.isHidden = false
        } else {
            self.activityIndicator.stopAnimating()
            self.placeholderImage.isHidden = true
        }
    }
    
}
