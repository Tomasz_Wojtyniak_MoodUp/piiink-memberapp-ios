//
//  BaseViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 18/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import RxSwift

class BaseViewController<VM: BaseViewModel>: UIViewController {

    let disposeBag = DisposeBag()
    var viewModel: VM!
    
    func inject(_ viewModel: VM) {
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationBar()
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.backIndicatorImage =  Asset.chevronLeft.image
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage =  Asset.chevronLeft.image
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func instantiate<ViewController: UIViewController>(_ : ViewController.Type) -> ViewController {
        return self.storyboard!
            .instantiateViewController(
                withIdentifier: ViewController.reuseIdentifier()
            ) as! ViewController
    }
    
    func instantiate<ViewController: UIViewController>(_ : ViewController.Type, storyboard: Constants.Storyboards) -> ViewController {
        
        return UIStoryboard(name: storyboard.rawValue, bundle: nil)
            .instantiateViewController(
                withIdentifier: ViewController.reuseIdentifier()
            ) as! ViewController
    }
    
    func showInModal<ViewController: UIViewController>(
        _ controllerType: ViewController.Type,
        completion: (_ modal: ModalViewController, _ controller: ViewController) -> Void
        ) {
        let modalViewController = self.instantiate(ModalViewController.self)
        modalViewController.setupModal()
        self.present(modalViewController, animated: true)
        completion(modalViewController, modalViewController.createView(viewController: controllerType))
    }

}
