//
//  HomeViewModel.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 15/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

final class HomeViewModel : BaseViewModel {
    
    private let members: Members
        
    func isCardRegistered() -> Single<Bool> {
        return self.members
            .getAuthorizedMember()
            .do(onSuccess: { [weak self] member in
                self?.storage.authorizedMember = member
            })
            .map { member -> Bool in true }
            .catchErrorJustReturn(false)
    }
    
    var isLogged: Bool {
        get {
            return self.storage.auth.value != nil
        }
    }
    
    init(storage: Storage, members: Members) {
        self.members = members
        
        super.init(storage: storage)
    }
}
