//
//  CardNavigationController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 08/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

final class CardNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.modalPresentationStyle = .overFullScreen
        self.modalTransitionStyle = .crossDissolve
        self.view.backgroundColor = .clear
    }

