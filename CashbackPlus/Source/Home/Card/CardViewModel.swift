//
//  CardViewModel.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 12/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

final class CardViewModel : BaseViewModel {
    
    private let auth: Auth
    private let members: Members
    
    func hasCardAdded() -> Bool {
        return self.storage.auth.value != nil
    }
    
    lazy var cardNumber = self.storage.cardNumber
    
    var isHiddenInfo: Bool {
        get {
            return self.storage.isCardInfoHidden
        }
        set {
            self.storage.isCardInfoHidden = newValue
        }
    }
    
    var member: Single<Member> {
        if let authorizedMember = self.storage.authorizedMember {
            return .just(authorizedMember)
        } else {
            return self.members
                .getAuthorizedMember()
                .do(onSuccess: {[weak self] member in
                    self?.storage.authorizedMember = member
                })
        }
    }
    
    func removeCard() -> Completable {
        guard let authData = self.storage.auth.value else { return .never() }
        
        return self.auth
            .logoutCard(auth: authData)
            .do(onCompleted: { [weak self] in
                guard let context = self else { return }
                
                context.storage.authorizedMember = nil
                context.storage.cardNumber = nil
                context.storage.store(auth: nil)
                context.storage.hasSeenCharityScreen = false
            })
    }
    
    init(storage: Storage, auth: Auth, members: Members) {
        self.auth = auth
        self.members = members
        
        super.init(storage: storage)
    }
}
