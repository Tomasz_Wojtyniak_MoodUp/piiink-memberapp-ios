//
//  CardViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 06/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

extension CardViewController: UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return StyleConstants.CardViewModal.animationDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toView = transitionContext.viewController(forKey: .to)?.view else { return }
        
        let presenting = toView == self.view
        
        if presenting {
            self.initialCenterY = self.boxView.center.y
            
            transitionContext.containerView.addSubview(self.view)
            
            self.blurEffectView.alpha = 0
            self.boxView.center = CGPoint(x: self.boxView.center.x, y: self.bottomBorder)
        }
   
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            if presenting {
                self.boxView.center = CGPoint(x: self.boxView.center.x, y: self.initialCenterY)
                self.blurEffectView.alpha = StyleConstants.CardViewModal.maxBlurOpacity
            } else {
                self.boxView.center = CGPoint(x: self.boxView.center.x, y: self.bottomBorder)
                self.blurEffectView.alpha = 0
            }
        }, completion: { (success) in
            if !presenting {
                self.view.removeFromSuperview()
            }

            transitionContext.completeTransition(success)
        })
    }
    
}
