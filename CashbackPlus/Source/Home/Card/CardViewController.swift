//
//  CardViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 06/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import RxSwift
import RxRelay

final class CardViewController: BaseViewController<CardViewModel> {
    
    @IBOutlet weak var blurEffectView: UIVisualEffectView!
    @IBOutlet weak var boxView: GenericBoxView!
    
    @IBOutlet weak var noAddedCardView: UIView!
    @IBOutlet weak var addedCardView: UIView!
    @IBOutlet weak var settingsButton: UIButton!
    
    @IBOutlet weak var qrCodeImageView: UIImageView!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var registerCardButtonWrapper: UIView!
    @IBOutlet weak var userNameWrapper: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var emailWrapper: UIView!
    @IBOutlet weak var emailLabel: UILabel!
    
    lazy var isHiddenCredentials = BehaviorRelay<Bool>(value: self.viewModel.isHiddenInfo)
    
    var initialCenterY = CGFloat()
    var bottomBorder = CGFloat()
    
    var delegate: CardViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }
    
    private func setup() {
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self
        
        self.bottomBorder = self.view.bounds.maxY + self.view.bounds.height
        self.blurEffectView.alpha = StyleConstants.CardViewModal.maxBlurOpacity
        
        self.setupScreens()
    }
    
    private func setupScreens() {
        self.noAddedCardView.isHidden = true
        self.addedCardView.isHidden = true
        
        self.registerCardButtonWrapper.isHidden = true
        self.userNameWrapper.isHidden = true
        self.emailWrapper.isHidden = true
        
        if self.viewModel.hasCardAdded() {
            self.setupInfoScreen()
        } else {
            self.noAddedCardView.isHidden = false
        }
    }
    
    func fillCardNumber(_ cardNumber: String, hidden: Bool) {
        if hidden {
            self.cardNumberLabel.text = CardHelper.formatToHiddenCardNumber(cardNumber)
        } else {
            self.cardNumberLabel.text = CardHelper.formatCardNumber(cardNumber)
        }
    }
    
    func fillMember(_ member: Member) {
        self.userNameLabel.text = "\(member.firstName) \(member.lastName)"
        self.emailLabel.text = member.email
    }
    
    private func setupInfoScreen() {
        guard
            let cardNumber = self.viewModel.cardNumber
            else {
                DispatchQueue.main.async { [weak self] in self?.hideModal() }
                return
            }
        
        self.addedCardView.isHidden = false
        self.setupQRCode(cardNumber)
        
        self.isHiddenCredentials
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] isHidden in
                guard let context = self else { return }
                
                context.fillCardNumber(cardNumber, hidden: isHidden)
                context.viewModel.isHiddenInfo = isHidden
            })
            .disposed(by: self.disposeBag)

       self.viewModel
            .member
            .observeOn(MainScheduler.instance)
            .subscribe(
                // If card is registered
                onSuccess: { [weak self] member in
                    guard let context = self else { return }

                    context.fillMember(member)
                    
                    context.registerCardButtonWrapper.isHidden = true
                    context.userNameWrapper.isHidden = false
                    context.emailWrapper.isHidden = false
                },
                // If card is NOT registered
                onError: { [weak self] error in
                    guard let context = self else { return }
                    
                    context.registerCardButtonWrapper.isHidden = false
                    context.userNameWrapper.isHidden = true
                    context.emailWrapper.isHidden = true
                }
            )
            .disposed(by: self.disposeBag)
    }
    
    private func setupQRCode(_ cardNumber: String) {
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        
        let data = cardNumber.data(using: String.Encoding.ascii)
        qrFilter.setValue(data, forKey: "inputMessage")
        
        if let qrImage = qrFilter.outputImage {
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            let scaledQrImage = qrImage.transformed(by: transform)
            
            self.qrCodeImageView.image = UIImage(ciImage: scaledQrImage)
        }
    }
    
    func hideModal() {
        self.dismiss(animated: true)
    }
    
    func removeCard() {
        self.viewModel
            .removeCard()
            .observeOn(MainScheduler.instance)
            .subscribe(onCompleted: {
                self.hideModal()
            }, onError: { (error) in
                print(error)
            })
            .disposed(by: self.disposeBag)
    }
    
}
