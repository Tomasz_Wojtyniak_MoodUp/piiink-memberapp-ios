//
//  CardViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 06/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

extension CardViewController {
    
    private typealias ActionHandler = () -> Void

    @IBAction func onCardAction(_ sender: Any) {
        self.hideModal()
    }
    
    @IBAction func onPanGesture(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.view)
        
        if recognizer.state == .began {
            self.initialCenterY = self.boxView.center.y
        }
        
        self.boxView.center = CGPoint(x: self.boxView.center.x,
                                      y: max(self.initialCenterY + translation.y, self.initialCenterY))
        
        
        let diffToBottom = self.bottomBorder - self.boxView.center.y
        self.blurEffectView.alpha = StyleConstants.CardViewModal.maxBlurOpacity * diffToBottom / (self.bottomBorder - self.initialCenterY)
        
        if recognizer.state == .ended {
            let diff = self.boxView.center.y - self.initialCenterY
            let breakpoint = self.boxView.bounds.height / StyleConstants.CardViewModal.breakpointHeightDivider
            
            if diff > breakpoint {
                self.hideModal()
            } else {
                UIView.animate(withDuration: StyleConstants.CardViewModal.animationDuration, animations: {
                    self.boxView.center = CGPoint(x: self.boxView.center.x, y: self.initialCenterY)
                })
            }
        }
    }
    
    @IBAction func onAddCardButtonAction(_ sender: Any) {
        self.delegate?.onAddCardButtonAction(self)
    }
    
    private func createMenuAction(title: String, image: UIImage, handler: ActionHandler?) -> UIAlertAction {
        let action = UIAlertAction(title: title, style: .default) { _ in handler?() }
        action.setValue(image, forKey: "image")
        action.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        action.setValue(StyleConstants.SettingsMenu.textColor, forKey: "titleTextColor")
        return action
    }
    
    private func onRemoveCardAction() {
        let alert = UIAlertController(
            title: L10n.Card.Settings.RemoveCard.Alert.title,
            message: L10n.Card.Settings.RemoveCard.Alert.desc,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: L10n.Common.Action.no, style: .cancel))
        alert.addAction(UIAlertAction(title: L10n.Common.Action.yes, style: .default) { [weak self] _ in
            guard let context = self else { return }
            context.removeCard()
        })
        
        self.present(alert, animated: true)
    }
    
    @IBAction func onSettingsAction(_ sender: Any) {
        let menu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        menu.addAction(self.createMenuAction(title: L10n.Card.Settings.removeCard, image: Asset.wasteBin.image, handler: self.onRemoveCardAction))
        
        menu.addAction(self.createMenuAction(title: L10n.Card.Settings.changeCard, image: Asset.change.image) { [weak self] in
            guard let context = self else { return }
            context.delegate?.onUpdateCardButtonAction(context)
        })
        
        if self.viewModel.isHiddenInfo {
            menu.addAction(self.createMenuAction(title: L10n.Card.Settings.showCardCredentails, image: Asset.show.image) { [weak self] in
                guard let context = self else { return }
                context.isHiddenCredentials.accept(false)
            })
        } else {
            menu.addAction(self.createMenuAction(title: L10n.Card.Settings.hideCardCredentails, image: Asset.hide.image) { [weak self] in
                guard let context = self else { return }
                context.isHiddenCredentials.accept(true)
            })
        }
        
        menu.addAction(UIAlertAction(title: L10n.Card.Settings.cancel, style: .cancel))
        
        menu.popoverPresentationController?.sourceView = self.settingsButton

        self.present(menu, animated: true, completion: nil)
    }
    
    @IBAction func onRegisterButtonAction(_ sender: Any) {
        self.delegate?.onRegisterCardButtonAction(self)
    }
    
}
