//
//  CardViewDelegate.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 08/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

protocol CardViewDelegate {
    
    func onAddCardButtonAction(_ controller: CardViewController)
    func onRegisterCardButtonAction(_ controller: CardViewController)
    func onUpdateCardButtonAction(_ controller: CardViewController)
    
}
