//
//  HomeViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 24/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

extension HomeViewController: CardViewDelegate {
    
    func onAddCardButtonAction(_ controller: CardViewController) {
        let addCardController = self.storyboard?
            .instantiateViewController(withIdentifier: AddCardViewController.reuseIdentifier()) as! AddCardViewController
        
        controller.hideModal()
        
        self.show(addCardController, sender: self)
    }
    
    func onRegisterCardButtonAction(_ controller: CardViewController) {
        let registerCardController = self.storyboard?
            .instantiateViewController(withIdentifier: RegistrationViewController.reuseIdentifier()) as! RegistrationViewController
        
        controller.hideModal()
        registerCardController.cardNumber = self.viewModel.storage.cardNumber
        
        self.show(registerCardController, sender: self)
    }
    
    func onUpdateCardButtonAction(_ controller: CardViewController) {
        let updateCardController = self.storyboard?
            .instantiateViewController(withIdentifier: UpdateMembershipViewController.reuseIdentifier()) as! UpdateMembershipViewController
        
        controller.hideModal()
        updateCardController.member = self.viewModel.storage.authorizedMember
        
        self.show(updateCardController, sender: self)
    }
    
}
