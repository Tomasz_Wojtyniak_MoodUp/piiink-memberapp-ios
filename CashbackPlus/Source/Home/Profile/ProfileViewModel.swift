//
//  ProfileViewModel.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 19/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay

final class ProfileViewModel : BaseViewModel {
    let cards: Cards
    let offers: Merchants
    
    var merchants: [Merchant] = []
    
    var hasSeenCharityScreen: Bool {
        return self.storage.hasSeenCharityScreen
    }
    
    var hasSeenProfile: Bool {
        return self.storage.hasSeenProfile
    }
    
    var isLogged: Observable<Bool> {
        return self.storage.auth
            .map { auth -> Bool in auth != nil }
    }
    
    func registerProfileSeen() {
        self.storage.hasSeenProfile = true
    }
    
    private lazy var allOffers = self.offers.getAllOffers()

    func setupBalance(page: Int = 0) -> Single<(Float, Float, [ProfileMerchant])> {
        return Single.zip(
                self.allOffers,
                self.cards.getBalance(page: page)
            )
            .map { (offers, balance) -> (Float, Float, [ProfileMerchant]) in
                self.merchants = offers
                let merchants = balance.pageResult?.content
                    .map { wallet -> (Wallet, Merchant?) in
                        (wallet, offers.first { $0.merchantID == wallet.merchantId } )
                    }
                    .map { (wallet, offer) in
                        ProfileMerchant(
                            merchantId: wallet.merchantId,
                            title: offer?.title ?? L10n.Profile.Points.Unknown.merchant,
                            points: wallet.points,
                            image: .url(URL(string: offer?.thumbnail.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
                        )
                    }
                
                return (Float(balance.pointsSum ?? 0.0), Float(balance.universalPoints ?? 0.0), merchants ?? [])
            }
    }
    
    func fetchBalance(page: Int = 0) -> Single<(Float, Float, [ProfileMerchant])> {
        return self.cards.getBalance(page: page)
            .map { (balance) -> (Float, Float, [ProfileMerchant]) in
                let merchants = balance.pageResult?.content
                    .map { wallet -> (Wallet, Merchant?) in
                        (wallet, self.merchants.first { $0.merchantID == wallet.merchantId } )
                    }
                    .map { (wallet, offer) in
                        ProfileMerchant(
                            merchantId: wallet.merchantId,
                            title: offer?.title ?? L10n.Profile.Points.Unknown.merchant,
                            points: wallet.points,
                            image: .url(URL(string: offer?.thumbnail.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""))
                        )
                    }
                
                return (Float(balance.pointsSum ?? 0.0), Float(balance.universalPoints ?? 0.0), merchants ?? [])
            }
    }

    init(storage: Storage, cards: Cards, offers: Merchants) {
        self.cards = cards
        self.offers = offers
        
        super.init(storage: storage)
    }
    
}

