//
//  ProfileMenuTableCell.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 21/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import UIKit

final class ProfileMenuTableCell : UITableViewCell {
    
    @IBOutlet weak var menuLabel: UILabel!
    
    func setup(text: String) {
        self.menuLabel.text = text
    }
    
}
