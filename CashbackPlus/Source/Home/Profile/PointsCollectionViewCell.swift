//
//  PointsCollectionViewCell.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 21/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import SDWebImage

final class PointsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    
    var numberFormatter = NumberFormatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.numberFormatter.decimalSeparator = "."
        self.numberFormatter.maximumFractionDigits = 2
        self.numberFormatter.minimumIntegerDigits = 1
    }
    
    private var merchant: ProfileMerchant? {
        didSet {
            guard let merchant = self.merchant else { return }
            
            self.logoImageView.image = nil
            
            switch merchant.image {
            case .image(let image):
                self.logoImageView.image = image
            case .url(let url):
                self.logoImageView.sd_setImage(with: url)
            }
            
            self.nameLabel.text = merchant.title
            
            self.pointsLabel.text = "$" + L10n.Profile.Merchant.points(numberFormatter.string(from: NSNumber(value: (merchant.points * 100).rounded(.down) / 100)) ?? "")
        }
    }
    
    func setup(_ merchant: ProfileMerchant) {
        self.merchant = merchant
    }
    
}
