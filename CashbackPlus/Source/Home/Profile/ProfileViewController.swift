//
//  ProfileViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 19/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Crashlytics

final class ProfileViewController: BaseViewController<ProfileViewModel>, CharitySlidesDelegate, UITableViewDelegate {
    
    private let pageSize = 25
        
    @IBOutlet weak var merchantsCollectionView: UICollectionView!
    @IBOutlet weak var menuTable: UITableView!
    @IBOutlet weak var pointsLabel: UILabel!
    
    @IBOutlet weak var noPointsView: UIStackView!
    @IBOutlet weak var withPointsView: UIStackView!
    @IBOutlet weak var loadingView: UIStackView!
    @IBOutlet weak var animation: UIImageView!
    @IBOutlet weak var backgroundPoints: UIImageView!
    
    private let refreshControl = UIRefreshControl()
    
    typealias SelectHandler = () -> Void
    typealias Option = (String, SelectHandler?)
    
    private var options: [Option] = []
    
    private var merchantSubscription: Disposable?
    private var pointsSubscription: Disposable?
    
    @IBOutlet weak var pointsInfoDescriptionLabel: UILabel!
    
    private var page = 0
    private var isLogged: Bool = false
    private var merchants = BehaviorRelay<[ProfileMerchant]>(value: [])
    private var loadingStatus = BehaviorRelay<Bool>(value: false)
    private var universalPoints: Float = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupOptions()
        self.setup()
    }
    
    private func setupOptions() {
        self.options = [
            (L10n.Profile.Options.recommend, self.onRecommendMerchantAction),
            (L10n.Profile.Options.charity, self.onCharityAction),
            (L10n.Profile.Options.privacyPolicy, self.onPrivacyPolicyAction),
            (L10n.Profile.Options.libraries, self.onLibrariesAction)
        ]
        
        self.menuTable.rx.modelSelected(Option.self)
            .subscribe(onNext: { (_, callback) in
                guard let callback = callback else { return }
                callback()
            })
            .disposed(by: self.disposeBag)
        
        Observable.just(self.options)
            .do(afterNext: { _ in
                self.menuTable.beginUpdates()
                self.menuTable.endUpdates()
            })
            .bind(to: self.menuTable.rx.items(cellIdentifier: ProfileMenuTableCell.reuseIdentifier(), cellType: ProfileMenuTableCell.self)) {
                _, option, cell in
                let (text, _) = option
                
                cell.setup(text: text)
            }
            .disposed(by: self.disposeBag)
    }
    
    private func loadBalance(page: Int, clear: Bool = false) {
        let balance = clear ?
            self.viewModel.setupBalance(page: page) :
            self.viewModel.fetchBalance(page: page)
        
        balance
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { [weak self] (totalPoints, universalPoints, merchants) in
                guard let context = self else { return }
                context.refreshControl.endRefreshing()
                
                context.universalPoints = universalPoints
                context.merchants.accept((clear ? [] : context.merchants.value) + merchants)
                
                context.page += 1
                
                if totalPoints > 0 {
                    context.setupForLoggedUserWithPoints(totalPoints)
                } else {
                    context.setupForLoggedUserWithNoPoints()
                }
                if (merchants.count == context.pageSize) {
                    context.loadBalanceNextPage()
                }
                }, onError: { (error) in
                    Crashlytics
                        .sharedInstance()
                        .recordError(
                            error,
                            withAdditionalUserInfo: [
                                Constants.Keys.view: ProfileViewController.reuseIdentifier(),
                                Constants.Keys.cardNumber: self.viewModel.storage.cardNumber ?? ""
                            ]
                        )
            })
            .disposed(by: self.disposeBag)
    }
    
    private func loadBalanceNextPage() {
        self.loadBalance(page: self.page, clear: false)
    }
    
    @objc private func refreshProfile(_ sender: Any) {
        if self.isLogged {
            self.page = 0
            self.loadBalance(page: 0, clear: true)
        } else {
            self.setupForNotLoggedUser()
            self.refreshControl.endRefreshing()
        }
    }
    
    func setup() {
        self.menuTable.refreshControl = self.refreshControl
        self.refreshControl.addTarget(self, action: #selector(refreshProfile(_:)), for: .valueChanged)
        
        self.viewModel.isLogged
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] isLogged in
                guard let context = self else { return }
                context.showHeaderLoading()
                context.isLogged = isLogged
                
                if isLogged {
                    context.page = 0
                    context.loadBalance(page: 0, clear: true)
                } else {
                    context.setupForNotLoggedUser()
                }
            })
            .disposed(by: self.disposeBag)
        
        
        self.merchants
            .observeOn(MainScheduler.instance)
            .map { [weak self] merchants -> [ProfileMerchant] in
                guard let context = self else { return [] }
                return [context.getUniversalMerchant(points: context.universalPoints)] + merchants
            }
            .bind(to: self.merchantsCollectionView.rx.items(cellIdentifier: PointsCollectionViewCell.reuseIdentifier(), cellType: PointsCollectionViewCell.self)) {
                _, model, cell in
                cell.setup(model)
            }
            .disposed(by: self.disposeBag)
        
        animation.image = nil
    }
    
    private func getUniversalMerchant(points: Float) -> ProfileMerchant {
        return ProfileMerchant(
            merchantId: nil,
            title: L10n.Profile.Merchant.universalPoints,
            points: points,
            image: .image(Asset.universalPoints.image)
        )
    }
    
    private func setupForNotLoggedUser() {
        self.showHeaderWithInfo()
        self.pointsInfoDescriptionLabel.text = L10n.Profile.PointsInfo.Description.noLogged
    }

    private func setupForLoggedUserWithNoPoints() {
        self.showHeaderWithInfo()
        self.pointsInfoDescriptionLabel.text = L10n.Profile.PointsInfo.Description.noPoints
    }
    
    private func setupForLoggedUserWithPoints(_ points: Float) {
        self.showHeaderWithPoints()
        if let points = self.format(points) {
            self.pointsLabel.text = "$" + points
        }
    }
    
    private func format(_ points: Float) -> String? {
        let numberFormatter = NumberFormatter()
        numberFormatter.decimalSeparator = "."
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimumIntegerDigits = 1
        return numberFormatter.string(from: NSNumber(value: (points * 100).rounded(.down) / 100))
    }
    
    private func showHeaderLoading() {
        self.noPointsView.isHidden = true
        self.withPointsView.isHidden = true
        self.loadingView.isHidden = false
        self.menuTable.tableHeaderView?.frame = self.loadingView.frame
        self.menuTable.tableHeaderView?.layoutIfNeeded()
        self.menuTable.tableHeaderView = self.menuTable.tableHeaderView
    }
    
    private func showHeaderWithPoints() {
        self.noPointsView.isHidden = true
        self.withPointsView.isHidden = false
        self.loadingStatus.accept(true)
        self.loadingView.isHidden = true
        self.menuTable.tableHeaderView?.frame = self.withPointsView.frame
        self.menuTable.tableHeaderView?.layoutIfNeeded()
        self.menuTable.tableHeaderView = self.menuTable.tableHeaderView
    }
    
    private func showHeaderWithInfo() {
        self.noPointsView.isHidden = false
        self.withPointsView.isHidden = true
        self.loadingView.isHidden = true
        self.menuTable.tableHeaderView?.frame = self.noPointsView.frame
        self.menuTable.tableHeaderView?.layoutIfNeeded()
        self.menuTable.tableHeaderView = self.menuTable.tableHeaderView
    }
    
    // MARK: Actions
    
    private func showCharity() {
        let controller = self.instantiate(CharityViewController.self)
        self.show(controller, sender: self)
    }
    
    private func showCharitySlides() {
        let controller = self.instantiate(CharitySlidesViewController.self)
        controller.isShowingContinueButtonOnEnd = true
        controller.delegate = self
        self.show(controller, sender: self)
    }
    
    func onCharitySlidesEnd(_ controller: CharitySlidesViewController) {
        self.navigationController?.popViewController(animated: false)
        self.showCharity()
    }
    
    func onRegisterButtonAction(_ controller: CharitySlidesViewController) {
        self.navigationController?.popViewController(animated: false)
        
        let registerController = self.instantiate(RegistrationViewController.self)
        registerController.cardNumber = viewModel.storage.cardNumber
        self.show(registerController, sender: self)
    }
    
    private func showModalLoginRequest() {
        self.showInModal(ModalLoginRequestViewController.self) { [weak self] (modal, controller) in
            controller.onBackHandler = {
                modal.dismiss(animated: true)
            }
            
            controller.onLoginHandler = { [weak self] in
                guard let context = self else { return }
                modal.dismiss(animated: true)
                let addCardController = context.instantiate(AddCardViewController.self)
                context.show(addCardController, sender: context)
            }
        }
    }
    
    @objc private func showRecommendMerchant() {
        let controller = self.instantiate(RecommendViewController.self)
        self.show(controller, sender: self)
    }
    
    private func onRecommendMerchantAction() {
        if self.isLogged {
            self.showRecommendMerchant()
        } else {
            self.showModalLoginRequest()
        }
    }
    
    private func onCharityAction() {
        if self.isLogged {
            if self.viewModel.hasSeenCharityScreen {
                self.showCharity()
            } else {
                // Scenario when card is already registered (outside of the app)
                self.showCharitySlides()
            }
        } else {
            self.showModalLoginRequest()
        }
    }
    
    private func onLibrariesAction() {
        guard let file = Configuration.shared.config.librariesFileName else { return }
        let viewController = LibrariesViewController(fileNamed: file)
        viewController.title = L10n.Profile.Options.libraries
        self.show(viewController, sender: self)
    }
    
    private func onPrivacyPolicyAction() {
        let controller = self.instantiate(PrivacyPolicyViewController.self, storyboard: .main)
        controller.title = L10n.PrivacyPolicy.title
        controller.isButtonShowing = false
        self.show(controller, sender: self)
    }
    
    @IBAction func onMemberInfoAction(_ sender: Any) {
        self.showInModal(ModalPointsInfoViewController.self) { (modal, controller) in
            controller.onAcceptHandler = {
                modal.dismiss(animated: true)
            }
        }
    }
    
    private func startAnimation() {
        let duration = 6.0
        
        self.menuTable.isScrollEnabled = false
        self.menuTable.scrollRectToVisible(CGRect(x: 0, y: 0, width: 1, height: 1), animated: false)
        self.backgroundPoints.image = Asset.confetti.image
        self.animation.alpha = 1.0
        
        let animationImage = UIImage.animatedImageNamed("Balloons/balloons-", duration: duration);
        animation.image = animationImage
        
        DispatchQueue.main.asyncAfter(deadline: .now() + duration - 3.0) { [weak self] in
            guard let context = self else { return }
            
            let duration = 1.5
            
            UIView.transition(
                with: context.backgroundPoints,
                duration: duration,
                options: .transitionCrossDissolve,
                animations: {
                    self?.backgroundPoints.image = Asset.balloons.image
                },
                completion: nil
            )
            
            UIView.transition(
                with: context.animation,
                duration: duration,
                options: .transitionCrossDissolve,
                animations: {
                    context.animation.alpha = 0.0
                },
                completion: { _ in
                    self?.animation.image = nil
                }
            )
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + duration - 2.0) { [weak self] in
            self?.menuTable.isScrollEnabled = true
        }
    }
    
    public func onProfileShow() {
        if !self.viewModel.hasSeenProfile {
            self.backgroundPoints.image = Asset.confetti.image
            
            // Waiting for box rendering
            self.loadingStatus
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { [weak self] status in
                    if status == true {
                        self?.startAnimation()
                    }
                })
                .disposed(by: self.disposeBag)
            
            self.viewModel.registerProfileSeen()
        }
    }
    
}
