//
//  LibraryViewController.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 26/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import AcknowList

final class LibraryViewController: AcknowViewController {
    
    var topView: UIView = UIView()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addTopView()
    }
    
    private func addTopView() {
        guard let bar = self.navigationController?.navigationBar,
            let inset = self.navigationController?.view.safeAreaInsets.top else { return }
        topView.backgroundColor = .white
        topView.frame = CGRect(
            x: 0,
            y: 0,
            width: bar.frame.width,
            height: bar.frame.height + inset)
        self.navigationController?.view.insertSubview(topView, belowSubview: bar)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.topView.removeFromSuperview()
    }
}
