//
//  ProfileMerchant.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 21/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import UIKit

struct ProfileMerchant {
    enum ImageType {
        case url(URL?)
        case image(UIImage)
    }
    
    let merchantId: Int?
    let title: String?
    let points: Float
    let image: ImageType
}
