//
//  MerchantsViewController.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 13/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

enum MerchantsSections: Int, CaseIterable {
    case bestOffers
    case nearby
    case newMerchents
    case others
}

final class MerchantsViewController: BaseViewController<MerchantsViewModel> {
    
    private let animationTime: TimeInterval = 0.25
    private let velocityPrescaler: CGFloat = 5
    private let selectedAlpha: CGFloat = 0.3
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchClickArea: UIView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var viewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerContainer: UIView!
    
    let headerHeight: CGFloat = 56
    
    private var locationService: LocationService?
    
    var cellHeights: [IndexPath : CGFloat] = [:]
    var startPosition: CGFloat = 0.0
    var startOffset: CGPoint = CGPoint(x: 0, y: 0)
    
    func inject(_ viewModel: MerchantsViewModel, locationService: LocationService) {
        self.viewModel = viewModel
        self.locationService = locationService
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        self.getData()
        self.setupListeners()
        self.setupCategoriesCollectionView()
        self.setupHeader()
        self.addCoverViewFromTop()
    }
    
    private func addCoverViewFromTop() {
        let coverView = UIView(frame:
            CGRect(
                x: 0,
                y: -(UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0),
                width: UIScreen.main.bounds.width,
                height: UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
            )
        )
        coverView.backgroundColor = self.headerContainer.backgroundColor
        
        self.view.addSubview(coverView)
    }
    
    private func setupHeader() {
        let shadowEffect = ShadowEffect()
        shadowEffect.add(to: self.headerContainer)
        self.headerContainer.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(self.onHeaderPan(_:))))
        
        self.searchClickArea.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTap(_:))))
        
        self.locationButton.setTitle(self.viewModel.currentRegion?.name ?? "", for: .normal)
    }
        
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        self.showSearch()
    }
    
    @objc func onHeaderPan(_ recognizer: UIPanGestureRecognizer) {
        guard let recognizerView = recognizer.view else {return}
        let translation = recognizer.translation(in: recognizerView.superview)
        if recognizer.state == .began {
            self.startPosition = self.viewTopConstraint.constant
            self.startOffset = self.tableView.contentOffset
        } else if recognizer.state == .changed {
            // Move header
            var value = self.startPosition + translation.y
            if value > 0 {
                value = 0
            } else if value <= -self.headerContainer.frame.height {
                value = -self.headerContainer.frame.height
            } else {
                self.tableView.setContentOffset(self.startOffset, animated: false)
            }
            self.viewTopConstraint.constant = value
        } else if recognizer.state == .ended {
            // Animate ending - slowly decelerate view
            let velocity = recognizer.velocity(in: recognizerView.superview)
            if velocity.y != 0 {
                var value = self.viewTopConstraint.constant + (velocity.y / self.velocityPrescaler)
                if value > 0 {
                    value = 0
                } else if value < -self.headerContainer.frame.height {
                    value = -self.headerContainer.frame.height
                }
                UIView.animate(
                    withDuration: self.animationTime,
                    delay: 0,
                    options: .curveEaseOut,
                    animations: {
                        self.viewTopConstraint.constant = value
                        self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }
    
    private func setupCategoriesCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.registerCollectionViewCells()
    }
    
    private func registerCollectionViewCells() {
        self.collectionView.register(
            CategoryCollectionViewCell.nib(),
            forCellWithReuseIdentifier: CategoryCollectionViewCell.nibName()
        )
    }
    
    private func setupListeners() {
        self.viewModel
            .storage
            .currentRegion
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] region in
                guard let context = self else {
                    return
                }
                context.viewModel.currentRegion = region
                context.viewModel.refreshOtherMerchants()
                context.tableView.reloadData()
                context.locationButton.setTitle(region?.name, for: .normal)
            })
            .disposed(by: self.disposeBag)
    }
    
    private func getData() {
        Completable
            .zip(
                [self.viewModel.getCategories(),
                 self.viewModel.getBestOffers(),
                 self.viewModel.getNewMerchants(),
                 self.viewModel.getAllMerchants()]
            )
            .observeOn(MainScheduler.instance)
            .subscribe(onCompleted: {
                self.viewModel.isMerchantsInit = false
                self.collectionView.reloadData()
                self.tableView.reloadData()
            }) { (error) in
                self.tableView.reloadData()
            }.disposed(by: self.disposeBag)
        
        self.setupNearbyMerchants()
        self.setupOtherMerchants()
    }
    
    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 40.0
        self.tableView.rowHeight = UITableView.automaticDimension
        
        self.registerTableViewCells()
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onHeaderPan(_:)))
        gesture.delegate = self
        self.tableView.addGestureRecognizer(gesture)
    }
    
    private func registerTableViewCells() {
        
        self.tableView.register(
            BestOffersTableViewCell.nib(),
            forCellReuseIdentifier: BestOffersTableViewCell.nibName()
        )
        
        self.tableView.register(
            MerchantsRowTableViewCell.nib(),
            forCellReuseIdentifier: MerchantsRowTableViewCell.nibName()
        )
        
        self.tableView.register(
            LocationAdTableViewCell.nib(),
            forCellReuseIdentifier: LocationAdTableViewCell.nibName()
        )
        
        self.tableView.register(
            NoItemsTableViewCell.nib(),
            forCellReuseIdentifier: NoItemsTableViewCell.nibName()
        )
    }
    
    private func setupNearbyMerchants() {
        self.subscribeNearbyOffers()
        self.locationService?
            .access
            .catchErrorJustReturn(false)
            .subscribe(onNext: { [weak self] status in
                guard let context = self else { return }
                
                context.viewModel.isLocationAccess = status
                if status {
                    context.locationService?.refreshLocation()
                }
                context.tableView.reloadData()
            })
            .disposed(by: self.disposeBag)
    }
    
    private func subscribeNearbyOffers() {
        self.viewModel
            .nearbyOffers
            .observeOn(MainScheduler.instance)
            .catchErrorJustReturn([])
            .subscribe(onNext: { [weak self] (nearbyMerchants) in
                guard let context = self else { return }
                
                context.viewModel.isNearbyInit = false
                context.viewModel.refreshOtherMerchants()
                context.tableView.reloadData()
                })
            .disposed(by: self.disposeBag)
    }
    
    private func setupOtherMerchants() {
        self.viewModel
            .otherMerchantsRelay
            .observeOn(MainScheduler.instance)
            .catchErrorJustReturn([])
            .subscribe {  [weak self]  (_) in
                guard let context = self else { return }
                
                context.tableView.reloadData()
            }
            .disposed(by: self.disposeBag)
    }
    
    func showMerchantDetails(_ merchant: Merchant) {
        guard let merchantDetailsViewController = UIStoryboard(name: "MerchantDetails", bundle: nil)
            .instantiateInitialViewController() as? MerchantDetailsViewController else { return }
        
        merchantDetailsViewController.viewModel.merchant = merchant
        
        self.show(merchantDetailsViewController, sender: self)
    }
    
    func showRegions() {
        guard let regionsViewController = UIStoryboard(name: "Regions", bundle: nil)
            .instantiateInitialViewController() as? RegionsViewController else { return }
        
        
        self.show(regionsViewController, sender: self)
    }
    
    func showSearch() {
        guard let searchViewController = UIStoryboard(name: "Search", bundle: nil)
            .instantiateInitialViewController() as? SearchViewController else { return }
        
        searchViewController.viewModel.isLocationAccess = self.viewModel.isLocationAccess
        var merchants = self.viewModel.getNearbyMerchants()
        if merchants.count >= Constants.Location.maxNearbyItems {
            merchants = Array(merchants[0 ..< Constants.Location.maxNearbyItems])
        }
        searchViewController.viewModel.setNearbyItems(merchants)
        
        self.show(searchViewController, sender: self)
    }
    
    @IBAction func onLocationClick(_ sender: Any) {
        self.showRegions()
    }
}

extension MerchantsViewController : LocationAccessAdDelegate {
    
    func onLocationAccessAdButtonAction(_ ad: LocationAccessAd) {
        self.locationService?.requestAccess()
    }
    
}

extension MerchantsViewController: MerchantDelegate {
    
    func onMerchantSelected(_ merchant: Merchant) {
        self.showMerchantDetails(merchant)
    }
    
}

extension MerchantsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: CategoryCollectionViewCell.nibName(),
            for: indexPath) as? CategoryCollectionViewCell else { return UICollectionViewCell() }
        
        cell.setup(self.viewModel.categories[indexPath.item])
        
        if (indexPath.item == self.viewModel.currentCategoryIndex) {
            cell.categoryImageView.alpha = self.selectedAlpha
            cell.titleLabel.alpha = self.selectedAlpha
        } else {
            cell.categoryImageView.alpha = 1.0
            cell.titleLabel.alpha = 1.0
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewModel.currentCategoryIndex = indexPath.item
        self.viewModel.refreshOtherMerchants()
        self.tableView.reloadData()
        self.collectionView.reloadData()
    }
}

extension MerchantsViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
