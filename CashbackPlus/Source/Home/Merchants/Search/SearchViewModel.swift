//
//  SearchViewModel.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 24/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

final class SearchViewModel: BaseViewModel {
    
    let defaultCellCount = 1
    
    private var searchResults: [Merchant] = []
    private var nearbyMerchants: [Merchant] = []
    var searchedPhrase = ""
    
    lazy var isLocationAccess = false
    
    private let merchants: Merchants
    private let locationProvider: LocationProvider
    
    lazy var nearbyOffers =  self.locationProvider
        .getLocation()
        .flatMap { location in self.merchants.getNearbyOffers(location) }
        .flatMap { (merchant) -> Single<[Merchant]> in
            var sortedMerchants = merchant
            sortedMerchants.sort(by: { ($0.distance ?? 0.0) < ($1.distance ?? 0.0) })
            if sortedMerchants.count >= Constants.Location.maxNearbyItems {
                sortedMerchants = Array(sortedMerchants[0 ..< Constants.Location.maxNearbyItems])
            }
            self.nearbyMerchants = sortedMerchants
            return Single.just(sortedMerchants)
        }
    
    init(storage: Storage, merchants: Merchants, locationProvider: LocationProvider) {
        self.merchants = merchants
        self.locationProvider = locationProvider
        super.init(storage: storage)
    }
    
    func getNearbyItemsCount() -> Int {
        if !self.searchedPhrase.isEmpty {
            return 0
        } else {
            if self.isLocationAccess {
                return self.nearbyMerchants.count
            } else {
                return self.defaultCellCount
            }
        }
    }
    
    func getSearchItemsCount() -> Int {
        return self.searchedPhrase.isEmpty ? 0 : self.searchResults.count
    }
    
    func searchMerchants(_ query: String) -> Observable<[Merchant]> {
        self.searchedPhrase = query
        return self.merchants.search(query: query)
            .flatMap({ (merchants) -> Single<[Merchant]> in
                self.searchResults = merchants
                return Single.just(merchants)
            })
            .asObservable()
    }
    
    func setNearbyItems(_ nearbyMerchants: [Merchant]) {
        self.nearbyMerchants = nearbyMerchants
    }
    
    func getNearbyItem(_ index: Int) -> Merchant {
        return self.nearbyMerchants[index]
    }
    
    func getSearchedItem(_ index: Int) -> Merchant {
        return self.searchResults[index]
    }
}
