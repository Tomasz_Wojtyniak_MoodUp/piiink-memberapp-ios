//
//  SearchResultTableViewCell.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 25/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {

    @IBOutlet weak var merchantLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var distanceContainer: UIStackView!
    
    let roundedEffect = RoundEffect()
    let shadowEffect = ShadowEffect()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundedEffect.add(to: self.containerView)
        self.shadowEffect.pathType = .roundRect
        self.shadowEffect.add(to: self.containerView)
    }
}
