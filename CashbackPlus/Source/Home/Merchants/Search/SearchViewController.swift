//
//  SearchViewController.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 24/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

enum SearchSections: Int, CaseIterable {
    case nearby
    case results
}

protocol SearchDelegate: AnyObject {
    func onLocationEnabled()
    func onNearbyMerchants(_ merchants: [Merchant])
}

final class SearchViewController: BaseViewController<SearchViewModel> {
    
    let headerHeight: CGFloat = 56
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchInput: SearchInput!
    @IBOutlet weak var searchContainer: UIView!
    @IBOutlet weak var noResultsContainer: UIStackView!
    @IBOutlet weak var noResultsLabel: UILabel!
    @IBOutlet weak var noResultsButton: PrimaryButton!
    
    let shadowEffect = ShadowEffect()
    
    weak var delegate: SearchDelegate?
    
    private var locationService: LocationService?
    
    func inject(_ viewModel: SearchViewModel, locationService: LocationService) {
        self.viewModel = viewModel
        self.locationService = locationService
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTranslations()
        self.shadowEffect.add(to: self.searchContainer)
        self.setupTableView()
        self.setupSearchInput()
    }
    
    private func setupSearchInput() {
        self.searchInput.inputField.rx.value
            .debounce(RxTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .catchErrorJustReturn(nil)
            .flatMap({ [weak self] (text) -> Observable<[Merchant]> in
                guard let context = self else { return Observable.just([]) }
                context.tableView.reloadData()
                return context.viewModel.searchMerchants(text ?? "")
            })
            .observeOn(MainScheduler.instance)
            .catchErrorJustReturn([])
            .subscribe(onNext: { [weak self]  (merchants) in
                guard let context = self else { return }
                if merchants.isEmpty && context.viewModel.getSearchItemsCount() == 0 && !context.viewModel.searchedPhrase.isEmpty {
                    context.noResultsLabel.text = L10n.Search.notFound(context.viewModel.searchedPhrase)
                    context.noResultsContainer.isHidden = false
                } else {
                    context.noResultsContainer.isHidden = true
                }
                context.tableView.reloadData()
            })
            .disposed(by: self.disposeBag)
    }
    
    private func setupNearbyMerchants() {
        self.subscribeNearbyOffers()
        self.locationService?
            .access
            .catchErrorJustReturn(false)
            .subscribe(onNext: { [weak self] status in
                guard let context = self else { return }
                
                context.viewModel.isLocationAccess = status
                if status {
                    context.locationService?.refreshLocation()
                }
                context.delegate?.onLocationEnabled()
                context.tableView.reloadData()
            })
            .disposed(by: self.disposeBag)
    }
    
    private func subscribeNearbyOffers() {
        self.viewModel
            .nearbyOffers
            .observeOn(MainScheduler.instance)
            .catchErrorJustReturn([])
            .subscribe(onNext: { [weak self] (nearbyMerchants) in
                guard let context = self else { return }
                context.delegate?.onNearbyMerchants(nearbyMerchants)
                context.tableView.reloadData()
            })
            .disposed(by: self.disposeBag)
    }
    
    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        
        self.registerTableViewCells()
    }
    
    private func registerTableViewCells() {
        self.tableView.register(
            LocationAdTableViewCell.nib(),
            forCellReuseIdentifier: LocationAdTableViewCell.nibName()
        )
        
        self.tableView.register(
            SearchResultTableViewCell.nib(),
            forCellReuseIdentifier: SearchResultTableViewCell.nibName()
        )
    }
    
    private func setupTranslations() {
        self.searchInput.inputField.placeholder = L10n.Search.hint
        self.noResultsLabel.text = L10n.Search.notFound("")
        self.noResultsButton.setTitle(L10n.Search.back, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func showMerchantDetails(_ merchant: Merchant) {
        guard let merchantDetailsViewController = UIStoryboard(name: "MerchantDetails", bundle: nil)
            .instantiateInitialViewController() as? MerchantDetailsViewController else { return }
        
        merchantDetailsViewController.viewModel.merchant = merchant
        
        self.show(merchantDetailsViewController, sender: self)
    }
    
    @IBAction func onCancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return SearchSections.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = SearchSections(rawValue: section) else { return 0 }
        
        switch section {
        case .nearby:
            return self.viewModel.getNearbyItemsCount()
        case .results:
            return self.viewModel.getSearchItemsCount()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = SearchSections(rawValue: indexPath.section) else { return UITableViewCell() }
        
        switch section {
        case .nearby:
            if !self.viewModel.isLocationAccess {
                return self.getRequestLocationCell(indexPath)
            } else {
                return self.setupResultCell(indexPath, isNearby: true)
            }
            
        case .results:
            return self.setupResultCell(indexPath, isNearby: false)
            
        }
    }
    
    private func setupResultCell(_ indexPath: IndexPath, isNearby: Bool) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(
            withIdentifier: SearchResultTableViewCell.nibName(),
            for: indexPath) as? SearchResultTableViewCell else { return UITableViewCell() }
        
        let model = isNearby ? self.viewModel.getNearbyItem(indexPath.row) : self.viewModel.getSearchedItem(indexPath.row)
        
        cell.merchantLabel.text = model.title
        cell.distanceContainer.isHidden = !isNearby
        cell.discountLabel.text = L10n.Offerbox.discountText(model.discount ?? "")
        cell.distanceLabel.text = String(format: "%.1f km", model.distance ?? 0.0)
        
        return cell
    }
    
    private func getRequestLocationCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(
            withIdentifier: LocationAdTableViewCell.nibName(),
            for: indexPath) as? LocationAdTableViewCell else { return UITableViewCell() }
        
        cell.locationAdView.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let section = SearchSections(rawValue: section) else { return nil }
        
        let headerView: HeaderView? = HeaderView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.headerHeight))
        
        switch section {
        case .nearby:
            headerView?.titleLabel.text = L10n.Merchants.nearBy
        case .results:
            headerView?.titleLabel.text = L10n.Search.resultsHeader
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let section = SearchSections(rawValue: section) else { return 0 }
        
        switch section {
        case .nearby:
            return self.viewModel.getNearbyItemsCount() != 0 ? self.headerHeight : CGFloat.leastNonzeroMagnitude
        case .results:
            return self.viewModel.getSearchItemsCount() != 0 ? self.headerHeight : CGFloat.leastNonzeroMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let section = SearchSections(rawValue: indexPath.section) else { return }
        
        switch section {
        case .nearby:
            if self.viewModel.isLocationAccess {
                self.showMerchantDetails(self.viewModel.getNearbyItem(indexPath.row))
            }
        case .results:
            self.showMerchantDetails(self.viewModel.getSearchedItem(indexPath.row))
        }
    }
}

extension SearchViewController: LocationAccessAdDelegate {
    
    func onLocationAccessAdButtonAction(_ ad: LocationAccessAd) {
        self.locationService?.requestAccess()
    }
    
}
