//
//  MerchantDetailsViewModel.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 20/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

final class MerchantDetailsViewModel: BaseViewModel {
    
    private let merchants: Merchants
    
    var merchant: Merchant?
    var discountModel: MerchantDiscount?
    
    init(storage: Storage, merchants: Merchants) {
        self.merchants = merchants
        
        super.init(storage: storage)
    }
    
    func getCurrentDiscount() -> Single<MerchantDiscount> {
        return merchants.getDiscount(
            id: Int(self.merchant?.backend_merchant_id ?? "-1") ?? -1,
            timeZone: NSTimeZone.local.identifier
            ).flatMap({ (discountModel) -> Single<MerchantDiscount> in
                self.discountModel = discountModel
                return Single.just(discountModel)
            })
    }
    
    func isMapButtonHidden() -> Bool {
        guard let _ = self.merchant?.locals.first?.lat,
            let _ = self.merchant?.locals.first?.lng else { return true }
        return false
    }
    
    func isPhoneHidden() -> Bool {
        guard let phone = self.merchant?.locals.first?.phone, !phone.isEmpty else { return true }
        return false
    }
    
    func isEmailHidden() -> Bool {
        guard let mail = self.merchant?.locals.first?.mail, !mail.isEmpty else { return true }
        return false
    }
    
    func isWebHidden() -> Bool {
        guard let web = self.merchant?.locals.first?.url, !web.isEmpty else { return true }
        return false
    }
    
    func isAddressHidden() -> Bool {
        guard let street = self.merchant?.locals.first?.street,
            let postCode = self.merchant?.locals.first?.post_code,
            let city = self.merchant?.locals.first?.city,
            !street.isEmpty && !postCode.isEmpty && !city.isEmpty else { return true }
        return false
    }
    
    func getMerchantAddress() -> String {
        guard let street = self.merchant?.locals.first?.street,
            let postCode = self.merchant?.locals.first?.post_code,
            let city = self.merchant?.locals.first?.city else { return "" }
        
        return "\(street), \(postCode) \(city)"
    }
    
    func getPhone() -> NSAttributedString {
        guard let text = self.merchant?.locals.first?.phone else { return NSAttributedString(string: "") }
        
        return  parseHTML(text: text)
    }
    
    func getEmail() -> NSAttributedString {
        guard let text = self.merchant?.locals.first?.mail else { return NSAttributedString(string: "") }
        
        return  parseHTML(text: text)
    }
    
    func getWeb() -> NSAttributedString {
        guard let text = self.merchant?.locals.first?.url else { return NSAttributedString(string: "") }
        
        return  parseHTML(text: text)
    }
    
    
    private func parseHTML(text: String) -> NSAttributedString {
        guard let data = text.data(using: String.Encoding.unicode) else { return NSAttributedString(string: "") }

        guard let string = try? NSMutableAttributedString(data: data,
                                                          options: [.documentType:NSAttributedString.DocumentType.html],
                                                          documentAttributes: nil) else { return NSAttributedString(string: "") }
        string.addAttribute(
            NSAttributedString.Key.font,
            value: FontFamily.Lato.regular.font(size: 16.0)!,
            range: NSRange(0..<string.length))
        
        string.addAttribute(
            NSAttributedString.Key.underlineStyle,
            value: NSUnderlineStyle.single.rawValue,
            range: NSRange(0..<string.length))
        
        return string
    }
}
