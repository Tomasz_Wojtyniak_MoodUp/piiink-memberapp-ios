//
//  MerchantDetailsViewController.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 20/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import UIKit
import Atributika
import MapKit
import RxSwift
import SDWebImage

final class MerchantDetailsViewController: BaseViewController<MerchantDetailsViewModel> {
    
    private let imageFlipperTimeInSec: UInt32 = 5
    private let imageTransitionTime = 0.3
    
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var bannerContainer: UIView!
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var detailsButton: PrimaryButton!
    @IBOutlet weak var discountContainer: UIView!
    
    @IBOutlet weak var informationHeaderLabel: UILabel!
    @IBOutlet weak var informationLabel: AttributedLabel!
    
    @IBOutlet weak var contactHeaderLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneContainer: UIStackView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailContainer: UIStackView!
    @IBOutlet weak var webLabel: UILabel!
    @IBOutlet weak var webContainer: UIStackView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressContainer: UIStackView!
    
    @IBOutlet weak var mapButton: UIButton!
    
    private let roundEffect = RoundEffect()
    private let shadowEffect = ShadowEffect()
    
    private var images: [String] = []
    private var imageIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTranslations()
        self.setupImageFlipper()
        self.setupDiscount()
        self.setupDescription()
        self.setupContactDetails()
        self.setupMapButton()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segues.moreOffers {
            guard let viewController = segue.destination as? MerchantMoreOffersViewController else { return }
            
            viewController.viewModel.merchant = self.viewModel.merchant
            viewController.viewModel.merchantDiscount = self.viewModel.discountModel
        }
    }
    
    private func setupMapButton() {
        self.mapButton.isHidden = self.viewModel.isMapButtonHidden()
    }
    
    private func setupContactDetails() {
        self.phoneContainer.isHidden = self.viewModel.isPhoneHidden()
        self.phoneLabel.attributedText = self.viewModel.getPhone()
        self.phoneContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onContainerTap(sender:))))
        
        self.emailContainer.isHidden = self.viewModel.isEmailHidden()
        self.emailLabel.attributedText = self.viewModel.getEmail()
        self.emailContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onContainerTap(sender:))))
        
        self.webContainer.isHidden = self.viewModel.isWebHidden()
        self.webLabel.attributedText = self.viewModel.getWeb()
        self.webContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onContainerTap(sender:))))
        
        self.addressContainer.isHidden = self.viewModel.isAddressHidden()
        self.addressLabel.text = self.viewModel.getMerchantAddress()
    }
    
    @objc func onContainerTap(sender: UITapGestureRecognizer) {
        switch sender.view {
        case self.phoneContainer:
            guard let number = URL(string: "tel://" + (self.phoneLabel.attributedText?.string ?? "")) else { return }
            UIApplication.shared.open(number, options: [:], completionHandler: nil)
        case self.emailContainer:
            guard let email = URL(string: "mailto://" + (self.emailLabel.attributedText?.string ?? "")) else { return }
            UIApplication.shared.open(email, options: [:], completionHandler: nil)
        case self.webContainer:
            guard let web = URL(string: (self.webLabel.attributedText?.string ?? "")) else { return }
            UIApplication.shared.open(web, options: [:], completionHandler: nil)
        default:
            return
        }
        
    }
    
    private func setupDescription() {
        self.informationLabel.numberOfLines = 0
        
        let link = Style("a")
            .foregroundColor(.blue, .normal)
            .foregroundColor(.blue, .highlighted)
        
        let all = Style.font(FontFamily.Lato.regular.font(size: 16.0))
        let b = Style("b").font(FontFamily.Lato.semibold.font(size: 16.0))
        let strong = Style("strong").font(FontFamily.Lato.semibold.font(size: 16.0))
        
        let transformers: [TagTransformer] = [
            TagTransformer.brTransformer,
            TagTransformer(tagName: "p", tagType: .start, replaceValue: "\n"),
            TagTransformer(tagName: "p", tagType: .end, replaceValue: "\n")
        ]
        
        self.informationLabel.attributedText = self.viewModel.merchant?.content?
            .style(tags: link, b, strong, transformers: transformers)
            .styleAll(all)
        
        self.informationLabel.onClick = { label, detection in
            switch detection.type {
            case .tag(let tag):
                if tag.name == "a", let href = tag.attributes["href"], let url = URL(string: href) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            default:
                break
            }
        }
    }
    
    private func setupDiscount() {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE h:00 a"
        formatter.locale = Locale(identifier: "en_US")
        self.timeLabel.text = formatter.string(from: Date())
        
        self.roundEffect.add(to: self.discountContainer)
        self.shadowEffect.pathType = .roundRect
        self.shadowEffect.add(to: self.discountContainer)
        
        self.viewModel.getCurrentDiscount()
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { (merchantDiscount) in
                self.discountLabel.text = "\(merchantDiscount.now.discount) %"
                self.discountLabel.font = FontFamily.Lato.medium.font(size: 36)
                self.detailsButton.isEnabled = true
            }) { (error) in
                self.discountLabel.text = L10n.MerchantDetails.noDiscount
                self.discountLabel.font = FontFamily.Lato.medium.font(size: 20)
            }
            .disposed(by: self.disposeBag)
    }
    
    private func setupImageFlipper() {
        self.roundEffect.add(to: self.bannerContainer)
        self.shadowEffect.pathType = .roundRect
        self.shadowEffect.add(to: self.bannerContainer)
        
        self.images = self.viewModel.merchant?.gallery ?? []
        if let thumbnail = self.viewModel.merchant?.thumbnail {
            self.images.insert(thumbnail, at: 0)
        }
        
        self.startImageFlipper()
    }
    
    private func startImageFlipper() {
        DispatchQueue.global(qos: .userInteractive).async (
            execute: {() -> Void in
                while true {
                    DispatchQueue.main.async(execute: {() -> Void in
                        if let encodedUrl = self.images[self.imageIndex].addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                            let url = URL(string: encodedUrl) {
                            SDWebImageDownloader.shared.downloadImage(with: url, completed: { (image, data, error, finished) in
                                UIView.transition(
                                    with: self.bannerImageView,
                                    duration: self.imageTransitionTime,
                                    options: .transitionCrossDissolve,
                                    animations: {self.bannerImageView.image = image},
                                    completion: nil
                                )
                            })
                        }
                        self.imageIndex += 1
                        if self.imageIndex == self.images.count {
                            self.imageIndex = 0
                        }
                    })
                    
                    sleep(self.imageFlipperTimeInSec)
                }
        })
    }
    
    private func setupTranslations() {
        self.navigationItem.title = self.viewModel.merchant?.title
        self.detailsButton.setTitle(L10n.MerchantDetails.moreOffers, for: .normal)
        self.informationHeaderLabel.text = L10n.MerchantDetails.additionalInfo
        self.contactHeaderLabel.text = L10n.MerchantDetails.contact
    }
    
    @IBAction func onMapClick(_ sender: Any) {
        guard let lat = self.viewModel.merchant?.locals.first?.lat, let latitude: CLLocationDegrees = Double(lat),
            let lng = self.viewModel.merchant?.locals.first?.lng, let longitude: CLLocationDegrees = Double(lng) else { return }
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.viewModel.merchant?.title ?? ""
        mapItem.openInMaps(launchOptions: options)
    }
    
    @IBAction func onDetailsClick(_ sender: Any) {
        self.performSegue(withIdentifier: Constants.Segues.moreOffers, sender: self)
    }
}
