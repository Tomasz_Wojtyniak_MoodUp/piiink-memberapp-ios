//
//  MoreOfferDiscountTableViewCell.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 23/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

class MoreOfferDiscountTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var discountLabel: UILabel!
    
    let roundEffect = RoundEffect()
    let shadowEffect = ShadowEffect()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundEffect.add(to: self.containerView)
        self.shadowEffect.pathType = .roundRect
        self.shadowEffect.add(to: self)
    }

}
