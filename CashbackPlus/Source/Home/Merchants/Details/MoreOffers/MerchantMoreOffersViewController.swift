//
//  MerchantMoreOffersViewController.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 23/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import UIKit
import MapKit

enum MoreOffersSections: Int, CaseIterable {
    case header
    case discounts
}

final class MerchantMoreOffersViewController: BaseViewController<MerchantMoreOffersViewModel> {
    
    private let tableViewBottomInset: CGFloat = 100
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTranslations()
        self.setupTableView()
        self.setupMapButton()
    }
    
    private func setupMapButton() {
        self.mapButton.isHidden = self.viewModel.isMapButtonHidden()
    }
    
    private func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.separatorStyle = .none
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: self.tableViewBottomInset, right: 0)

        self.registerTableViewCells()
    }
    
    private func registerTableViewCells() {
        self.tableView.register(
            MoreOffersHeaderTableViewCell.nib(),
            forCellReuseIdentifier: MoreOffersHeaderTableViewCell.nibName()
        )
        
        self.tableView.register(
            MoreOfferDiscountTableViewCell.nib(),
            forCellReuseIdentifier: MoreOfferDiscountTableViewCell.nibName()
        )
    }
    
    private func setupTranslations() {
        self.title = L10n.MoreOffers.title
    }
    
    @IBAction func onMapClicked(_ sender: Any) {
        guard let lat = self.viewModel.merchant?.locals.first?.lat, let latitude: CLLocationDegrees = Double(lat),
            let lng = self.viewModel.merchant?.locals.first?.lng, let longitude: CLLocationDegrees = Double(lng) else { return }
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = self.viewModel.merchant?.title ?? ""
        mapItem.openInMaps(launchOptions: options)
    }
}

extension MerchantMoreOffersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return MoreOffersSections.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = MoreOffersSections(rawValue: section) else { return 0 }
        
        switch section {
        case .header:
            return self.viewModel.defaultCellCount
        case .discounts:
            return self.viewModel.discounts.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = MoreOffersSections(rawValue: indexPath.section) else { return UITableViewCell() }
        
        switch section {
        case .header:
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: MoreOffersHeaderTableViewCell.nibName(),
                for: indexPath) as? MoreOffersHeaderTableViewCell else { return UITableViewCell() }
            
            if let urlString = self.viewModel.merchant?.thumbnail.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                let url = URL(string: urlString) {
                cell.thumbnailImage.sd_setImage(with: url, completed: nil)
            }
            
            cell.headerLabel.text = L10n.MoreOffers.header
            
            return cell
            
        case .discounts:
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: MoreOfferDiscountTableViewCell.nibName(),
                for: indexPath) as? MoreOfferDiscountTableViewCell else { return UITableViewCell() }
            
            let model = self.viewModel.discounts[indexPath.row]
            
            let translateHours = {(hour: Int) -> String in
                let amOrPm = hour >= 12 ? "PM" : "AM"
                let newHour = (hour % 12) != 0 ? (hour % 12) : 12;
                return "\(newHour):00 \(amOrPm)"
            }
            
            let day = self.viewModel.getDayName(dayIndex: model.day)
            cell.timeLabel.text = "\(day) \(translateHours(model.hourStart)) - \(translateHours(model.hourEnd))"
            cell.discountLabel.text = "\(model.discount)%"
            
            return cell
        }
    }
    
    
}
