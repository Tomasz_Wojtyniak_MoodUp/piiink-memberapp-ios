//
//  DiscountWrapperModel.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 23/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct DiscountWrapperModel: Codable {
    let day: Int
    let hourStart: Int
    let hourEnd: Int
    let discount: Int
}
