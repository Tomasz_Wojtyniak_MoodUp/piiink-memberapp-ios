//
//  MerchantMoreOffersViewModel.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 23/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

final class MerchantMoreOffersViewModel: BaseViewModel {
    
    let defaultCellCount = 1
    
    var merchant: Merchant?
    var merchantDiscount: MerchantDiscount? {
        didSet {
            self.prepareDiscountModels()
        }
    }
    var discounts: [DiscountWrapperModel] = []
    
    func isMapButtonHidden() -> Bool {
        guard let _ = self.merchant?.locals.first?.lat,
            let _ = self.merchant?.locals.first?.lng else { return true }
        return false
    }
    
    private func prepareDiscountModels() {
        self.discounts.removeAll()
        
        guard let merchantDiscount = self.merchantDiscount else { return }
        for day in 0...6 {
            switch day {
            case 0:
                self.prepareModelsForDay(
                    dayIndex: day,
                    dayModel: merchantDiscount.discount.monday
                )
            case 1:
                self.prepareModelsForDay(
                    dayIndex: day,
                    dayModel: merchantDiscount.discount.tuesday
                )
            case 2:
                self.prepareModelsForDay(
                    dayIndex: day,
                    dayModel: merchantDiscount.discount.wednesday
                )
            case 3:
                self.prepareModelsForDay(
                    dayIndex: day,
                    dayModel: merchantDiscount.discount.thursday
                )
            case 4:
                self.prepareModelsForDay(
                    dayIndex: day,
                    dayModel: merchantDiscount.discount.friday
                )
            case 5:
                self.prepareModelsForDay(
                    dayIndex: day,
                    dayModel: merchantDiscount.discount.saturday
                )
            case 6:
                self.prepareModelsForDay(
                    dayIndex: day,
                    dayModel: merchantDiscount.discount.sunday
                )
            default:
                continue
            }
        }
    }
    
    private func prepareModelsForDay(dayIndex: Int, dayModel: DiscountDay) {
        var dayModels: [DiscountWrapperModel] = []
        
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 0, hourEnd: 1, discount: dayModel.hour0))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 1, hourEnd: 2, discount: dayModel.hour1))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 2, hourEnd: 3, discount: dayModel.hour2))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 3, hourEnd: 4, discount: dayModel.hour3))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 4, hourEnd: 5, discount: dayModel.hour4))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 5, hourEnd: 6, discount: dayModel.hour5))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 6, hourEnd: 7, discount: dayModel.hour6))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 7, hourEnd: 8, discount: dayModel.hour7))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 8, hourEnd: 9, discount: dayModel.hour8))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 9, hourEnd: 10, discount: dayModel.hour9))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 10, hourEnd: 11, discount: dayModel.hour10))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 11, hourEnd: 12, discount: dayModel.hour11))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 12, hourEnd: 13, discount: dayModel.hour12))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 13, hourEnd: 14, discount: dayModel.hour13))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 14, hourEnd: 15, discount: dayModel.hour14))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 15, hourEnd: 16, discount: dayModel.hour15))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 16, hourEnd: 17, discount: dayModel.hour16))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 17, hourEnd: 18, discount: dayModel.hour17))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 18, hourEnd: 19, discount: dayModel.hour18))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 19, hourEnd: 20, discount: dayModel.hour19))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 20, hourEnd: 21, discount: dayModel.hour20))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 21, hourEnd: 22, discount: dayModel.hour21))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 22, hourEnd: 23, discount: dayModel.hour22))
        dayModels.append(DiscountWrapperModel(day: dayIndex, hourStart: 23, hourEnd: 0, discount: dayModel.hour23))
        
        self.discounts.append(contentsOf: self.simplifyModels(models: dayModels))
    }
    
    private func simplifyModels(models: [DiscountWrapperModel]) -> [DiscountWrapperModel] {
        var simplifiedList: [DiscountWrapperModel] = []
        var startRange = models.first?.hourStart ?? 0
        var currentDiscount = models.first?.discount ?? 0
        
        for (index, model) in models.enumerated() {
            if (index == (models.count - 1)) {
                simplifiedList.append(DiscountWrapperModel(day: model.day, hourStart: startRange, hourEnd: 24, discount: currentDiscount))
            } else {
                if currentDiscount == model.discount {
                    continue
                } else {
                    simplifiedList.append(DiscountWrapperModel(day: model.day, hourStart: startRange, hourEnd: index, discount: currentDiscount))
                    startRange = index
                    currentDiscount = model.discount
                }
            }
        }
        
        return simplifiedList
    }
    
    func getDayName(dayIndex: Int) -> String {
        switch dayIndex {
        case 0:
            return L10n.MoreOffers.monday
        case 1:
            return L10n.MoreOffers.tuesday
        case 2:
            return L10n.MoreOffers.wednesday
        case 3:
            return L10n.MoreOffers.thursday
        case 4:
            return L10n.MoreOffers.friday
        case 5:
            return L10n.MoreOffers.saturday
        case 6:
            return L10n.MoreOffers.sunday
        default:
            return ""
        }
    }
}
