//
//  RegionsViewController.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 23/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import UIKit

final class RegionsViewController: BaseViewController<RegionsViewModel> {
    
    @IBOutlet weak var currentLocationContainer: UIView!
    @IBOutlet weak var prefixLabel: UILabel!
    @IBOutlet weak var currentLocationLabel: UILabel!
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private let roundEffect = RoundEffect()
    private let shadowEffect = ShadowEffect()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTranslations()
        self.setupTableView()
        self.roundEffect.add(to: self.currentLocationContainer)
    }
    
    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = .none
        
        self.registerTableViewCells()
    }
    
    private func registerTableViewCells() {
        self.tableView.register(
            RegionTableViewCell.nib(),
            forCellReuseIdentifier: RegionTableViewCell.nibName()
        )
    }
    
    private func setupTranslations() {
        self.title = L10n.Regions.title
        self.prefixLabel.text = L10n.Regions.prefix
        self.headerLabel.text = L10n.Regions.header
        self.currentLocationLabel.text = self.viewModel.storage.currentRegion.value?.name
    }
}

extension RegionsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.storage.regions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: RegionTableViewCell.nibName(),
            for: indexPath) as? RegionTableViewCell else { return UITableViewCell() }
        
        let regionName = self.viewModel.storage.regions[indexPath.row].name
        cell.regionLabel.text = regionName
        
        if indexPath.row == 0 {
            cell.roundedCorners = [.topLeft, .topRight]
        } else if indexPath.row == (self.viewModel.storage.regions.count - 1) {
            cell.roundedCorners = [.bottomLeft, .bottomRight]
        } else {
            cell.roundedCorners = []
        }
        
        cell.checkmarkImage.isHidden = self.viewModel.storage.currentRegion.value?.name != regionName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let region = self.viewModel.storage.regions[indexPath.row]
        self.viewModel.storage.store(chosenRegion: region)
        self.currentLocationLabel.text = region.name
        self.tableView.reloadData()
    }
}
