//
//  RegionTableViewCell.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 24/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

class RegionTableViewCell: UITableViewCell {

    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var checkmarkImage: UIImageView!
    
    var roundedCorners: UIRectCorner = []
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCorners(corners: self.roundedCorners, radius: StyleConstants.RoundCorners.radius)
    }
}
