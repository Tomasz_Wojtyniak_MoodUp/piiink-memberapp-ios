//
//  LocationAdTableViewCell.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 19/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

class LocationAdTableViewCell: UITableViewCell {

    @IBOutlet weak var locationAdView: LocationAccessAd!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
