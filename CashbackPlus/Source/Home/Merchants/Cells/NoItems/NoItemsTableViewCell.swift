//
//  NoItemsTableViewCell.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 20/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import Atributika

class NoItemsTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: AttributedLabel!
    @IBOutlet weak var wrapperView: UIView!
    
    var onRecommendLinkPressed: (() -> Void)?;
    let roundEffect = RoundEffect()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let all = Style.font(StyleConstants.Modal.regularFont ?? .systemFont(ofSize: 16)).foregroundColor(StyleConstants.Modal.textColor)
        let link = Style("a")
            .foregroundColor(.orange, .normal)
            .foregroundColor(.orange, .highlighted)
            .underlineColor(.orange)
            .underlineStyle(.single)
        
        self.descriptionLabel.numberOfLines = 0
        self.descriptionLabel.attributedText = L10n.Merchants.noMerchants
            .style(tags: link)
            .styleAll(all)
        
        self.descriptionLabel.onClick = { label, detection in
            switch detection.type {
            case .tag(let tag):
                if tag.name == "a" {
                    self.onRecommendLinkPressed?();
                }
                break
            default:
                break
            }
        }
        
        
        self.roundEffect.add(to: self.wrapperView)
    }
}
