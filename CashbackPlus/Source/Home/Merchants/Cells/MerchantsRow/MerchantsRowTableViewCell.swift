//
//  MerchantsRowTableViewCell.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 19/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

class MerchantsRowTableViewCell: UITableViewCell {
    
    @IBOutlet weak var leftMerchant: OfferBox!
    @IBOutlet weak var rightMerchant: OfferBox!
    
    var isPlaceholder = false {
        didSet {
            self.updatePlaceholders()
        }
    }
    
    weak var delegate: MerchantDelegate?
    var leftModel, rightModel: Merchant?
    
    func setup(leftMerchant: Merchant, rightMerchant: Merchant?) {
        self.leftModel = leftMerchant
        self.setupBox(box: self.leftMerchant, model: leftMerchant)
        
        if let merchant = rightMerchant {
            self.rightModel = rightMerchant
            self.rightMerchant.isHidden = false
            self.setupBox(box: self.rightMerchant, model: merchant)
        } else {
            self.rightMerchant.isHidden = true
        }
    }
    
    public func setupBox(box: OfferBox, model: Merchant) {
        if let encodedUrl = model.thumbnail.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let url = URL(string: encodedUrl) {
            box.setImage(url: url)
        } else {
            box.image = nil
        }
        
        box.title = model.title
        
        guard let discount = model.discount, !discount.isEmpty else {
            box.discountText = L10n.Offerbox.discountText("0")
            return
        }
        box.discountText = L10n.Offerbox.discountText(discount)
        box.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onBoxTap)))
    }
    
    @objc func onBoxTap(selector: UITapGestureRecognizer) {
        if let model = self.leftModel, selector.view == self.leftMerchant {
            self.delegate?.onMerchantSelected(model)
        } else if let model = self.rightModel, selector.view == self.rightMerchant {
            self.delegate?.onMerchantSelected(model)
        }
    }
    
    public func updatePlaceholders() {
        self.leftMerchant.isPlaceholder = self.isPlaceholder
        self.rightMerchant.isPlaceholder = self.isPlaceholder
    }
}
