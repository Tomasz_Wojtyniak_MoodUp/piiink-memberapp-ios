//
//  CategoryCollectionViewCell.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 13/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var categoryImageView: CategoryIcon!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setup(_ category: Category) {
        self.categoryImageView.category = category.type
        self.titleLabel.text = self.formatedName(category)
    }
    
    private func formatedName(_ category: Category) -> String {
        switch category.type {
        case .all:
            return L10n.CategoryCollection.all
        case .unknown:
            return self.encodeHTMLName(category)
        default:
            return self.encodeHTMLName(category)
        }
    }
    
    private func encodeHTMLName(_ category: Category) -> String {
        guard let data = category.name.data(using: .utf8) else {
            return ""
        }
        
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]
        
        guard let attributedString = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
            return ""
        }
        
        return attributedString.string
    }
}
