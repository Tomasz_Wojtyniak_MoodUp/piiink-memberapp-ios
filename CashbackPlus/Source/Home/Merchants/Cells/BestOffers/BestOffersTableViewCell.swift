//
//  BestOffersTableViewCell.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 17/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

class BestOffersTableViewCell: UITableViewCell {
    
    private let preloadItemsCount = 4

    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var isMerchantInit = true
    
    weak var delegate: MerchantDelegate?
    
    var merchants: [Merchant] = [] {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.initCollectionView()
    }
    
    private func initCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.registerCollectionViewCells()
    }
    
    private func registerCollectionViewCells() {
        self.collectionView.register(
            MerchantCollectionViewCell.nib(),
            forCellWithReuseIdentifier: MerchantCollectionViewCell.nibName()
        )
    }
}

extension BestOffersTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.merchants.isEmpty && self.isMerchantInit {
            return self.preloadItemsCount
        } else {
            return self.merchants.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: MerchantCollectionViewCell.nibName(),
            for: indexPath) as? MerchantCollectionViewCell else { return UICollectionViewCell() }
        
        if self.merchants.isEmpty && self.isMerchantInit {
            cell.offerBoxView.isPlaceholder = true
        } else {
            cell.offerBoxView.isPlaceholder = false
            cell.setup(model: self.merchants[indexPath.item])
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !(self.merchants.isEmpty && self.isMerchantInit) {
            self.delegate?.onMerchantSelected(self.merchants[indexPath.item])
        }
    }
}
