//
//  MerchantDelegate.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 20/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

protocol MerchantDelegate: AnyObject {
    func onMerchantSelected(_ merchant: Merchant)
}
