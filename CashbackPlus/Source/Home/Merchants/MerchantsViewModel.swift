//
//  MerchantsViewModel.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 13/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay
import CoreLocation

enum MerchantsType {
    case bestOffer
    case nearby
    case new
    case others
}

final class MerchantsViewModel: BaseViewModel {
    
    let defaultCellCount = 1
    private let preloadItemsCount = 2
    
    public let otherMerchantsRelay: PublishRelay<[Merchant]> = PublishRelay()
    
    private let merchants: Merchants
    private let locationProvider: LocationProvider
    
    lazy var categories = [Constants.Categories.all]
    private lazy var bestOffers: [Merchant] = []
    private lazy var nearbyMerchants: [Merchant] = []
    private lazy var newMerchants: [Merchant] = []
    private lazy var allMerchants: [Merchant] = []
    private lazy var otherMerchants: [Merchant] = []
    
    var currentCategoryIndex = 0
    var currentRegion: Region?
    
    lazy var isLocationAccess = false
    lazy var isNearbyInit = true
    lazy var isMerchantsInit = true
    
    lazy var nearbyOffers =  self.locationProvider
        .getLocation()
        .filter({ $0.coordinate.latitude != 0.0 || $0.coordinate.longitude != 0.0 })
        .flatMap { location in
            self.merchants.getNearbyOffers(location)
        }
        .flatMap { (merchant) -> Single<[Merchant]> in
            var sortedMerchants = merchant
            sortedMerchants.sort(by: { ($0.distance ?? 0.0) < ($1.distance ?? 0.0) })
            self.nearbyMerchants = sortedMerchants
            self.refreshOtherMerchants()
            return Single.just(sortedMerchants)
        }
    
    init(storage: Storage, merchants: Merchants, locationProvider: LocationProvider) {
        self.merchants = merchants
        self.locationProvider = locationProvider
        self.currentRegion = storage.currentRegion.value
        super.init(storage: storage)
    }
    
    func getCategories() -> Completable {
        return merchants.getCategories()
            .flatMapCompletable({ (categories) -> Completable in
                self.categories = [Constants.Categories.all]
                self.categories.append(contentsOf: categories.sorted(by: { $0.name < $1.name }))
                return Completable.empty()
            })
    }
    
    func getBestOffers() -> Completable {
        return merchants.getBestOffers()
            .flatMapCompletable({ (merchants) -> Completable in
                self.bestOffers = merchants.sorted(by: { (Float($0.discount ?? "0") ?? 0.0) < (Float($1.discount ?? "0") ?? 0.0) })
                self.refreshOtherMerchants()
                return Completable.empty()
            })
    }
    
    func getNewMerchants() -> Completable {
        return merchants.getNewMerchants()
            .flatMapCompletable({ (merchants) -> Completable in
                self.newMerchants = merchants
                self.refreshOtherMerchants()
                return Completable.empty()
            })
    }
    
    func getAllMerchants() -> Completable {
        return merchants.getAllOffers()
            .flatMapCompletable({ (merchants) -> Completable in
                self.allMerchants = merchants
                self.refreshOtherMerchants()
                return Completable.empty()
            })
    }
    
    func refreshOtherMerchants() {
        var otherMerchants = self.allMerchants
        guard !otherMerchants.isEmpty else {
            self.otherMerchantsRelay.accept(otherMerchants)
            return
        }
        
        otherMerchants = otherMerchants
            .filter({ otherMerchant in !getFilteredMerchants(.bestOffer).contains(where: { $0.title == otherMerchant.title }) })
            .filter({ otherMerchant in !getFilteredMerchants(.nearby).contains(where: { $0.title == otherMerchant.title }) })
            .filter({ otherMerchant in !getFilteredMerchants(.new).contains(where: { $0.title == otherMerchant.title }) })
        
        self.otherMerchants = otherMerchants
        self.otherMerchantsRelay.accept(otherMerchants)
    }
    
    func getFilteredMerchants(_ merchantsType: MerchantsType) -> [Merchant] {
        var merchants = self.getMerchantsList(for: merchantsType)
            .filter { merchant in
                return self.categories[currentCategoryIndex].name == Constants.Categories.all.name ||
                    merchant.category.contains(self.categories[currentCategoryIndex].name)
            }.filter { merchant in
                guard let region = self.currentRegion else { return true }
                return merchant.region.contains(region.name)
        }
        
        if merchantsType == .nearby && merchants.count >= Constants.Location.maxNearbyItems {
            merchants = Array(merchants[0 ..< Constants.Location.maxNearbyItems])
        }
        
        return merchants
    }
    
    func getMerchantsList(for type: MerchantsType) -> [Merchant] {
        switch type {
        case .bestOffer:
            return self.bestOffers
        case .nearby:
            return self.nearbyMerchants
        case .new:
            return self.newMerchants
        case .others:
            return self.otherMerchants
        }
    }
    
    func getNearbyItemsCount() -> Int {
        let filteredNearby = self.getFilteredMerchants(.nearby)
        
        if self.isLocationAccess {
            if filteredNearby.isEmpty && self.isNearbyInit {
                return self.preloadItemsCount
            }
            let itemsCount = filteredNearby.count
            if itemsCount == 0 {
                return self.defaultCellCount
            } else {
                return (itemsCount / 2) + (itemsCount % 2)
            }
        } else {
            return self.defaultCellCount
        }
    }
    
    func getNewItemsCount() -> Int {
        let filteredNew = self.getFilteredMerchants(.new)
        
        if filteredNew.isEmpty && self.isMerchantsInit {
            return self.preloadItemsCount
        } else {
            let itemsCount = filteredNew.count
            if itemsCount == 0 {
                return self.defaultCellCount
            } else {
                return (itemsCount / 2) + (itemsCount % 2)
            }
        }
    }
    
    func getOthersItemCount() -> Int {
        let filteredOthers = self.getFilteredMerchants(.others)
        
        if filteredOthers.isEmpty && self.isMerchantsInit {
            return self.preloadItemsCount
        } else {
            let itemsCount = filteredOthers.count
            return itemsCount == 0 ? 0 : ((itemsCount / 2) + (itemsCount % 2))
        }
    }
    
    func setNearbyMerchants(_ merchants: [Merchant]) {
        self.nearbyMerchants = merchants
    }
    
    func getNearbyMerchants() -> [Merchant] {
        return self.nearbyMerchants
    }
}
