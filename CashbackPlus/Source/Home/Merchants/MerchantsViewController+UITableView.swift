//
//  MerchantsViewController+UITableViewDataSource.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 19/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import UIKit

extension MerchantsViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return MerchantsSections.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = MerchantsSections(rawValue: section) else { return 0 }
        
        switch section {
        case .bestOffers:
            return self.viewModel.defaultCellCount
            
        case .nearby:
            return self.viewModel.getNearbyItemsCount()
            
        case .newMerchents:
            return self.viewModel.getNewItemsCount()
            
        case .others:
            return self.viewModel.getOthersItemCount()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = MerchantsSections(rawValue: indexPath.section) else { return UITableViewCell() }
        
        switch (section) {
            
        case .bestOffers:
            return self.getBestOffersCell(indexPath)
            
        case .nearby:
            return self.getNearbyCell(indexPath)
            
        case .newMerchents:
            return self.getNewMerchantsCell(indexPath)
            
        case .others:
            return self.getOtherMerchantsCell(indexPath)
            
        }
    }
    
    private func getOtherMerchantsCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(
            withIdentifier: MerchantsRowTableViewCell.nibName(),
            for: indexPath) as? MerchantsRowTableViewCell else { return UITableViewCell() }
        
        self.setupMerchantsRowCell(
            cell,
            models: self.viewModel.getFilteredMerchants(.others),
            indexPath: indexPath,
            isInit: self.viewModel.isMerchantsInit
        )
        
        return cell
    }
    
    private func setupMerchantsRowCell(_ cell: MerchantsRowTableViewCell, models: [Merchant], indexPath: IndexPath, isInit: Bool) {
        if models.isEmpty && isInit {
            cell.isPlaceholder = true
            cell.delegate = nil
        } else {
            cell.isPlaceholder = false
            cell.setup(
                leftMerchant: models[indexPath.row * 2],
                rightMerchant: models.count <= ((indexPath.row * 2) + 1) ? nil : models[(indexPath.row * 2) + 1])
            cell.delegate = self
        }
    }
    
    private func getNewMerchantsCell(_ indexPath: IndexPath) -> UITableViewCell {
        let filteredNew = self.viewModel.getFilteredMerchants(.new)
        
        if filteredNew.isEmpty && !self.viewModel.isMerchantsInit {
            return self.getNoItemsCell(indexPath)
        } else {
            guard let cell = self.tableView.dequeueReusableCell(
                withIdentifier: MerchantsRowTableViewCell.nibName(),
                for: indexPath) as? MerchantsRowTableViewCell else { return UITableViewCell() }
            
            self.setupMerchantsRowCell(
                cell,
                models: filteredNew,
                indexPath: indexPath,
                isInit: self.viewModel.isMerchantsInit
            )
            
            return cell
            
        }
    }
    
    private func getBestOffersCell(_ indexPath: IndexPath) -> UITableViewCell {
        let filteredBestOffers = self.viewModel.getFilteredMerchants(.bestOffer)
        
        if filteredBestOffers.isEmpty && !self.viewModel.isMerchantsInit {
            return self.getNoItemsCell(indexPath)
        } else {
            guard let cell = self.tableView.dequeueReusableCell(
                withIdentifier: BestOffersTableViewCell.nibName(),
                for: indexPath) as? BestOffersTableViewCell else { return UITableViewCell() }
            
            cell.delegate = self
            cell.merchants = filteredBestOffers
            cell.isMerchantInit = self.viewModel.isMerchantsInit
            
            return cell
        }
    }
    
    private func getNoItemsCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(
            withIdentifier: NoItemsTableViewCell.nibName(),
            for: indexPath) as! NoItemsTableViewCell
        
        cell.onRecommendLinkPressed = {
            let controller = self.instantiate(RecommendViewController.self, storyboard: Constants.Storyboards.home)
            self.show(controller, sender: self)
        }
        
        return cell
    }
    
    private func getNearbyCell(_ indexPath: IndexPath) -> UITableViewCell {
        if !self.viewModel.isLocationAccess {
            return self.getRequestLocationCell(indexPath)
        } else {
            let filteredNearby = self.viewModel.getFilteredMerchants(.nearby)
            
            if filteredNearby.isEmpty && !self.viewModel.isNearbyInit {
                return self.getNoItemsCell(indexPath)
            } else {
                guard let cell = self.tableView.dequeueReusableCell(
                    withIdentifier: MerchantsRowTableViewCell.nibName(),
                    for: indexPath) as? MerchantsRowTableViewCell else { return UITableViewCell() }
                
                self.setupMerchantsRowCell(
                    cell,
                    models: filteredNearby,
                    indexPath: indexPath,
                    isInit: self.viewModel.isNearbyInit
                )
                
                return cell
            }
        }
    }
    
    private func getRequestLocationCell(_ indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(
            withIdentifier: LocationAdTableViewCell.nibName(),
            for: indexPath) as? LocationAdTableViewCell else { return UITableViewCell() }
        
        cell.locationAdView.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let section = MerchantsSections(rawValue: section) else { return nil }
        
        let headerView: HeaderView? = HeaderView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.headerHeight))
        
        switch section {
        case .bestOffers:
            headerView?.titleLabel.text = L10n.Merchants.bestOffers
        case .nearby:
            headerView?.titleLabel.text = L10n.Merchants.nearBy
        case .newMerchents:
            headerView?.titleLabel.text = L10n.Merchants.newMerchants
        case .others:
            headerView?.titleLabel.text = L10n.Merchants.otherMerchants
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let section = MerchantsSections(rawValue: section) else { return 0 }
        
        switch section {
        case .bestOffers, .nearby, .newMerchents:
            return self.headerHeight
        case .others:
            let filteredOthers = self.viewModel.getFilteredMerchants(.others)
            return filteredOthers.isEmpty && !self.viewModel.isMerchantsInit ? 0 : self.headerHeight
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let height = cellHeights[indexPath] else { return CGFloat.leastNonzeroMagnitude }
        return height
    }
}
