//
//  HomeViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 24/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import RxSwift

final class HomeViewController: BaseViewController<HomeViewModel> {
    
    @IBOutlet weak var offersContainer: UIView!
    @IBOutlet weak var profileContainer: UIView!
    
    @IBOutlet weak var tabBar: HomeTabBar!
    
    @IBOutlet weak var offersTabBarItem: UITabBarItem!
    @IBOutlet weak var profileTabBarItem: UITabBarItem!
    
    private var profileController: ProfileViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        switch self.tabBar.selectedItem {
        case self.offersTabBarItem:
            self.navigationController?.isNavigationBarHidden = true
            break
        case self.profileTabBarItem:
            self.navigationController?.isNavigationBarHidden = false
            break
        default:
            break
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    private func setup() {
        self.tabBar.delegate = self
        self.showOffersScreen()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidCardAdd), name: .didAddCard, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onDidRegisterCard), name: .didRegisterCard, object: nil)
    }
    
    @objc func onDidCardAdd() {
        self.showCardPopup()
    }
    
    @objc func onDidRegisterCard() {
        if self.viewModel.isLogged {
            self.viewModel
                .isCardRegistered()
                .observeOn(MainScheduler.instance)
                .subscribe(onSuccess: { [weak self] isRegistered in
                    guard let context = self else { return }
                    
                    if isRegistered {
                        context.showCardPopup()
                    } else {
                        context.showRegistrationErrorAlert()
                    }
                })
                .disposed(by: self.disposeBag)
        } else {
            self.showCardPopup()
        }
    }
    
    private func showRegistrationErrorAlert() {
        let alert = UIAlertController(
            title: L10n.Card.Registration.errorTitle,
            message: L10n.Card.Registration.errorMsg,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: L10n.Card.Registration.errorDefaultAction, style: .default))
        self.present(alert, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let childViewController = segue.destination as? ProfileViewController {
            self.profileController = childViewController
        }
    }
    
    func showOffersScreen() {
        self.tabBar.selectedItem = self.offersTabBarItem
        self.navigationController?.isNavigationBarHidden = true
        
        UIView.animate(withDuration: StyleConstants.Animations.viewReplacingDuration, animations: { [weak self] in
            guard let context = self else { return }
            
            context.offersContainer.isHidden = false
            context.profileContainer.isHidden = true
        })
    }
    
    func showProfileScreen() {
        self.tabBar.selectedItem = self.profileTabBarItem
        self.navigationController?.isNavigationBarHidden = false
        
        let giftButton = UIBarButtonItem(image: Asset.giftIconNavigation.image, style: .plain, target: self, action: #selector(showGiftPopup))
        self.navigationItem.rightBarButtonItem = giftButton
        self.title = L10n.Profile.title
        
        UIView.animate(withDuration: StyleConstants.Animations.viewReplacingDuration, animations: { [weak self] in
            guard let context = self else { return }
            
            context.offersContainer.isHidden = true
            context.profileContainer.isHidden = false
        })
        
        profileController?.onProfileShow()
    }
    
    private func showCardPopup() {
        let controller = self.instantiate(CardViewController.self)
        controller.delegate = self
        self.present(controller, animated: true)
    }
    
    @objc private func showGiftPopup() {
        self.showInModal(ModalPrizeInfoViewController.self) { (modal, controller) in
            controller.onAcceptHandler = {
                modal.dismiss(animated: true)
            }
        }
    }
    
    @IBAction func onCardAction(_ sender: Any) {
        self.showCardPopup()
    }
    
}
