//
//  HomeViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 24/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

extension HomeViewController: UITabBarDelegate {

    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item {
        case self.offersTabBarItem:
            self.showOffersScreen()
            break
        case self.profileTabBarItem:
            self.showProfileScreen()
            break
        default:
            break
        }
    }

}
