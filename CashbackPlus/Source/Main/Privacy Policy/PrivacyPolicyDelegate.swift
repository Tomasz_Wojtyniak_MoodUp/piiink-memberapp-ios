//
//  PrivacyPolicyDelegate.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 22/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

protocol PrivacyPolicyDelegate {
    
    func onPrivacyPolicyAccept(_ controller: PrivacyPolicyViewController)
    
}
