//
//  LocationTableViewCell.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 30/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

final class LocationTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectedMarkImageView: UIImageView!
    
    var region: Region? {
        didSet {
            self.titleLabel.text = self.region?.name
        }
    }
    
    func setup(_ region: Region) {
        self.region = region
        self.selectedMarkImageView.isHidden = true
    }
    
    func markSelected() {
        self.selectedMarkImageView.isHidden = false
    }

}
