//
//  LocalizationViewModel.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 02/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay

final class LocalizationViewModel : BaseViewModel {
    
    private var remoteRegions: Regions
    private var cachedRegions: Regions
    
    lazy var availableRegions = Observable.concat([
        self.cachedRegions.getRegions().asObservable(),
        self.remoteRegions.getRegions().asObservable().do(onNext: { regions in
            self.storage.store(regions: regions)
            self.isFetching.accept(false)
        })
    ])
    
    var currentRegion: BehaviorRelay<Region?> {
        return self.storage.currentRegion
    }
    
    lazy var isFetching = BehaviorRelay<Bool>(value: true)
    
    func chooseRegion(_ region: Region) {
        self.storage.store(chosenRegion: region)
    }
    
    init(storage: Storage, cachedRegions: Regions, remoteRegions: Regions) {
        self.cachedRegions = cachedRegions
        self.remoteRegions = remoteRegions
        
        super.init(storage: storage)
    }
}
