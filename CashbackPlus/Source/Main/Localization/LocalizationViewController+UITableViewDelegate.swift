//
//  LocalizationViewController+UITableViewDelegate.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 30/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

extension LocalizationViewController : UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard
            let cell = tableView.cellForRow(at: indexPath) as? LocationTableViewCell,
            let region = cell.region
            else { return }
        
        self.viewModel.chooseRegion(region)
    }
    
}
