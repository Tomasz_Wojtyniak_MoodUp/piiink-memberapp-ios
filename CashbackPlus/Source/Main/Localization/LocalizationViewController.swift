//
//  LocalizationViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 30/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa

final class LocalizationViewController: BaseViewController<LocalizationViewModel> {
    
    @IBOutlet weak var searchInput: SearchInput!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeader: LocationTableHeader!
    @IBOutlet weak var noFoundInfoView: UIView!
    @IBOutlet weak var spinnerView: UIView!
    @IBOutlet weak var tableWrapperHeight: NSLayoutConstraint!
    
    let roundEffect = RoundEffect()

    private struct SelectableRegion {
        let region: Region
        let selected: Bool
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setup()
    }
    
    private func setup() {
        // Remove bottom line
        self.tableView.tableFooterView =
            UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 1))
    
        self.tableView.delegate = self
        self.roundEffect.add(to: self.tableView)
        
        self.setupTableHeader()
        self.setupFilter()
        self.setupSpinner()
    }
    
    private func setupFilter() {
        self.noFoundInfoView.isHidden = true
        
        let inputObserve = self.searchInput.inputField.rx.text.orEmpty
            .throttle(.milliseconds(300), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .observeOn(MainScheduler.instance)
        
        Observable
            .combineLatest(inputObserve, self.viewModel.availableRegions, self.viewModel.currentRegion)
            .flatMap { arg -> Observable<[(Region, Bool)]> in
                let (query, regions, currentRegion) = arg
                
                return .just(
                    regions
                        .filter{ $0.name.hasPrefix(query) }
                        .map { region in                            
                            if let currentRegion = currentRegion {
                                return (region, region == currentRegion)
                            } else {
                                return (region, false)
                            }
                        }
                )
            }
            .do (afterNext: { [weak self] regions in
                guard let context = self else { return }
                
                if regions.isEmpty {
                    context.noFoundInfoView.isHidden = false
                    context.tableHeader.border = false
                } else {
                    context.noFoundInfoView.isHidden = true
                    context.tableHeader.border = true
                }
                
                context.refreshTableWrapper()
            })
            .bind(to: self.tableView.rx.items(cellIdentifier: LocationTableViewCell.reuseIdentifier(), cellType: LocationTableViewCell.self)) {
                _, model, cell in
                
                let (region, isSelected) = model
                cell.setup(region)
                
                if isSelected {
                    cell.markSelected()
                }
            }
            .disposed(by: self.disposeBag)
    }
    
    private func setupTableHeader() {
        self.viewModel
            .currentRegion
            .subscribe(onNext: { [weak self] region in
                guard let region = region else { return }
                self?.tableHeader.location = region.name
            })
            .disposed(by: self.disposeBag)
    }
    
    private func setupSpinner() {
        self.viewModel
            .isFetching
            .subscribe(onNext: { [weak self] status in
                self?.spinnerView.isHidden = !status
            })
            .disposed(by: self.disposeBag)
    }
    
    func refreshTableWrapper() {
        self.tableView.layoutIfNeeded()
        let height = self.tableView.contentSize.height
        
        self.tableWrapperHeight.constant = height
    }

}
