//
//  LocationTableHeaderView.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 30/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

@IBDesignable
final class LocationTableHeader: UITableViewHeaderFooterView {
    
    @IBOutlet var xibView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    
    @IBInspectable
    var location: String = "" {
        didSet {
            self.setupTitle(self.location)
        }
    }
    
    var border: Bool {
        get {
            return xibView.layer.shadowOpacity > 0
        }
        set {
            xibView.layer.shadowOpacity = newValue ? 1.0 : 0.0
        }
    }

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
        self.contentView.prepareForInterfaceBuilder()
    }
    
    private func setup() {
        self.loadXIB()
        self.setBottomBorder()
    }
    
    private func loadXIB() {
        let bundle = Bundle(for: LocationTableHeader.self)
        bundle.loadNibNamed(Constants.XIBs.locationTableHeader, owner: self, options: nil)
        
        self.xibView.frame = self.bounds
        self.xibView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        self.addSubview(self.xibView)
    }

    private func setupTitle(_ location: String) {
        guard
            let boldFont = StyleConstants.LocationTableHeader.Fonts.bold
            else { return }
        
        let title = NSMutableAttributedString(string: L10n.Localization.Table.header(location))
        
        if let wordRange = title.string.range(of: location) {
            title.addAttribute(
                .font,
                value: boldFont,
                range: NSRange(wordRange, in: title.string)
            )
        }
        
        self.headerLabel.attributedText = title
        
    }
    
    func setBottomBorder() {
        let layer = self.xibView.layer
        
        layer.backgroundColor = StyleConstants.LocalizationTableHeader.backgroundColor
        layer.shadowColor = StyleConstants.LocalizationTableHeader.shadowColor
        layer.shadowOffset = StyleConstants.LocalizationTableHeader.offset
        layer.shadowRadius = .zero
        
        layer.masksToBounds = false
        self.border = true
    }
}
