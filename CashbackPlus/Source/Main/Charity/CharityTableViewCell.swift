//
//  CharityTableViewCell.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 16/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

final class CharityTableViewCell: UITableViewCell {

    @IBOutlet weak var checkboxImageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    var charity: Charity? {
        didSet {
            self.label.text = self.charity?.name
        }
    }
    
    var isChosen: Bool = false {
        didSet {
            if self.isChosen {
                self.checkboxImageView.image = Asset.heartFill.image
            } else {
                self.checkboxImageView.image = Asset.heartStroke.image
            }
        }
    }
    
    func setup(_ charity: Charity) {
        self.charity = charity
        self.isChosen = false
    }
    
}
