//
//  CharityViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 16/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class CharityViewController: BaseViewController<CharityViewModel>, UITableViewDelegate {

    @IBOutlet weak var charityTableView: UITableView!
    @IBOutlet weak var searchInputView: SearchInput!
    @IBOutlet weak var contentView: UIStackView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var noResultsFoundView: UIView!
    
    private var chosenCharity: BehaviorRelay<Charity>?
    private var charities = BehaviorRelay<[Charity]>(value: [])
    private var searchCharities = BehaviorRelay<[Charity]>(value: [])
    private var filter = ""
    private var isLoadingNextPage = false
    private var page = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }

    private func setup() {
        self.charityTableView.delegate = self
        self.viewModel.markSeenCharitySeen()
        
        self.searchInputView.inputField.rx
            .controlEvent(.editingChanged)
            .throttle(Constants.Inputs.throttleTime, scheduler: MainScheduler.instance)
            .observeOn(MainScheduler.instance)
            .map { [weak self] in self?.self.searchInputView.inputField.text ?? "" }
            .subscribe(onNext: { [weak self] text in
                guard let context = self else { return }
                
                context.filter = text
                context.fetchFirstPage(name: text)
            })
            .disposed(by: self.disposeBag)
        
        self.contentView.alpha = 0
        self.onLoadStart()
        
        self.setupTable()
        self.setupOnTap()
    }
    
    private func onLoadStart() {
        UIView.animate(withDuration: StyleConstants.Animations.viewReplacingDuration) {
            self.contentView.alpha = 0
        }

        self.indicatorView.startAnimating()
    }
    
    private func onLoadFinished() {
        UIView.animate(withDuration: StyleConstants.Animations.viewReplacingDuration) {
            self.contentView.alpha = 1
        }
        
        self.indicatorView.stopAnimating()
    }
    
    private func fetchNextPage() {
        guard !self.isLoadingNextPage else { return }
        self.isLoadingNextPage = true
        
        let name = self.filter
        
        self.viewModel
            .getCharitiesPage(page: self.page + 1, name: name)
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { [weak self] page in
                guard
                    let context = self,
                    let chosenCharity = context.chosenCharity?.value
                    else { return }
                
                let charities = page.content
                var result: [Charity] = []
                
                if name.isEmpty {
                    result = charities.filter { charity in charity != chosenCharity }
                } else {
                    result = charities
                }
                
                context.charities.accept(context.charities.value + result)
                
                context.isLoadingNextPage = false
                context.page += 1
            })
            .disposed(by: self.disposeBag)
    }
    
    private func fetchFirstPage(name: String = "") {
        self.page = 0
        
        self.viewModel
            .getCharitiesPage(page: 0, name: name)
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { [weak self] page in
                guard
                    let context = self,
                    let chosenCharity = context.chosenCharity?.value
                    else { return }
                
                let charities = page.content
                var result: [Charity] = []
                
                if name.isEmpty {
                    result = [chosenCharity] + charities.filter { charity in charity != chosenCharity }
                } else {
                    result = charities
                }
                
                context.charities.accept(result)
                context.onLoadFinished()
            })
            .disposed(by: self.disposeBag)
    }
    
    private func setupTable() {
        self.viewModel
            .getChosenCharity()
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { [weak self] charity in
                guard let context = self else { return }
                
                context.chosenCharity = BehaviorRelay<Charity>(value: charity)
                context.setupTableBinding()
                context.fetchFirstPage()
            })
            .disposed(by: self.disposeBag)
    }
    
    private func setupTableBinding() {
        guard let chosenCharity = self.chosenCharity else { return }
        
        Observable
            .combineLatest(
                self.charities,
                chosenCharity
            )
            .observeOn(MainScheduler.instance)
            .do(afterNext: { [weak self] (charities, _) in
                guard let context = self else { return }
                context.noResultsFoundView.isHidden = !charities.isEmpty
            })
            .map { (charities, chosenCharity) -> [(Charity, Bool)] in
                return charities.map { charity in (charity, charity == chosenCharity) }
            }
            .bind(to: self.charityTableView.rx.items(cellIdentifier: CharityTableViewCell.reuseIdentifier(), cellType: CharityTableViewCell.self)) {
                _, model, cell in
                let (charity, isChosen) = model
                cell.setup(charity)
                cell.isChosen = isChosen
            }
            .disposed(by: self.disposeBag)
    }
    
    private func setupOnTap() {
        self.charityTableView.rx
            .itemSelected
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] indexPath in
                guard
                    let context = self,
                    let cell = context.charityTableView.cellForRow(at: indexPath) as? CharityTableViewCell,
                    let chosenCharity = cell.charity
                    else { return }
                
                context.chosenCharity?.accept(chosenCharity)
            })
        .disposed(by: self.disposeBag)
    }
    
    @IBAction func onInfoButtonAction(_ sender: Any) {
        let controller = self.instantiate(CharitySlidesViewController.self)
        self.show(controller, sender: self)
    }
    
    func onRegisterEnd(_ controller: RegistrationViewController, status: Bool?) {
        
    }
    
    @IBAction func onSaveButtonAction(_ sender: Any) {
        guard let charity = self.chosenCharity?.value else { return }
        self.onLoadStart()
        
        self.viewModel
            .chooseCharity(charity)
            .observeOn(MainScheduler.instance)
            .subscribe(onCompleted: { [weak self] in
                self?.navigationController?.popViewController(animated: true)
            })
            .disposed(by: self.disposeBag)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = scrollView.contentOffset.y + scrollView.bounds.size.height - scrollView.contentInset.bottom
        let height = scrollView.contentSize.height
        
        if y > (height - StyleConstants.InfinityScrolling.distance) {
            self.fetchNextPage()
        }
    }
}
