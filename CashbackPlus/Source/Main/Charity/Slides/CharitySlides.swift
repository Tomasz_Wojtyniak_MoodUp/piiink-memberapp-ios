//
//  CharitySlides.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 16/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import UIKit


enum CharitySlides {
    
    private static func format(_ text: String) -> NSAttributedString {
        guard let boldFont = StyleConstants.Splashes.boldFont else { return NSAttributedString() }
        
        let attributedString = NSMutableAttributedString(
            string: text.replacingOccurrences(of: Constants.Localizables.boldSeparator, with: "")
        )
        
        for range in text.getRanges(of: Constants.Localizables.boldSeparator) {
            attributedString.addAttribute(
                .font,
                value: boldFont,
                range: range
            )
        }
        
        return attributedString
    }
    
    static let donate = Slide(
        image: Asset.charityIcon.image,
        title: L10n.Charity.Slide.Donate.title,
        description: format(L10n.Charity.Slide.Donate.description)
    )
    
    static let family = Slide(
        image: Asset.familyIcon.image,
        title: L10n.Charity.Slide.Family.title,
        description: format(L10n.Charity.Slide.Family.description)
    )
    
    static let register = Slide(
        image: Asset.charityIcon2.image,
        title: L10n.Charity.Slide.Register.title,
        description: format(L10n.Charity.Slide.Register.description)
    )
    
}
