//
//  CharityViewModel.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 19/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

final class CharityViewModel : BaseViewModel {

    private let cards: Cards
    private let charities: Charities
    private let members: Members
    
    enum CharityErrors : Error {
        case undefined
    }

    private func getChosenCharityId() -> Single<Int> {
        return self.cards
            .getCardInfo()
            .map { card in
                card.charityId
            }
    }
    
    func getChosenCharity() -> Single<Charity> {
        return self.getChosenCharityId()
            .flatMap { [weak self] charityId in
                guard let context = self else { return .error(CharityErrors.undefined) }
                return context.charities.getCharity(id: charityId)
            }
    }
    
    func getCharitiesPage(page: Int, name: String? = nil) -> Single<CharityPage> {
        return self.charities.getCharities(page: page, name: name)
    }
    
    func chooseCharity(_ charity: Charity) -> Completable {
        guard
            let member = self.storage.authorizedMember
            else { return .error(CharityErrors.undefined) }
        
        return
            self.members
                .chooseCharity(charity)
                .do(onSuccess: { [weak self] member in
                    self?.storage.authorizedMember = member
                })
                .asCompletable()
    }
    
    func markSeenCharitySeen() {
        self.storage.hasSeenCharityScreen = true
    }
    
    init(storage: Storage, cards: Cards, charities: Charities, members: Members) {
        self.cards = cards
        self.charities = charities
        self.members = members
        
        super.init(storage: storage)
    }
    
}
