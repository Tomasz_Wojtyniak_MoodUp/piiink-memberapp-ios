//
//  CharitySlidesDelegate.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 16/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

protocol CharitySlidesDelegate {
    
    func onCharitySlidesEnd(_ controller: CharitySlidesViewController)
    func onRegisterButtonAction(_ controller: CharitySlidesViewController)
    
}
