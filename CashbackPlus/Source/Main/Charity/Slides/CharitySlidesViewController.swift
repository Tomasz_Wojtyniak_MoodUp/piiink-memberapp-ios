//
//  CharitySlidesViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 16/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import RxSwift
import Atributika

final class CharitySlidesViewController: BaseViewController<CharitySlidesViewModel> {

    @IBOutlet weak var registerButton: PrimaryButton!
    @IBOutlet weak var continueButton: PrimaryButton!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: AttributedLabel!
    @IBOutlet weak var pageControl: SplashPageControl!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var delegate: CharitySlidesDelegate?
    
    private var slides: [Slide] = []
    private var isShowingRegisterButtonOnEnd = false
    var isShowingContinueButtonOnEnd = false
    private var currentSlideIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func showSlide(_ slideIndex: Int) {
        self.currentSlideIndex = slideIndex
        
        let slide = self.slides[slideIndex]
        
        self.applySlide(slide)
        self.pageControl.currentPage = slideIndex
        
        self.registerButton.isHidden = !(self.isLastSlide(slideIndex) && self.isShowingRegisterButtonOnEnd)
        self.continueButton.isHidden = !(self.isLastSlide(slideIndex) && self.isShowingContinueButtonOnEnd && !self.isShowingRegisterButtonOnEnd)
    }
    
    private func applySlide(_ slide: Slide) {
        let fallbackFont = FontConvertible.Font.systemFont(ofSize: 16);
        let normalFont = StyleConstants.Splashes.normalFont ?? fallbackFont;
        let boldFont = StyleConstants.Splashes.boldFont ?? fallbackFont;

        let all = Style.font(normalFont);
        let bold = Style("b").font(boldFont);
        let link = Style("a").font(boldFont);
        
        self.imageView.image = slide.image
        self.titleLabel.text = slide.title
        self.textLabel.attributedText = slide.description.string
            .style(tags: link, bold)
            .styleAll(all)
        
        self.textLabel.isUserInteractionEnabled = true
    }
    
    private func isLastSlide(_ index: Int) -> Bool {
        return (index == slides.count - 1)
    }
    
    private func showNextSlide() {
        guard !self.isLastSlide(self.currentSlideIndex) else { return }
        
        self.showSlide(self.currentSlideIndex + 1)
    }
    
    private func showPrevSlide() {
        guard self.currentSlideIndex != 0 else { return }
        
        self.showSlide(self.currentSlideIndex - 1)
    }
    
    private func setup() {
        self.contentView.isHidden = true
        self.activityIndicator.isHidden = false
        self.viewModel
            .isCardRegistered
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { [weak self] isRegistered in
                guard let context = self else { return }
                context.contentView.isHidden = false
                context.activityIndicator.isHidden = true
                context.slides = [CharitySlides.donate, CharitySlides.family]
                
                if !isRegistered {
                    context.slides += [CharitySlides.register]
                    context.isShowingRegisterButtonOnEnd = true
                }
                
                context.pageControl.numberOfPages = context.slides.count
                context.showSlide(0)
                })
            .disposed(by: self.disposeBag)
        
        self.textLabel.numberOfLines = 0
        self.textLabel.onClick = self.onTextClick
    }
    
    private func onTextClick(label: AttributedLabel, detection: Detection) {
        switch detection.type {
        case .tag(let tag):
            if tag.name == "a", let href = tag.attributes["href"], let url = URL(string: href) {
                UIApplication.shared.open(url)
            }
        default:
            break
        }
    }

    @IBAction func onRegisterButtonAction(_ sender: Any) {
        self.delegate?.onRegisterButtonAction(self)
    }
    
    @IBAction func onContinueButtonAction(_ sender: Any) {
        self.delegate?.onCharitySlidesEnd(self)
    }
    
    @IBAction func onRightSwipeAction(_ sender: UISwipeGestureRecognizer) {
        self.showPrevSlide()
    }
    
    @IBAction func onLeftSwipeAction(_ sender: UISwipeGestureRecognizer) {
        self.showNextSlide()
    }
    
}
