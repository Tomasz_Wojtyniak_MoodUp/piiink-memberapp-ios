//
//  CharitySlidesViewModel.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 19/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

final class CharitySlidesViewModel : BaseViewModel {
    
    private let members: Members
    
    var isCardRegistered: Single<Bool> {
        if self.storage.authorizedMember != nil {
            return .just(true)
        } else {
            return self.members
                .getAuthorizedMember()
                .do(onSuccess: {[weak self] member in
                    self?.storage.authorizedMember = member
                })
                .map { member in true }
                .catchErrorJustReturn(false)
        }
    }
    
    init(storage: Storage, members: Members) {
        self.members = members
        
        super.init(storage: storage)
    }
    
}
