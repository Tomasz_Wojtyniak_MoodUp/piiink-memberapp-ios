//
//  RegistrationDelegate.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 19/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

protocol RegistrationDelegate {
    func onRegisterEnd(_ controller: RegistrationViewController, status: Bool?)
}
