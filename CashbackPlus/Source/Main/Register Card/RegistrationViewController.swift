//
//  RegistrationViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 15/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import WebKit

final class RegistrationViewController: BaseViewController<BaseViewModel>, WKUIDelegate, WKNavigationDelegate {
    
    let onSuccess = "type=onSuccess"
    let cardAlreadyExistsOk = "type=cardAlreadyExistsOK"
    let cardNumberErrorGoBack = "type=cardNumberErrorGoBack"

    @IBOutlet weak var activityIndicatior: UIActivityIndicatorView!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var resultImage: UIImageView!
    @IBOutlet weak var resultHeader: UILabel!
    @IBOutlet weak var resultDescription: UILabel!
    @IBOutlet weak var resultButton: PrimaryButton!
    @IBOutlet weak var resultBox: GenericBoxView!
    
    
    var delegate: RegistrationDelegate?
    var cardNumber: String?
    var isSuccess = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        guard var registartionForm = Configuration.shared.config.registrationForm else { return }
        if let cardNumber = self.cardNumber {
            registartionForm = "\(registartionForm)?cardNumber=\(cardNumber)"
        }
        guard let url = URL(string: registartionForm) else { return }
        self.webView.load(URLRequest(url: url))
        self.webView.addObserver(self, forKeyPath: "URL", options: .new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(WKWebView.url) {
            if self.webView.url?.absoluteString.contains(onSuccess) ?? false {
                self.isSuccess = true
                self.resultBox.isHidden = false
                self.resultImage.image = UIImage(asset: Asset.positiveVote)
                self.resultHeader.text = L10n.Card.Registration.Result.Header.success
                self.resultDescription.text = L10n.Card.Registration.Result.Description.success
                self.resultButton.setTitle(L10n.Card.Registration.Result.Button.success, for: .normal)
            } else if isRegistrationError() {
                self.isSuccess = false
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    private func isRegistrationError() -> Bool {
        return (self.webView.url?.absoluteString.contains(cardAlreadyExistsOk) ?? false) ||
            (self.webView.url?.absoluteString.contains(cardNumberErrorGoBack) ?? false)
    }
    
    private func finish() {
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: .didRegisterCard, object: nil)
        
        self.delegate?.onRegisterEnd(self, status: true)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.activityIndicatior.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.activityIndicatior.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.activityIndicatior.startAnimating()
    }
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let alert = UIAlertController(
            title: L10n.Card.Registration.Web.alertTitle,
            message: message,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: L10n.Card.Registration.errorDefaultAction, style: .default) { _ in
            completionHandler()
        })
        
        self.present(alert, animated: true)
        
    }
    
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        let alert = UIAlertController(
            title: L10n.Card.Registration.Web.alertTitle,
            message: message,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: L10n.Card.Registration.Web.alertCancelAction, style: .default) { _ in
            completionHandler(false)
        })
        
        alert.addAction(UIAlertAction(title: L10n.Card.Registration.Web.alertConfirmAction, style: .default) { _ in
            completionHandler(true)
        })
        
        self.present(alert, animated: true)
    }
    
    @IBAction func onResultButton(_ sender: Any) {
        if isSuccess {
            finish()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
