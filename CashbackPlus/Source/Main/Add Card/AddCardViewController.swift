//
//  AddCardViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 08/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class AddCardViewController: BaseViewController<AddCardViewModel>, QRScannerDelegate {
    
    @IBOutlet weak var cardNumberField: CheckableInputView!
    @IBOutlet weak var pinField: CheckableInputView!
    @IBOutlet weak var saveButton: PrimaryButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var cardNumberEditing: Observable<String>?
    var pinCodeEditing: Observable<String>?
    var cardNumberValidator = false
    
    var previousTextFieldContent: String?
    var previousSelection: UITextRange?
    
    @IBOutlet weak var messageLabel: UILabel!
    
    private enum MessageType {
        case tip
        case error
    }
    
    var isValidForm = false {
        didSet {
            self.saveButton.active = self.isValidForm
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func getCleanCardNumber() -> String? {
        guard let cardNumber = self.cardNumberField.inputField.text else { return nil }
        
        return String(cardNumber
            .filter { letter in letter.isNumber }
        )
    }
    
    private func setupCardNumber() {
        self.cardNumberField.inputField.keyboardType = .numberPad
        
        // Formatter
        self.setupCardNumberFormatter()
        
        // Validator when editing
        self.cardNumberEditing = self.cardNumberField.inputField.rx.controlEvent(.editingChanged)
            .map { [weak self] in self?.getCleanCardNumber() ?? "" }
        
        self.cardNumberEditing?
            .subscribe(onNext: { [weak self] cardNumber in
                guard let context = self else { return }
                
                let correctStatus = CardHelper.isCorrectCardNumber(cardNumber)
                
                if correctStatus {
                    context.cardNumberValidator = true
                }
                
                if context.cardNumberValidator {
                    context.cardNumberField.correctStatus = correctStatus
                }
            })
            .disposed(by: self.disposeBag)
        
        // Validator when focus out
        self.cardNumberField.inputField.rx.controlEvent(.editingDidEnd)
            .map { [weak self] in self?.getCleanCardNumber() ?? "" }
            .subscribe(onNext: { [weak self] cardNumber in
                guard let context = self else { return }
                
                context.cardNumberValidator = true
                context.cardNumberField.correctStatus = CardHelper.isCorrectCardNumber(cardNumber)
            })
            .disposed(by: self.disposeBag)
    }
    
    private func setupPinCode() {
        self.pinField.inputField.keyboardType = .numberPad
        
        // Formatter
        self.pinCodeEditing = self.pinField.inputField.rx.controlEvent(.editingChanged)
            .map { [weak self] in self?.pinField.inputField.text ?? String() }
            .map { pinField in String(pinField.prefix(Constants.CardHelper.pinCodeLength)) }
        
        self.pinCodeEditing?
            .subscribe(onNext: { [weak self] pinCode in
                self?.pinField.inputField.text = pinCode
            })
            .disposed(by: self.disposeBag)
    }
    
    private func setupFormValidator() {
        guard
            let cardNumberEditing = self.cardNumberEditing,
            let pinCodeEditing = self.pinCodeEditing
            else { return }
        
        // Global form validator
        Observable.combineLatest(cardNumberEditing, pinCodeEditing)
            .subscribe(onNext: {[weak self] (cardNumber, pinCode) in
                guard let context = self else { return }
                var isValid = true
                
                if cardNumber.isEmpty || pinCode.isEmpty {
                    isValid = false
                }
                
                if !CardHelper.isCorrectCardNumber(cardNumber) {
                    isValid = false
                }
                
                if !CardHelper.isCorrectCardPin(pinCode) {
                    isValid = false
                }
                
                context.isValidForm = isValid
            })
            .disposed(by: self.disposeBag)
    }
    
    func setup() {
        self.setupCardNumber()
        self.setupPinCode()
        self.setupFormValidator()
        
        self.activityIndicator.stopAnimating()
        self.setMessage(L10n.AddCard.Message.tip, type: .tip)
    }
    
    private func setMessage(_ msg: String?, type: MessageType) {
        self.messageLabel.text = msg
        
        switch type {
        case .tip:
            self.messageLabel.textColor = ColorName.defaultTextColor.color
            break
        case .error:
            self.messageLabel.textColor = ColorName.errorTextColor.color
            break
        }
    }
    
    @IBAction func onSaveButtonAction(_ sender: Any) {
        guard
            self.isValidForm,
            let cardNumber = self.getCleanCardNumber(),
            let pinCode = self.pinField.inputField.text
            else {
                self.setMessage(L10n.AddCard.Message.Error.empty, type: .error)
                return
            }
        
        self.activityIndicator.startAnimating()
        self.setMessage("", type: .tip)
        
        self.viewModel
            .loginAuth(
                cardNumber: cardNumber,
                cardPin: pinCode
            )
            .observeOn(MainScheduler.instance)
            .subscribe(
                onCompleted: { [weak self] in
                    guard let context = self else { return }
                    context.navigationController?.popViewController(animated: true)
                    NotificationCenter.default.post(name: .didAddCard, object: nil)
                },
                onError: { [weak self] error in
                    guard
                        let context = self
                        else { return }
                    
                    context.activityIndicator.stopAnimating()
                    context.setMessage(L10n.AddCard.Message.Error.generic, type: .error)
                }
            )
            .disposed(by: self.disposeBag)
    }
    
    @IBAction func onScanTapAction(_ sender: Any) {
        let controller = self.instantiate(QRScannerViewController.self)
        controller.delegate = self
        self.show(controller, sender: true)
    }
    
    func onQRScanner(_ scanner: QRScannerViewController, value: String) {
        guard CardHelper.isCorrectCardNumber(value) else { return }
        
        self.cardNumberField.inputField.text = CardHelper.formatCardNumber(value)
        self.cardNumberField.inputField.sendActions(for: .editingChanged)
        self.cardNumberField.inputField.sendActions(for: .editingDidEnd)
        
        self.navigationController?.popToViewController(self, animated: true)
    }
}
