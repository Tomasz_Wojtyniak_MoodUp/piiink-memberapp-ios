//
//  AddCardViewModel.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 12/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

final class AddCardViewModel : BaseViewModel {
    
    private let disposeBag = DisposeBag()
    private let auth: Auth
    private let members: Members
    
    enum AddCardErrors : Error {
        case undefined
    }
    
    private func removeCard() -> Completable {
        guard let authData = self.storage.auth.value else { return .empty() }
        
        return self.auth
            .logoutCard(auth: authData)
            .do(onCompleted: { [weak self] in
                guard let context = self else { return }
                
                context.storage.authorizedMember = nil
                context.storage.cardNumber = nil
                context.storage.store(auth: nil)
            })
    }
    
    func loginAuth(cardNumber: String, cardPin: String) -> Completable {
        return self.removeCard()
            .andThen(
                self.auth.loginCard(cardNumber: cardNumber, cardPin: cardPin)
            )
            .do(onSuccess: { [weak self] authData in
                guard let context = self else { return }
                
                context.storage.store(auth: authData)
                context.storage.cardNumber = cardNumber
                
                context.prefetchAuthorizedMember()
                }
            )
            .asCompletable()
    }
    
    private func prefetchAuthorizedMember() {
        self.members
            .getAuthorizedMember()
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: {[weak self] member in
                self?.storage.authorizedMember = member
            })
            .disposed(by: self.disposeBag)
    }
    
    init(storage: Storage, auth: Auth, members: Members) {
        self.auth = auth
        self.members = members
        
        super.init(storage: storage)
    }
}
