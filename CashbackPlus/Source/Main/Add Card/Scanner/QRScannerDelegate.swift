//
//  QRScannerDelegate.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 14/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

protocol QRScannerDelegate {
    
    func onQRScanner(_ scanner: QRScannerViewController, value: String)
    
}
