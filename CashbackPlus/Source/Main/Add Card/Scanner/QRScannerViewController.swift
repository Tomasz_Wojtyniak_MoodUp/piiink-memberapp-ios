//
//  QRScannerViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 14/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import AVFoundation

final class QRScannerViewController: BaseViewController<BaseViewModel>, AVCaptureMetadataOutputObjectsDelegate {

    private let captureSession = AVCaptureSession()
    
    @IBOutlet weak var previewView: UIView!
    
    var delegate: QRScannerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.resolvePermissions()
    }
    
    private func resolvePermissions() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            self.onAccess()
            break
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { [weak self] granted in
                guard let context = self else { return }
                granted ? context.onAccess() : context.onNotAccess()
            }
            break
            
        case .denied, .restricted:
            self.onNotAccess()
            break
                
        @unknown default:
            self.onNotAccess()
            break
        }
    }
    
    private func onAccess() {
        DispatchQueue.main.async {
            self.setupCaptureSession()
        }
    }
    
    private func onNotAccess() {
        let alert = UIAlertController(
            title: L10n.Card.Scanner.errorTitle,
            message: L10n.Card.Scanner.errorMsg,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: L10n.Card.Scanner.errorButtonSettings, style: .default) { [weak self] _ in
            if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(settingsURL, options: [:])
            }
            
            self?.navigationController?.popViewController(animated: true)
        })
        
        alert.addAction(UIAlertAction(title: L10n.Card.Scanner.errorButtonCancel, style: .cancel) { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        })
        
        self.present(alert, animated: true)
    }

    private func setupCaptureSession() {
        self.captureSession.beginConfiguration()
        
        self.setupInput()
        self.setupOutput()
        self.captureSession.commitConfiguration()
        
        self.setupPreview()
        
        self.captureSession.startRunning()
    }
    
    private func setupInput() {
        let videoDeviceDefault = AVCaptureDevice.default(
            .builtInWideAngleCamera,
            for: .video,
            position: .back
        )
        
        guard
            let videoDevice = videoDeviceDefault,
            let videoDeviceInput = try? AVCaptureDeviceInput(device: videoDevice),
            self.captureSession.canAddInput(videoDeviceInput)
            else { return }
        
        self.captureSession.addInput(videoDeviceInput)
        
    }
    
    private func setupOutput() {
        let captureMetadataOutput = AVCaptureMetadataOutput()
        
        guard self.captureSession.canAddOutput(captureMetadataOutput) else { return }
        self.captureSession.addOutput(captureMetadataOutput)
        
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = [.qr]
    }
    
    private func setupPreview() {
        let videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        videoPreviewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer.frame = self.previewView.layer.bounds
        self.previewView.layer.addSublayer(videoPreviewLayer)
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        guard metadataObjects.count > 0 else { return }
        
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == .qr, let value = metadataObj.stringValue {
            self.delegate?.onQRScanner(self, value: value)
        }
    }
    
}
