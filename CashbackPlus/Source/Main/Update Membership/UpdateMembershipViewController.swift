//
//  UpdateMembershipViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 09/09/2020.
//  Copyright © 2020 Maciej Olejnik. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

final class UpdateMembershipViewController: BaseViewController<UpdateMembershipViewModel> {

    @IBOutlet weak var firstName: CheckableInputView!
    @IBOutlet weak var lastName: CheckableInputView!
    @IBOutlet weak var phoneNumber: CheckableInputView!
    @IBOutlet weak var email: CheckableInputView!
    @IBOutlet weak var postalCode: CheckableInputView!
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var submitButton: PrimaryButton!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    private typealias Validator = (String) -> Bool
    private var fieldsStatus: BehaviorRelay<[String: Bool?]> = BehaviorRelay(value: [:])
    
    public var member: Member?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let backButton = UIBarButtonItem(
            title: "",
            style: .plain,
            target: self,
            action: #selector(self.onBackAction)
        )
        
        backButton.image = Asset.chevronLeft.image
        
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.title = L10n.Card.Settings.changeCard
    }
    
    @objc func onBackAction() {
        let isSomeFieldValid = self.fieldsStatus.value
            .compactMap { $0.value }
            .contains { $0 }
        
        if isSomeFieldValid {
            let alert = UIAlertController(
                title: L10n.Recommend.BackAlert.title,
                message: L10n.Recommend.BackAlert.description,
                preferredStyle: .alert
            )
            
            alert.addAction(UIAlertAction(title: L10n.Common.Action.cancel, style: .cancel))
            alert.addAction(UIAlertAction(title: L10n.Common.Action.ok, style: .default) { [weak self] _ in
                self?.goBack()
            })
            
            self.present(alert, animated: true)
        } else {
            self.goBack()
        }
    }
    
    private func goBack() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setup() {
        self.showErrorText(nil)
        self.submitButton.active = true
        self.activity.isHidden = true;
        self.setupInputs()
        self.setupSubmitButton()
    }
    
    private func setupInputs() {
        let This = type(of: self)
        
        self.firstName.inputField.text = member?.firstName
        self.lastName.inputField.text = member?.lastName
        self.phoneNumber.inputField.text = member?.phone
        self.email.inputField.text = member?.email
        self.postalCode.inputField.text = member?.postalCode
        
        self.applyValidators(inputView: self.firstName, validators: [])
        self.applyValidators(inputView: self.lastName, validators: [])
        self.applyValidators(inputView: self.phoneNumber, validators: [This.isPhoneValidator])
        self.applyValidators(inputView: self.email, validators: [This.isEmailValidator])
        self.applyValidators(inputView: self.postalCode, validators: [])
    }
    
    private func setupSubmitButton() {
        self.fieldsStatus
            .subscribe(onNext: { [weak self] status in
                guard let context = self else { return }
                
                let areAllFieldsValid = status
                    .compactMap { $0.value }
                    .allSatisfy { $0 }
                
                context.submitButton.active = areAllFieldsValid
            })
            .disposed(by: self.disposeBag)
        
        self.fieldsStatus.accept([:])
    }
    
    private func showErrorText(_ text: String?) {
        self.errorLabel.text = text
        self.errorLabel.isHidden = (text == nil)
    }
    
    private func applyValidators(inputView: CheckableInputView, validators: [Validator], allowEmpty: Bool = false) {
        guard
            let inputField = inputView.inputField,
            let placeholder = inputView.placeholder
            else { return }
        
        if allowEmpty {
            self.setValidStatus(nil, id: placeholder)
        } else {
            self.setValidStatus(false, id: placeholder)
        }
        
        let editingEvent = inputField.rx.controlEvent(.editingChanged)
        
        editingEvent
            .map { inputField.text }
            .subscribe(onNext: { [weak self] text in
                guard let context = self else { return }
                
                if let text = text, !text.isEmpty {
                    let isValid = validators.map { $0(text) }.allSatisfy { $0 }
                    context.setValidStatus(isValid, id: placeholder)
                } else {
                    if allowEmpty {
                        context.setValidStatus(nil, id: placeholder)
                    } else {
                        context.setValidStatus(false, id: placeholder)
                    }
                }
            })
            .disposed(by: self.disposeBag)
        
        editingEvent
            .throttle(.milliseconds(1500), scheduler: MainScheduler.instance)
            .skip(1)
            .map { inputField.text }
            .subscribe(onNext: { [weak self] text in
                guard let context = self else { return }
                
                let isValid = context.fieldsStatus.value[placeholder] ?? nil
                inputView.correctStatus = isValid
            })
            .disposed(by: self.disposeBag)
    }

    private func setValidStatus(_ status: Bool?, id: String) {
        var updatedStatus = self.fieldsStatus.value
        updatedStatus.updateValue(status, forKey: id)
        self.fieldsStatus.accept(updatedStatus)
    }

    
    @IBAction func onAcceptButton(_ sender: Any) {
        let areAllFieldsValid = self.fieldsStatus.value
            .compactMap { $0.value }
            .allSatisfy { $0 }
        
        if areAllFieldsValid {
            let member = Member(
                id: nil,
                firstName: firstName.inputField.text!,
                lastName: lastName.inputField.text!,
                email: email.inputField.text!,
                phone: phoneNumber.inputField.text!,
                postalCode: postalCode.inputField.text!
            )
            
            self.activity.isHidden = false
            
            self.viewModel
                .updateMembership(member)
                .subscribeOn(MainScheduler.instance)
                .subscribe(onCompleted: { [weak self] in
                    self?.activity.isHidden = true
                    
                    // Info: It prevents weird crashes
                    DispatchQueue.main.async {
                        self?.goBack()
                    }
                }, onError: { [weak self] _ in
                    self?.activity.isHidden = true
                })
                .disposed(by: self.disposeBag)
        }
    }
    
    
}
