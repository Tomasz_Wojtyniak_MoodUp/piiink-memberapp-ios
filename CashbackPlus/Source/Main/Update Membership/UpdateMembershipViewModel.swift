//
//  UpdateMembershipViewModel.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 09/09/2020.
//  Copyright © 2020 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

final class UpdateMembershipViewModel : BaseViewModel {

    private let members: Members
    
    func updateMembership(_ member: Member) -> Completable {
        return
            self.members
                .updateMember(member)
                .do(onSuccess: { [weak self] member in
                    self?.storage.authorizedMember = member
                })
                .asCompletable()
    }
    
    init(storage: Storage, members: Members) {
        self.members = members
        super.init(storage: storage)
    }
    
}
