//
//  UpdateMembershipViewController+Validators.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 09/09/2020.
//  Copyright © 2020 Maciej Olejnik. All rights reserved.
//

import Foundation
import Validator

extension UpdateMembershipViewController {
    
    static func isEmailValidator(text: String) -> Bool {
        return Constants.Validators.emailPredicate.evaluate(with: text)
    }
    
    static func isPhoneValidator(text: String) -> Bool {
        return Constants.Validators.phonePredicate.evaluate(with: text)
    }
    
}
