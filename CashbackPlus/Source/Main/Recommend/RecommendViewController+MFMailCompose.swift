//
//  RecommendViewController+MFMailCompose.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 28/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import MessageUI

extension RecommendViewController : MFMailComposeViewControllerDelegate {
    
    private func fillMessageBody(
            name: String,
            merchantType: String,
            address: String,
            contactPersonName: String,
            phone: String,
            email: String,
            memberNumber: String
        ) -> String {
        
        return L10n.Recommend.Email.body(
            name,
            merchantType,
            address,
            contactPersonName,
            phone,
            email,
            memberNumber
        )
    }
    
    private func formatText(_ field: CheckableInputView) -> String {
        let placeholder = Constants.Mail.emptyFieldPlaceholder
        
        guard let text = field.inputField.text else { return placeholder }
        return text.isEmpty ? placeholder : text
    }
    
    private func createMessageBody() -> String {
        return self.fillMessageBody(
            name:              self.formatText(self.merchantName),
            merchantType:      self.formatText(self.bussinessType),
            address:           self.formatText(self.address),
            contactPersonName: self.formatText(self.contactName),
            phone:             self.formatText(self.phoneNumber),
            email:             self.formatText(self.email),
            memberNumber:      self.viewModel.storage.cardNumber ?? ""
        )
    }
    
    private func generateEmail() -> MFMailComposeViewController {
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = self
        mail.setToRecipients([Constants.Mail.addressForRecommendMerchant])
        mail.setSubject(L10n.Recommend.Email.subject)
        mail.setMessageBody(self.createMessageBody(), isHTML: true)
        return mail
    }
    
    private func generateLink() -> URL? {
        guard
            var components = URLComponents(string: "mailto:\(Constants.Mail.addressForRecommendMerchant)")
            else { return nil }
        
        components.queryItems = [
            .init(name: "subject", value: L10n.Recommend.Email.subject),
            .init(name: "body", value: self.createMessageBody())
        ]
        
        return try? components.asURL()
    }
    
    private func showMailServiceAlert() {
        let alert = UIAlertController(
            title: L10n.Recommend.MailFail.title,
            message: L10n.Recommend.MailFail.description,
            preferredStyle: .alert
        )
        
        alert.addAction(UIAlertAction(title: L10n.Common.Action.ok, style: .default))
        
        self.present(alert, animated: true)
    }
    
    func showEmailController() {
        if MFMailComposeViewController.canSendMail() {
            let mailViewController = self.generateEmail()
            present(mailViewController, animated: true)
        } else {
            if let link = generateLink() {
                UIApplication.shared.open(link, options: [:]) { [weak self] result in
                    if result {
                        self?.dismiss(animated: true)
                    } else {
                        self?.showMailServiceAlert()
                    }
                }
            } else {
                self.showMailServiceAlert()
            }
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: false) {
            controller.dismiss(animated: true)
        }
    }
    
}
