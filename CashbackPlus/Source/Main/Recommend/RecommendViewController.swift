//
//  RecommendViewController.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 27/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

final class RecommendViewController: BaseViewController<BaseViewModel> {

    @IBOutlet weak var merchantName: CheckableInputView!
    @IBOutlet weak var bussinessType: CheckableInputView!
    @IBOutlet weak var address: CheckableInputView!
    @IBOutlet weak var contactName: CheckableInputView!
    @IBOutlet weak var phoneNumber: CheckableInputView!
    @IBOutlet weak var email: CheckableInputView!
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var submitButton: PrimaryButton!
    
    private typealias Validator = (String) -> Bool
    private var fieldsStatus: BehaviorRelay<[String: Bool?]> = BehaviorRelay(value: [:])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let backButton = UIBarButtonItem(
            title: "",
            style: .plain,
            target: self,
            action: #selector(self.onBackAction)
        )
        
        backButton.image = Asset.chevronLeft.image
        
        self.navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func onBackAction() {
        let isSomeFieldValid = self.fieldsStatus.value
            .compactMap { $0.value }
            .contains { $0 }
        
        if isSomeFieldValid {
            let alert = UIAlertController(
                title: L10n.Recommend.BackAlert.title,
                message: L10n.Recommend.BackAlert.description,
                preferredStyle: .alert
            )
            
            alert.addAction(UIAlertAction(title: L10n.Common.Action.cancel, style: .cancel))
            alert.addAction(UIAlertAction(title: L10n.Common.Action.ok, style: .default) { [weak self] _ in
                self?.goBack()
            })
            
            self.present(alert, animated: true)
        } else {
            self.goBack()
        }
    }
    
    private func goBack() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.popViewController(animated: true)
    }
    
    private func setup() {
        self.showErrorText(nil)
        self.submitButton.active = false
        self.setupInputs()
        self.setupSubmitButton()
    }
    
    private func setupInputs() {
        let This = type(of: self)
        
        self.applyValidators(inputView: self.merchantName, validators: [])
        self.applyValidators(inputView: self.email, validators: [This.isEmailValidator], allowEmpty: true)
    }
    
    private func setupSubmitButton() {
        self.fieldsStatus
            .subscribe(onNext: { [weak self] status in
                guard let context = self else { return }
                
                let areAllFieldsValid = status
                    .compactMap { $0.value }
                    .allSatisfy { $0 }
                
                context.submitButton.active = areAllFieldsValid
            })
            .disposed(by: self.disposeBag)
    }
    
    private func showErrorText(_ text: String?) {
        self.errorLabel.text = text
        self.errorLabel.isHidden = (text == nil)
    }
    
    private func applyValidators(inputView: CheckableInputView, validators: [Validator], allowEmpty: Bool = false) {
        guard
            let inputField = inputView.inputField,
            let placeholder = inputView.placeholder
            else { return }
        
        if allowEmpty {
            self.setValidStatus(nil, id: placeholder)
        } else {
            self.setValidStatus(false, id: placeholder)
        }
        
        let editingEvent = inputField.rx.controlEvent(.editingChanged)
        
        editingEvent
            .map { inputField.text }
            .subscribe(onNext: { [weak self] text in
                guard let context = self else { return }
                
                if let text = text, !text.isEmpty {
                    let isValid = validators.map { $0(text) }.allSatisfy { $0 }
                    context.setValidStatus(isValid, id: placeholder)
                } else {
                    if allowEmpty {
                        context.setValidStatus(nil, id: placeholder)
                    } else {
                        context.setValidStatus(false, id: placeholder)
                    }
                }
            })
            .disposed(by: self.disposeBag)
        
        editingEvent
            .throttle(.milliseconds(1500), scheduler: MainScheduler.instance)
            .skip(1)
            .map { inputField.text }
            .subscribe(onNext: { [weak self] text in
                guard let context = self else { return }
                
                let isValid = context.fieldsStatus.value[placeholder] ?? nil
                inputView.correctStatus = isValid
            })
            .disposed(by: self.disposeBag)
    }

    private func setValidStatus(_ status: Bool?, id: String) {
        var updatedStatus = self.fieldsStatus.value
        updatedStatus.updateValue(status, forKey: id)
        self.fieldsStatus.accept(updatedStatus)
    }

    
    @IBAction func onAcceptButton(_ sender: Any) {
        let areAllFieldsValid = self.fieldsStatus.value
            .compactMap { $0.value }
            .allSatisfy { $0 }
        
        if areAllFieldsValid {
            self.showEmailController()
        }
    }
    
    
}
