//
//  RecommendViewController+Validators.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 27/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

extension RecommendViewController {
    
    static func isEmailValidator(text: String) -> Bool {
        return Constants.Validators.emailPredicate.evaluate(with: text)
    }
    
}
