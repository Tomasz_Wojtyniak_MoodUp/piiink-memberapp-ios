//
//  BaseViewModel.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 18/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

class BaseViewModel {

    var storage: Storage
    
    init(storage: Storage) {
        self.storage = storage
    }
    
}
