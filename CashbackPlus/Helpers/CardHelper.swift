//
//  Validators.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 09/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

enum CardHelper {
    
    static func isCorrectCardNumber(_ number: String) -> Bool {
        return number.range(of: Constants.CardHelper.tycCardNumberRegex, options: .regularExpression, range: nil, locale: nil) != nil
    }
    
    static func isCorrectCardPin(_ number: String) -> Bool {
        return number.count == Constants.CardHelper.pinCodeLength
    }
    
    static func formatCardNumber(_ number: String) -> String {
        return String(
            number
                .enumerated()
                .map { $0 > 0 && $0 % 4 == 0 ? [" ", $1] : [$1] }
                .joined()
        )
    }
    
    static func formatToHiddenCardNumber(_ number: String, showCount: Int = 4) -> String {
        return formatCardNumber(
            String(
                String(repeating: "*", count: Constants.CardHelper.tycCardNumberLength - showCount) +
                number.suffix(showCount)
            )
        )
    }
}
