//
//  CacheKeys.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 12/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

extension Cache {
    enum Key : String {
        case regions = "regions"
        case currentRegion = "current_region"
        case authData = "auth"
        case authorizedMember = "authorized_member"
        case cardNumber = "card_number"
        case hasSeenCharityScreen = "has_seen_charity_screen"
        case hasSeenPrivacyPolicy = "has_seen_privacy_policy"
        case isCardInfoHidden = "hidden_card_info"
        case hasSeenProfile = "hasSeenProfile"
    }
}


