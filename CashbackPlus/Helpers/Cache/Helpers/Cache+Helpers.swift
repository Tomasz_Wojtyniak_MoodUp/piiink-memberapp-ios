//
//  Cache+Helpers.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 12/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

extension Cache {
    private static let encoder = JSONEncoder()
    private static let decoder = JSONDecoder()
    
    static func loadJson<Obj : Decodable>(_: Obj.Type, _ key: Cache.Key?) -> Obj? {
        guard
            let id = key?.rawValue,
            let data = UserDefaults.standard.data(forKey: id)
            else { return nil }
        
       return try? decoder.decode(Obj.self, from: data)
    }
    
    static func load<Obj>(_: Obj.Type, _ key: Cache.Key?) -> Obj? {
        guard
            let id = key?.rawValue
            else { return nil }
        
        return UserDefaults.standard.object(forKey: id) as? Obj
    }
    
    static func saveJson<Obj : Encodable>(_ obj: Obj, _ key: Cache.Key?) {
        guard
            let id = key?.rawValue
            else { return }
        
        if let data = try? encoder.encode(obj) {
            UserDefaults.standard.set(data, forKey: id)
        } else {
            UserDefaults.standard.removeObject(forKey: id)
        }
        UserDefaults.standard.synchronize()
    }
    
    static func save<Obj>(_ obj: Obj?, _ key: Cache.Key?) {
        guard
            let id = key?.rawValue
            else { return }
        
        if let obj = obj {
            UserDefaults.standard.set(obj, forKey: id)
        } else {
            UserDefaults.standard.removeObject(forKey: id)
        }
        UserDefaults.standard.synchronize()
    }
}
