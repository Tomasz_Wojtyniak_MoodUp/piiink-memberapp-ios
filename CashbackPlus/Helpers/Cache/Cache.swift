//
//  Cache.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 02/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay
import CoreLocation

final class Cache : Storage {
    
    let currentRegion: BehaviorRelay<Region?>
    var regions: [Region]
    
    var auth: BehaviorRelay<AuthData?>
    
    var authorizedMember: Member? {
        didSet {
            Cache.saveJson(self.authorizedMember, .authorizedMember)
        }
    }
    
    var cardNumber: String? {
        didSet {
            Cache.save(self.cardNumber, .cardNumber)
        }
    }
    
    var isCardInfoHidden: Bool {
        didSet {
            Cache.save(self.isCardInfoHidden, .isCardInfoHidden)
        }
    }
    
    var hasSeenCharityScreen: Bool {
        didSet {
            Cache.save(self.hasSeenCharityScreen, .hasSeenCharityScreen)
        }
    }
    
    var hasSeenPrivacyPolicy: Bool {
        didSet {
            Cache.save(self.hasSeenPrivacyPolicy, .hasSeenPrivacyPolicy)
        }
    }
    
    var hasSeenProfile: Bool {
        didSet {
            Cache.save(self.hasSeenProfile, .hasSeenProfile)
        }
    }
    
    init() {
        self.currentRegion = BehaviorRelay(value: Cache.loadJson(Region.self, .currentRegion))
        self.regions = Cache.loadJson([Region].self, .regions) ?? []
        
        self.auth = BehaviorRelay(value: Cache.loadJson(AuthData.self, .authData))
        self.authorizedMember = Cache.loadJson(Member.self, .authorizedMember)
        self.cardNumber = Cache.load(String.self, .cardNumber)
        
        self.hasSeenCharityScreen = Cache.load(Bool.self, .hasSeenCharityScreen) ?? false
        self.hasSeenPrivacyPolicy = Cache.load(Bool.self, .hasSeenPrivacyPolicy) ?? false
        self.hasSeenProfile = Cache.load(Bool.self, .hasSeenProfile) ?? false
        
        self.isCardInfoHidden = Cache.load(Bool.self, .isCardInfoHidden) ?? Constants.Application.defaultHiddenCredentials
    }
    
    // MARK: Storing functions
    
    func store(auth: AuthData?) {
        self.auth.accept(auth)
        Cache.saveJson(auth, .authData)
    }
    
    func store(chosenRegion: Region) {
        self.currentRegion.accept(chosenRegion)
        Cache.saveJson(chosenRegion, .currentRegion)
    }
    
    func store(regions: [Region]) {
        self.regions = regions
        Cache.saveJson(self.regions, .regions)
    }
}
