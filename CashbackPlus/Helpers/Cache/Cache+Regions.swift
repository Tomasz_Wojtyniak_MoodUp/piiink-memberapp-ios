//
//  Cache+Regions.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 12/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

extension Cache : Regions {
    
    func getRegions() -> Single<[Region]> {
        return .just(self.regions)
    }
    
}
