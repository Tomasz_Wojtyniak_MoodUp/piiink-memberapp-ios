//
//  RemoteRepository.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 24/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation

extension RemoteRepository : Charities {
    
    func getCharities(page: Int, name: String? = nil) -> Single<CharityPage> {
        var parameters: [String: Any] =  [
            "page": page
        ]
        
        if let name = name, !name.isEmpty {
            parameters["name"] = name
        }
        
        return self.newBackendClient.singleRequest(Endpoint(
            method: .get,
            path: "charities",
            parameters: parameters
        ))
    }
    
    func getCharity(id: Int) -> Single<Charity> {
        return self.newBackendClient.singleRequest(Endpoint(
            method: .get,
            path: "charities/" + id.description
        ))
    }
    
}
