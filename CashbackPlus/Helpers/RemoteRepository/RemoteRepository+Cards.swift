//
//  RemoteRepository+Wallets.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 21/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

extension RemoteRepository : Cards {
    
    func getBalance(page: Int) -> Single<CardsBalanceResult> {
        return self.newBackendClient.singleRequest(Endpoint(
            method: .get,
            path: "cards/balance",
            parameters: [
                "page": page
            ]
        ))
    }
    
    func getCardInfo() -> Single<Card> {
        return self.newBackendClient.singleRequest(Endpoint(
            method: .get,
            path: "cards/self"
        ))
    }

}
