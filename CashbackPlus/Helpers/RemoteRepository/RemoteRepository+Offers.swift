//
//  RemoteRepository.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 24/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation

extension RemoteRepository : Merchants {
    
    func getBestOffers() -> Single<[Merchant]> {
        return self.wordpressGet("get_best_deals")
    }
    
    func getCategories() -> Single<[Category]> {
        return self.wordpressGet("get_categories")
    }
    
    func getNearbyOffers(_ location: CLLocation) -> Single<[Merchant]> {
        return self.wordpressGet("get_nearby", parameters: [
            "lng": location.coordinate.longitude.description,
            "lat": location.coordinate.latitude.description,
        ])
    }
    
    func getNewMerchants() -> Single<[Merchant]> {
        return self.wordpressGet("get_new_merchants")
    }
    
    func getAllOffers() -> Single<[Merchant]> {
        return self.wordpressGet("get_merchants")
    }
    
    func getDiscount(id: Int, timeZone: String) -> Single<MerchantDiscount> {
        let parameters: [String: Any] =  [
            "timeZone": timeZone
        ]
        
        return self.newBackendClient.singleRequest(Endpoint(
            method: .get,
            path: "/discounts/\(id)",
            parameters: parameters
        ))
    }
    
    func search(query: String) -> Single<[Merchant]> {
        return self.wordpressGet("search", parameters: [
            "query": query,
            ])
    }
}
