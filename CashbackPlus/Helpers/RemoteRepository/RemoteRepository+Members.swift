//
//  RemoteRepository.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 24/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation
import Alamofire

extension RemoteRepository : Members {
    
    func getAuthorizedMember() -> Single<Member> {
        return self.newBackendClient.singleRequest(Endpoint(
            method: .get,
            path: "members/self"
        ))
    }
    
    func updateMember(_ member: Member) -> Single<Member> {
        return self.newBackendClient.singleRequest(Endpoint(
            method: .put,
            path: "members/self",
            parameters: [
                "firstName": member.firstName,
                "lastName": member.lastName,
                "email": member.email,
                "phone": member.phone,
                "postalCode": member.postalCode,
            ],
            parameterEncoding: JSONEncoding.default
        ))
    }
    
    func chooseCharity(_ charity: Charity) -> Single<Member> {
        return self.newBackendClient.singleRequest(Endpoint(
            method: .put,
            path: "members/self",
            parameters: [
                "charityId": charity.id
            ],
            parameterEncoding: JSONEncoding.default
        ))
    }
    
}
