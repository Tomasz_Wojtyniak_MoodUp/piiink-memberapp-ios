//
//  RemoteRepository.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 24/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

final class RemoteRepository {
    
    let wordpressClient: ClientProtocol
    let newBackendClient: ClientProtocol
    
    let disposeBag = DisposeBag()
    
    init(cache: Storage, wordpressClient: ClientProtocol, newBackendClient: ClientProtocol) {
        self.wordpressClient = wordpressClient
        self.newBackendClient = newBackendClient
        self.newBackendClient.setRefreshTokenMethod(method: self.refreshToken(cache: cache))
        
        cache.auth
            .subscribe(onNext: { [weak self] auth in
                guard let context = self else { return }
                context.newBackendClient.setToken(auth?.token)
            })
            .disposed(by: self.disposeBag)
    }
    
    func wordpressGet<Response: Decodable>(_ path: String, parameters: Parameters = [:]) -> Single<Response> {
        var parameters = parameters
        parameters["action"] = path
        
        return self.wordpressClient.singleRequest(Endpoint(
            method: .get,
            path: "",
            parameters: parameters
        ))
    }
}
