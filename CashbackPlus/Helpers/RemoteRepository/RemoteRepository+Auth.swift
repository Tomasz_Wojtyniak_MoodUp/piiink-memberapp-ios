//
//  RemoteRepository.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 24/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation

extension RemoteRepository : Auth {
    
    func loginCard(cardNumber: String, cardPin: String) -> Single<AuthResponse> {
        return self.newBackendClient.singleRequest(Endpoint<AuthResponse>(
            method: .post,
            path: "auth/login",
            authenticate: (cardNumber, cardPin)
        ))
        .do(onSuccess: { (authResponse) in
            self.newBackendClient.setToken(authResponse.token)
        })
    }
    
    func logoutCard(auth: AuthResponse) -> Completable {
        return self.newBackendClient.completableRequest(Endpoint(
            method: .post,
            path: "auth/logout",
            parameters: [
                "refreshToken": auth.refreshToken,
                "accessToken": auth.token
            ]
        ))
        .do(onCompleted: {
            self.newBackendClient.setToken(nil)
        })
    }
    
    func refreshToken(cache: Storage) -> Completable {
        return self.newBackendClient.singleRequest(Endpoint<AuthResponse>(
            method: .post,
            path: "auth/refresh"
        ))
        .do(onSubscribe: {
            self.newBackendClient.setToken(cache.auth.value?.refreshToken ?? "")
        })
        .do(onSuccess: { (authResponse) in
            self.newBackendClient.setToken(authResponse.token)
            cache.store(auth: authResponse)
        })
        .flatMapCompletable({ (authResponse) -> Completable in
            return Completable.empty()
        })
    }
}
