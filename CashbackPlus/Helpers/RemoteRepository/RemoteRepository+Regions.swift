//
//  RemoteRepository.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 24/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation

extension RemoteRepository : Regions {
    
    func getRegions() -> Single<[Region]> {
        return self.wordpressGet("get_regions")
    }
    
}
