//
//  String+NSRanges.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 05/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

extension String {
    
    func getRanges(of separator: String) -> [NSRange] {
        let components = self.components(separatedBy: separator)
        var result: [NSRange] = []
        
        for componentIndex in stride(from: 1, to: components.count, by: 2) {
            let component = components[componentIndex]
            let range = self.range(of: component)
            
            if let range = range {
                result.append(
                    NSRange(location: self.distance(from: self.startIndex, to: range.lowerBound) - separator.count * componentIndex, length: component.count)
                )
            }
        }
        
        return result
    }
    
}
