//
//  LocationManager.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 25/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxRelay
import CoreLocation

final class LocationManager : NSObject, CLLocationManagerDelegate, LocationService, LocationProvider {
    
    private let locationManager = CLLocationManager()
    private let location: BehaviorRelay<CLLocation>
    var access: BehaviorRelay<Bool>
    
    init(location: CLLocation) {
        self.location = BehaviorRelay<CLLocation>(value: location)
        self.access = BehaviorRelay<Bool>(value: LocationManager.hasAccess(CLLocationManager.authorizationStatus()))
        
        super.init()
        
        self.locationManager.delegate = self
    }
    
    func getLocation() -> BehaviorRelay<CLLocation> {
        return self.location
    }
    
    func refreshLocation() {
        self.locationManager.requestLocation()
    }
    
    func requestAccess() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            self.locationManager.requestAlwaysAuthorization()
            break
        case .restricted, .denied :
            if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
            }
            break
        default:
            break
        }
    }
    
    private static func hasAccess(_ status: CLAuthorizationStatus) -> Bool {
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            return true
        default:
            return false
        }
    }
    
    // MARK: Delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        self.location.accept(location)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {}
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.access.accept(LocationManager.hasAccess(status))

    }
    
}
