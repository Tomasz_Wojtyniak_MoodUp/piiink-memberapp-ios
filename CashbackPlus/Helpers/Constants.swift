//
//  Constants.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 19/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import RxSwift

enum Constants {
    
    enum Storyboards: String {
        case splash = "Splash"
        case home = "Home"
        case main = "Main"
    }
    
    enum Segues {
        static let moreOffers = "MoreOffers"
        static let naviToLocalization = "NaviToLocalization"
        static let privacyPolicyToHome = "PrivacyPolicyToHome"
        static let launchToHome = "LaunchToHome"
        static let launchToSplash = "LaunchToSplash"
    }
    
    enum ViewControllers {
        static let privacyPolicyNavigation = "PrivacyPolicyNavigationController"
    }
    
    enum XIBs {
        static let headerView = "HeaderView"
        static let offerBox = "OfferBox"
        static let searchInput = "SearchInput"
        static let locationAccessAd = "LocationAccessAd"
        static let locationTableHeader = "LocationTableHeader"
        static let checkableInputView = "CheckableInputView"
    }
    
    enum HTTP {
        static let authHeader = "Authorization"
        
        enum Wordpress {
            static let baseURL = URL(string: "http://cashbackplus.com.au/en/api/")!
            static let defaultParameters = ["l": "en"]
        }
        
        enum MemberAPI {
            static let baseURL = URL(string: "http://183.78.170.141/api/")!
        }
    }
    
    enum Categories {
        static let all = Category(name: L10n.CategoryCollection.all, slug: "all")
    }
    
    enum Screens {
        enum Offers {
            static let nearbyOffersLimitCount = 4
        }
    }
    
    enum Localizables {
        static let boldSeparator = "@b"
        static let headerSeparator = "@h"
    }
    
    enum CardHelper {
        static let tycCardNumberRegex = "^6[0-9]{15}$" // Starts with "6", and has 16 digits
        static let pinCodeLength = 6
        static let tycCardNumberLength = 16
    }
    
    enum Validators {
        static let emailPredicate = NSPredicate(format:"SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")
        static let phonePredicate = NSPredicate(format:"SELF MATCHES %@", "(\\+[0-9]{2})[0-9]{9}")
    }
    
    enum Application {
        static let defaultHiddenCredentials = true
    }
    
    enum Registration {
        enum Browser {
            static let startURL = URL(string: "http://cb.cashbackplus.com.au/cbrewards_au/SMS/OnlineRegistration.html")!
            static let permittedURLs: [URL] = [
                startURL,
                URL(string: "http://cb.cashbackplus.com.au/cbrewards_au/servlet/com.registration.OnlineRegistration")!
            ]
        }
    }
    
    enum Mail {
        static let addressForRecommendMerchant = "dc@thankyouclub.org"
        static let emptyFieldPlaceholder = "-"
    }
    
    enum Inputs {
        static let throttleTime: TimeInterval = 0.5
    }
    
    enum Location {
        static let initRegionSlug = "australia"
        static let maxNearbyItems = 10
    }
    
    enum Keys {
        static let cardNumber = "cardNumber"
        static let view = "view"
    }
}

