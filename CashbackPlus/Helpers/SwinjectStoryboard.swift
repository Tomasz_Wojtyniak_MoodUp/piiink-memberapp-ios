//
//  SwinjectStoryboard.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 22/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import SwinjectStoryboard
import CoreLocation

extension SwinjectStoryboard {
    
    @objc class func setup() {
        
        // Cache
        defaultContainer.register(Cache.self) { resolver in
            Cache()
        }.inObjectScope(.container)
        
        // API
        defaultContainer.register(RemoteRepository.self) { resolver in
            Configuration.shared.setupConfiguration()
            return RemoteRepository(
                cache: resolver.resolve(Cache.self)!,
                wordpressClient: Client(baseURL: URL(string: Configuration.shared.config.wordpressApi ?? ""), parameters: Constants.HTTP.Wordpress.defaultParameters),
                newBackendClient: Client(baseURL:  URL(string: Configuration.shared.config.newApi ?? ""))
            )
        }.inObjectScope(.container)
        
        // Location manager
        defaultContainer.register(LocationManager.self) { resolver in
            // Todo inject location from cache
            LocationManager(location: CLLocation(latitude: 0, longitude: 0))
        }.inObjectScope(.container)
        
        defaultContainer.register(LocationService.self) { resolver in
            resolver.resolve(LocationManager.self)!
        }
        
        defaultContainer.register(LocationProvider.self) { resolver in
            resolver.resolve(LocationManager.self)!
        }
        
        // Models
        defaultContainer.register(Merchants.self) { resolver in
            resolver.resolve(RemoteRepository.self)!
        }
        
        // Controllers
        defaultContainer.storyboardInitCompleted(HomeViewController.self) {
            resolver, controller in
            controller
                .inject(
                    HomeViewModel(
                        storage: resolver.resolve(Cache.self)!,
                        members: resolver.resolve(RemoteRepository.self)!
                    )
            )
        }
        
        defaultContainer.storyboardInitCompleted(CardViewController.self) {
            resolver, controller in
            controller
                .inject(
                    CardViewModel(
                        storage: resolver.resolve(Cache.self)!,
                        auth: resolver.resolve(RemoteRepository.self)!,
                        members: resolver.resolve(RemoteRepository.self)!
                    )
            )
        }
        
        
        defaultContainer.storyboardInitCompleted(AddCardViewController.self) {
            resolver, controller in
            controller
                .inject(
                    AddCardViewModel(
                        storage: resolver.resolve(Cache.self)!,
                        auth: resolver.resolve(RemoteRepository.self)!,
                        members: resolver.resolve(RemoteRepository.self)!
                    )
            )
        }
        
        defaultContainer.storyboardInitCompleted(SplashViewController.self) {
            resolver, controller in
            controller
                .inject(
                    SplashViewModel(
                        storage: resolver.resolve(Cache.self)!,
                        cachedRegions: resolver.resolve(Cache.self)!,
                        remoteRegions: resolver.resolve(RemoteRepository.self)!
                    )
            )
        }
        
        defaultContainer.storyboardInitCompleted(LocalizationViewController.self) {
            resolver, controller in
            controller
                .inject(
                    LocalizationViewModel(
                        storage: resolver.resolve(Cache.self)!,
                        cachedRegions: resolver.resolve(Cache.self)!,
                        remoteRegions: resolver.resolve(RemoteRepository.self)!
                    )
                )
        }
        
        defaultContainer.storyboardInitCompleted(UpdateMembershipViewController.self) {
            resolver, controller in
            controller
                .inject(
                    UpdateMembershipViewModel(
                        storage: resolver.resolve(Cache.self)!,
                        members: resolver.resolve(RemoteRepository.self)!
                    )
                )
        }
        
        defaultContainer.storyboardInitCompleted(CharitySlidesViewController.self) {
            resolver, controller in
            controller
                .inject(
                    CharitySlidesViewModel(
                        storage: resolver.resolve(Cache.self)!,
                        members: resolver.resolve(RemoteRepository.self)!
                    )
            )
        }
        
        defaultContainer.storyboardInitCompleted(CharityViewController.self) {
            resolver, controller in
            controller
                .inject(
                    CharityViewModel(
                        storage: resolver.resolve(Cache.self)!,
                        cards: resolver.resolve(RemoteRepository.self)!,
                        charities: resolver.resolve(RemoteRepository.self)!,
                        members: resolver.resolve(RemoteRepository.self)!
                    )
            )
        }
        
        defaultContainer.storyboardInitCompleted(ProfileViewController.self) {
            resolver, controller in
            controller
                .inject(
                    ProfileViewModel(
                        storage: resolver.resolve(Cache.self)!,
                        cards: resolver.resolve(RemoteRepository.self)!,
                        offers: resolver.resolve(RemoteRepository.self)!
                    )
            )
        }
        
        defaultContainer.storyboardInitCompleted(PrivacyPolicyViewController.self) {
            resolver, controller in
            controller
                .inject(
                    BaseViewModel(
                        storage: resolver.resolve(Cache.self)!
                    )
            )
        }
        
        defaultContainer.storyboardInitCompleted(LaunchViewController.self) {
            resolver, controller in
            controller
                .inject(
                    BaseViewModel(
                        storage: resolver.resolve(Cache.self)!
                    )
            )
        }
        
        defaultContainer.storyboardInitCompleted(MerchantsViewController.self) {
            resolver, controller in
            controller
                .inject(
                    MerchantsViewModel(
                        storage: resolver.resolve(Cache.self)!,
                        merchants: resolver.resolve(Merchants.self)!,
                        locationProvider: resolver.resolve(LocationProvider.self)!
                    ),
                    locationService: resolver.resolve(LocationService.self)!
            )
        }
        
        defaultContainer.storyboardInitCompleted(MerchantDetailsViewController.self) {
            resolver, controller in
            controller
                .inject(
                    MerchantDetailsViewModel(
                        storage: resolver.resolve(Cache.self)!,
                        merchants: resolver.resolve(Merchants.self)!
                    )
            )
        }
        
        defaultContainer.storyboardInitCompleted(MerchantMoreOffersViewController.self) {
            resolver, controller in
            controller
                .inject(
                    MerchantMoreOffersViewModel(
                        storage: resolver.resolve(Cache.self)!
                    )
            )
        }
        
        defaultContainer.storyboardInitCompleted(RegionsViewController.self) {
            resolver, controller in
            controller
                .inject(
                    RegionsViewModel(
                        storage: resolver.resolve(Cache.self)!
                    )
            )
        }
        
        defaultContainer.storyboardInitCompleted(SearchViewController.self) {
            resolver, controller in
            controller
                .inject(
                    SearchViewModel(
                        storage: resolver.resolve(Cache.self)!,
                        merchants: resolver.resolve(Merchants.self)!,
                        locationProvider: resolver.resolve(LocationProvider.self)!
                    ),
                    locationService: resolver.resolve(LocationService.self)!
            )
        }
        
        defaultContainer.storyboardInitCompleted(RecommendViewController.self) {
            resolver, controller in
            controller
                .inject(
                    BaseViewModel(
                        storage: resolver.resolve(Cache.self)!
                    )
            )
        }
    }
}
