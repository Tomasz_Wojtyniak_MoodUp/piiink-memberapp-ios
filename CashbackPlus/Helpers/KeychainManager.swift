//
//  KeychainManager.swift
//  SwiftStarterProject
//
//  Created by Piotr Gomoła on 30/01/2019.
//  Copyright © 2019 MoodUp. All rights reserved.
//

import Foundation
import UIKit
import KeychainSwift

class KeychainManager {
    private let accessToken = "accessToken"
    private var keychain: KeychainSwift
    
    init() {
        self.keychain = KeychainSwift()
    }
    
    public func storeAccessToken(token: String) -> Bool {
        let result = keychain.set(token, forKey: self.accessToken)
        if result {
            self.delegate?.accessTokenUpdated(token: token)
        }
        return result
    }
    
    public func getAccessToken() -> String? {
        return keychain.get(self.accessToken)
    }
    
    public func removeAccessToken() -> Bool {
        return keychain.delete(self.accessToken)
    }
    
    public func storePassword(username: String, password: String) -> Bool {
        return keychain.set(password, forKey: username)
    }
    
    public func getPassword(username: String) -> String? {
        return keychain.get(username)
    }
}
