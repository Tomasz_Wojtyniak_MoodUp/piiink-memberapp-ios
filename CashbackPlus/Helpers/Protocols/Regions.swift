//
//  Regions.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 02/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

protocol Regions {
    func getRegions() -> Single<[Region]>
}
