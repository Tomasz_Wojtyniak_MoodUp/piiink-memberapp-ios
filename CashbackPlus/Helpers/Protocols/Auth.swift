//
//  Cards.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 12/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

protocol Auth {
    func loginCard(cardNumber: String, cardPin: String) -> Single<AuthResponse>
    func logoutCard(auth: AuthResponse) -> Completable
}
