//
//  Wallets.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 21/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

protocol Cards {
    func getBalance(page: Int) -> Single<CardsBalanceResult>
    func getCardInfo() -> Single<Card>
}
