//
//  LocationProvider.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 25/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxRelay
import CoreLocation

protocol LocationProvider {
    func getLocation() -> BehaviorRelay<CLLocation>
}
