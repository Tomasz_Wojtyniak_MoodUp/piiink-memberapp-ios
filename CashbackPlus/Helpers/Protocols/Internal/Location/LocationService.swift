//
//  LocationService.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 26/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxRelay

protocol LocationService {
    var access: BehaviorRelay<Bool> { get }
    
    func refreshLocation()
    func requestAccess()
}
