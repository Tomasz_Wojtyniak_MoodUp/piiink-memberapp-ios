//
//  Storage.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 03/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxRelay
import CoreLocation

protocol Storage {
    var currentRegion: BehaviorRelay<Region?> { get }
    var regions: [Region] { get }
    
    typealias AuthData = AuthResponse
    
    var auth: BehaviorRelay<AuthData?> { get }
    var authorizedMember: Member? { get set }
    var cardNumber: String? { get set }
    
    var isCardInfoHidden: Bool { get set }
    var hasSeenCharityScreen: Bool { get set }
    var hasSeenPrivacyPolicy: Bool { get set }
    var hasSeenProfile: Bool { get set }
    
    func store(auth: AuthData?)
    func store(chosenRegion: Region)
    func store(regions: [Region])
}
