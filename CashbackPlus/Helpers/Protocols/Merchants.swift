//
//  Offers.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 24/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation

protocol Merchants {
    func getBestOffers() -> Single<[Merchant]>
    func getCategories() -> Single<[Category]>
    func getNearbyOffers(_ location: CLLocation) -> Single<[Merchant]>
    func getNewMerchants() -> Single<[Merchant]>
    func getAllOffers() -> Single<[Merchant]>
    func getDiscount(id: Int, timeZone: String) -> Single<MerchantDiscount>
    func search(query: String) -> Single<[Merchant]>
}
