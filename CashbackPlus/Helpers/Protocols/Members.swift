//
//  Members.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 12/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

protocol Members {
    func getAuthorizedMember() -> Single<Member>
    func updateMember(_ member: Member) -> Single<Member>
    func chooseCharity(_ charity: Charity) -> Single<Member> 
}
