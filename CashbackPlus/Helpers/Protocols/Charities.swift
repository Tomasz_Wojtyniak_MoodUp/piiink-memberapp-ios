//
//  Charities.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 19/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation
import RxSwift

protocol Charities {
    func getCharities(page: Int, name: String?) -> Single<CharityPage>
    func getCharity(id: Int) -> Single<Charity>
}
