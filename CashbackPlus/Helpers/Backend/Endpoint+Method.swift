//
//  Enums.swift
//  apiclient
//
//  Created by Jakub Janicki on 03.09.2018.
//  Copyright © 2018 Jakub Janicki. All rights reserved.
//

import Foundation

enum Method {
    case get, post, put, patch, delete
}
