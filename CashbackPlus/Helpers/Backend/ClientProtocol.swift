//
//  ClientProtocol.swift
//  apiclient
//
//  Created by Jakub Janicki on 03.09.2018.
//  Copyright © 2018 Jakub Janicki. All rights reserved.
//

import Foundation
import RxSwift

protocol ClientProtocol {
    func setToken(_ token: String?)
    func setRefreshTokenMethod(method: Completable)
    func singleRequest<Response>(_ endpoint: Endpoint<Response>) -> Single<Response>
    func completableRequest(_ endpoint: Endpoint<Void>) -> Completable
}
