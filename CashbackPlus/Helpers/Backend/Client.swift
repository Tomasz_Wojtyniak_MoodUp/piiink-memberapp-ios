//
//  Client.swift
//  apiclient
//
//  Created by Jakub Janicki on 03.09.2018.
//  Copyright © 2018 Jakub Janicki. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

final class Client: ClientProtocol, RequestRetrier, RequestAdapter {
    
    private typealias RefreshCompletion = (_ succeeded: Bool) -> Void
    
    private let lock = NSLock()
    private let sessionManager: Alamofire.SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders

        return Alamofire.SessionManager(configuration: configuration)
    }()
    private let baseURL: URL
    private let parameters: Parameters
    private let queue = DispatchQueue(label: "client_queue")
    private var isRefreshing = false
    private var requestsToRetry: [RequestRetryCompletion] = []
    private var disposeBag = DisposeBag()
    private var accessToken: String? = nil
    
    var refreshToken: Completable?
    
    init(baseURL: URL?, parameters: Parameters = [:]) {
        self.baseURL = baseURL ?? URL(fileURLWithPath: "/")
        self.parameters = parameters
        self.sessionManager.retrier = self
        self.sessionManager.adapter = self
    }
    
    func setToken(_ token: String?) {
        self.accessToken = token
    }
    
    func setRefreshTokenMethod(method: Completable) {
        self.refreshToken = method
    }
    
    func singleRequest<Response>(_ endpoint: Endpoint<Response>) -> Single<Response> {
        return Single<Response>.create { observer in
            let request = self.sessionManager.request(self.url(path: endpoint.path),
                                               method: httpMethod(from: endpoint.method),
                                               parameters: endpoint.parameters,
                                               encoding: endpoint.parameterEncoding,
                                               headers: endpoint.additionalHeader)
            
            if let (user, password) = endpoint.authenticate {
                request.authenticate(user: user, password: password)
            }

            request
                .validate()
                .responseData(queue: self.queue) { [weak self] response in
                    let result = response.result.flatMap(endpoint.decode)
                    switch result {
                    case let .success(val):
                        self?.validateResponse(response: response, completion: { (success, errorType) in
                            if success {
                                observer(.success(val))
                            } else {
                                // Create and return your own error based on response and error type
                                observer(.error(BackendError(response, errorType ?? .generic)))
                            }
                        })
                        
                    case let .failure(err):
                        observer(.error(err))
                    }
            }
            
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func completableRequest(_ endpoint: Endpoint<Void>) -> Completable {
        return Completable.create { observer in
            let request = self.sessionManager.request(self.url(path: endpoint.path),
                                               method: httpMethod(from: endpoint.method),
                                               parameters: endpoint.parameters,
                                               encoding: endpoint.parameterEncoding,
                                               headers: endpoint.additionalHeader)
            
            if let (user, password) = endpoint.authenticate {
                request.authenticate(user: user, password: password)
            }
            
            request
                .validate()
                .responseData(queue: self.queue) { [weak self] response in
                    let result = response.result.flatMap(endpoint.decode)
                    switch result {
                    case .success(_):
                        self?.validateResponse(response: response, completion: { (success, errorType) in
                            if success {
                                observer(.completed)
                            } else {
                                // Create and return your own error based on response and error type
                                observer(.error(BackendError(response, errorType ?? .generic)))
                            }
                        })
                        
                    case let .failure(err):
                        observer(.error(err))
                    }
            }
            
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    private func url(path: Path) -> URL {
        return self.baseURL.appendingPathComponent(path)
    }
    
    enum ErrorType {
        case generic, notFound, unavailable, unauthorized
    }
    
    class BackendError: Error {
        
        let response: DataResponse<Data>
        let errorType: ErrorType
        
        init(_ res: DataResponse<Data>, _ err: ErrorType) {
            response = res
            errorType = err
        }
    }
    
    private func validateResponse(response: DataResponse<Data>, completion: (Bool, ErrorType?) -> ()) {
        guard let statusCode = response.response?.statusCode else {
            // Handle generic error
            completion(false, .generic)
            return
        }
        
        // Switch was used because you can easily modify validation process to handle all of the errors here
        switch statusCode {
        case 200..<400:
            completion(true, nil)
        case 401:
            completion(false, .unauthorized)
        case 404:
            completion(false, .notFound)
        case 503:
            completion(false, .unavailable)
        default:
            completion(false, .generic)
        }
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        if let token = self.accessToken {
            var urlRequest = urlRequest
            urlRequest.setValue("Bearer " + token, forHTTPHeaderField: "Authorization")
            return urlRequest
        }

        return urlRequest
    }
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        lock.lock() ; defer { lock.unlock() }
        if let response = request.task?.response as? HTTPURLResponse,
            let token = self.accessToken,
            response.statusCode == 401 {
            self.requestsToRetry.append(completion)

            if !self.isRefreshing {
                self.refreshTokens { [weak self] succeeded in
                    guard let context = self else { return }

                    context.lock.lock() ; defer { context.lock.unlock() }

                    context.requestsToRetry.forEach { $0(succeeded, 0.0) }
                    context.requestsToRetry.removeAll()
                }
            }
        } else {
            completion(false, 0.0)
        }
    }
    
    private func refreshTokens(completion: @escaping RefreshCompletion) {
        guard let refreshToken = self.refreshToken, !self.isRefreshing else { return }
        
        self.isRefreshing = true
        
        refreshToken.subscribe(onCompleted: {
            self.isRefreshing = false
            completion(true)
        }, onError: { (error) in
            self.isRefreshing = false
            completion(false)
        }).disposed(by: self.disposeBag)
    }
}

private func httpMethod(from method: Method) -> Alamofire.HTTPMethod {
    switch method {
        case .get: return .get
        case .post: return .post
        case .put: return .put
        case .patch: return .patch
        case .delete: return .delete
    }
}
