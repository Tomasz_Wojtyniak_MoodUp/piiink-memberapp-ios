//
//  Endpoint.swift
//  apiclient
//
//  Created by Jakub Janicki on 03.09.2018.
//  Copyright © 2018 Jakub Janicki. All rights reserved.
//

import Foundation
import Alamofire

final class Endpoint<Response> {
    typealias Credentials = (String, String)
    
    let method: Method
    let path: Path
    let parameters: Parameters?
    let parameterEncoding: ParameterEncoding
    let additionalHeader: HTTPHeaders?
    let decode: (Data) throws -> Response
    let authenticate: Credentials?
    
    init(method: Method = .get,
         path: Path,
         parameters: Parameters? = nil,
         parameterEncoding: ParameterEncoding = URLEncoding.default,
         additionalHeader: HTTPHeaders? = nil,
         authenticate: Credentials? = nil,
         decode: @escaping (Data) throws -> Response) {
        self.method = method
        self.path = path
        self.parameters = parameters
        self.parameterEncoding = parameterEncoding
        self.additionalHeader = additionalHeader
        self.authenticate = authenticate
        self.decode = decode
    }
}

extension Endpoint where Response: Swift.Decodable {
    convenience init(method: Method = .get,
                     path: Path,
                     parameters: Parameters? = nil,
                     parameterEncoding: ParameterEncoding = URLEncoding.default,
                     additionalHeader: HTTPHeaders? = nil,
                     authenticate: Credentials? = nil) {
        self.init(
            method: method,
            path: path,
            parameters: parameters,
            parameterEncoding: parameterEncoding,
            additionalHeader: additionalHeader,
            authenticate: authenticate
        ) {
            try JSONDecoder().decode(Response.self, from: $0)
        }
    }
}

extension Endpoint where Response == Void {
    convenience init(method: Method = .get,
                     path: Path,
                     parameters: Parameters? = nil,
                     parameterEncoding: ParameterEncoding = URLEncoding.default,
                     additionalHeader: HTTPHeaders? = nil,
                     authenticate: Credentials? = nil) {
        self.init(
            method: method,
            path: path,
            parameters: parameters,
            parameterEncoding: parameterEncoding,
            additionalHeader: additionalHeader,
            authenticate: authenticate,
            decode: { _ in () }
        )
    }
}

extension Endpoint where Response == String {
    convenience init(method: Method = .get,
                     path: Path,
                     parameters: Parameters? = nil,
                     parameterEncoding: ParameterEncoding = URLEncoding.default,
                     additionalHeader: HTTPHeaders? = nil,
                     authenticate: Credentials? = nil) {
        self.init(
            method: method,
            path: path,
            parameters: parameters,
            parameterEncoding: parameterEncoding,
            additionalHeader: additionalHeader,
            authenticate: authenticate
        ) {
            String(data: $0, encoding: .utf8) ?? ""
        }
    }
}
