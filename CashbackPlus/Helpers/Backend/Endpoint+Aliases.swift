//
//  Aliases.swift
//  apiclient
//
//  Created by Jakub Janicki on 03.09.2018.
//  Copyright © 2018 Jakub Janicki. All rights reserved.
//

import Foundation

typealias Parameters = [String: Any]
typealias Path = String
