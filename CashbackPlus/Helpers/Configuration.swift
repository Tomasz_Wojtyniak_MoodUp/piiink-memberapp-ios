//
//  Configuration.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 11/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

final class Configuration {
    private init() { }
    
    private var path: String?
    
    private var configModel: Config?
    var config: Config {
        return self.configModel!
    }
    
    static let shared = Configuration()
    
    func setupConfiguration() {        
        var resourceName: String
        resourceName = "Config"
        
        guard let path = Bundle.main.path(forResource: resourceName, ofType: "json") else { return }
        let pathUrl = URL(fileURLWithPath: path)
        do {
            let jsonData = try Data(contentsOf: pathUrl)
            self.configModel = try JSONDecoder().decode(Config.self, from: jsonData)
        } catch {
            print("👎 Unable to load data: \(error)")
        }
    }
}
