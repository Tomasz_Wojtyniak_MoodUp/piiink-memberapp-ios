//
//  Extensions.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 19/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

extension UIViewController {
    class func reuseIdentifier() -> String {
        return String(describing: self)
    }
}

extension UIView {
    class func reuseIdentifier() -> String {
        return String(describing: self)
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension Notification.Name {
    static let didAddCard = Notification.Name("didAddCard")
    static let didRegisterCard = Notification.Name("didRegisterCard")
}

extension String {
    var hidden: String {
        return String(
            self.prefix(1) + String(repeating: "*", count: self.count - 1)
        )
    }
}

extension UITableViewCell {
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }
    
    class func nibName() -> String {
        return String(describing: self)
    }
    
    class func nib() -> UINib {
        return UINib(nibName: self.nibName(), bundle: nil)
    }
}

extension UICollectionViewCell {
    class func nibName() -> String {
        return String(describing: self)
    }
    
    class func nib() -> UINib {
        return UINib(nibName: self.nibName(), bundle: nil)
    }
}
