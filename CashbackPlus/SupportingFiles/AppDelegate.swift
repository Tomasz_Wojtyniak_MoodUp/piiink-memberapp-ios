//
//  AppDelegate.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 18/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        Configuration.shared.setupConfiguration()
        
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        
        return true
    }

}

