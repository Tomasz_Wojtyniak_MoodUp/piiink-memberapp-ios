//
//  Charity.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 16/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct Charity: Codable {
    let id: Int
    let name: String
    let websiteUrl: String
    
    static func == (lhs: Charity, rhs: Charity) -> Bool {
        return lhs.id == rhs.id
    }
    
    static func != (lhs: Charity, rhs: Charity) -> Bool {
        return lhs.id != rhs.id
    }
}
