//
//  Offer.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 24/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct Merchant: Codable {
    let title: String
    let link: String
    let category: [String]
    let region: [String]
    let thumbnail: String
    let content: String?
    let discount: String?
    let gallery: [String]
    let distance: Double?
    let locals: [Locals]
    let backend_merchant_id: String?
    
    var merchantID: Int? {
        guard let description = self.backend_merchant_id else { return nil }
        return Int(description)
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.title = try values.decode(String.self, forKey: .title)
        self.link = try values.decode(String.self, forKey: .link)
        self.category = try values.decode([String].self, forKey: .category)
        self.region = try values.decode([String].self, forKey: .region)
        self.thumbnail = try values.decode(String.self, forKey: .thumbnail)
        self.content = try? values.decode(String.self, forKey: .content)
        self.discount = try? values.decode(String.self, forKey: .discount)
        self.gallery = try values.decode([String].self, forKey: .gallery)
        self.distance = try? values.decode(Double.self, forKey: .distance)
        self.backend_merchant_id = try? values.decode(String.self, forKey: .backend_merchant_id)
        if let locals = try? values.decode([Locals].self, forKey: .locals) {
            self.locals = locals
        } else {
            self.locals = [try values.decode(Locals.self, forKey: .locals)]
        }
    }
}
