//
//  Category+Type.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 12/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

extension Category {
    enum CategoryType {
        case all
        case accomodation
        case activitiesEntertaiment
        case automotive
        case bakery
        case butcher
        case clothing
        case clubs
        case coffeCafe
        case dentistHygenist
        case education
        case electronics
        case finance
        case foodAndBeverage
        case fruitAndVeg
        case hairAndBeauty
        case hardware
        case healthAndFitness
        case hire
        case houseAndGarden
        case it
        case medical
        case pets
        case printing
        case pub
        case publicServices
        case recreation
        case restaurants
        case retail
        case services
        case signWriting
        case sports
        case takeawayFood
        case tradies
        case unknown
    }
    
    var type: CategoryType {
        switch self.slug {
        case "all": // this one doesn't exist in retrived from http data
            return .all
        case "accommodation":
            return .accomodation
        case "activities-and-entertainment":
            return .activitiesEntertaiment
        case "automotive":
            return .automotive
        case "bakery":
            return .bakery
        case "butcher":
            return .butcher
        case "clothing":
            return .clothing
        case "clubs":
            return .clubs
        case "coffee-cafe":
            return .coffeCafe
        case "dentist-hygienist":
            return .dentistHygenist
        case "education":
            return .education
        case "electronics":
            return .electronics
        case "finance":
            return .finance
        case "food-and-beverage":
            return .foodAndBeverage
        case "fruit-and-veg":
            return .fruitAndVeg
        case "hair-and-beauty":
            return .hairAndBeauty
        case "hardware":
            return .hardware
        case "health-and-fitness":
            return .healthAndFitness
        case "hire":
            return .hire
        case "house-and-garden":
            return .houseAndGarden
        case "it":
            return .it
        case "medical":
            return .medical
        case "pets":
            return .pets
        case "printing":
            return .printing
        case "pub":
            return .pub
        case "public-services":
            return .publicServices
        case "recreation":
            return .recreation
        case "restaurants":
            return .restaurants
        case "retail":
            return .retail
        case "services":
            return .services
        case "sign-writing":
            return .signWriting
        case "sports":
            return .sports
        case "takeaway-food":
            return .takeawayFood
        case "tradies":
            return .tradies
        default:
            return .unknown
        }
    }
}
