//
//  Category.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 25/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct Category: Codable {
    let name: String
    let slug: String
}
