//
//  AuthResponse.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 12/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct AuthResponse: Codable {
    let token: String
    let grantType: String
    let refreshToken: String
}
