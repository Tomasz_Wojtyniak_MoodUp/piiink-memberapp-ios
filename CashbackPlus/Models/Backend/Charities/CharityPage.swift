//
//  CharityPage.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 29/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct CharityPage: Codable {
    let count: Int
    let totalElements: Int
    let page: Int
    let pageSize: Int
    let totalPages: Int
    let firstPage: Bool
    let lastPage: Bool
    let content: [Charity]
}
