//
//  DiscountDay.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 23/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct DiscountDay: Codable {
    let hour0: Int
    let hour1: Int
    let hour2: Int
    let hour3: Int
    let hour4: Int
    let hour5: Int
    let hour6: Int
    let hour7: Int
    let hour8: Int
    let hour9: Int
    let hour10: Int
    let hour11: Int
    let hour12: Int
    let hour13: Int
    let hour14: Int
    let hour15: Int
    let hour16: Int
    let hour17: Int
    let hour18: Int
    let hour19: Int
    let hour20: Int
    let hour21: Int
    let hour22: Int
    let hour23: Int
}
