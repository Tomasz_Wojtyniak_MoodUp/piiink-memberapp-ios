//
//  Discount.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 23/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct Discount: Codable {
    let merchantId: Int
    let monday: DiscountDay
    let tuesday: DiscountDay
    let wednesday: DiscountDay
    let thursday: DiscountDay
    let friday: DiscountDay
    let saturday: DiscountDay
    let sunday: DiscountDay
}
