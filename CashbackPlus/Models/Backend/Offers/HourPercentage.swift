//
//  HourPercentage.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 23/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct HourPercentage: Codable {
    let weekDay: String
    let hour: Int
    let hourKey: String
    let time: String
    let timeZone: TimeZone
    let discount: Int
}

