//
//  UpdateSelfMember.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 19/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct UpdateSelfMember: Codable {
    let firstName: String
    let lastName: String
    let email: String
    let phone: String
    let postalCode: String
    let charityId: Int
    
    init(_ member: Member, charity: Charity) {
        self.firstName = member.firstName
        self.lastName = member.lastName
        self.email = member.email
        self.phone = member.phone
        self.postalCode = member.postalCode
        self.charityId = charity.id
    }
}
