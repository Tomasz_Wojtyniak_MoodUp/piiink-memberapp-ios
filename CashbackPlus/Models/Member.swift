//
//  Member.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 12/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct Member: Codable {
    let id: Int?
    let firstName: String
    let lastName: String
    let email: String
    let phone: String
    let postalCode: String
}
