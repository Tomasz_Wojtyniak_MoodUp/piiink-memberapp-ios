//
//  Locals.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 23/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct Locals: Codable {
    let street: String?
    let post_code: String?
    let city: String?
    let phone: String?
    let mail: String?
    let url: String?
    let lat: String?
    let lng: String?
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.street = try? values.decode(String.self, forKey: .street)
        self.post_code = try? values.decode(String.self, forKey: .post_code)
        self.city = try? values.decode(String.self, forKey: .city)
        self.phone = try? values.decode(String.self, forKey: .phone)
        self.mail = try? values.decode(String.self, forKey: .mail)
        self.url = try? values.decode(String.self, forKey: .url)
        if let lat = try? values.decode(Double.self, forKey: .lat) {
            self.lat = String(lat)
        } else {
            self.lat = try? values.decode(String.self, forKey: .lat)
        }
        if let lng = try? values.decode(Double.self, forKey: .lng) {
            self.lng = String(lng)
        } else {
            self.lng = try? values.decode(String.self, forKey: .lng)
        }
    }
}
