//
//  Card.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 30/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct Card: Codable {
    let id: Int
    let memberId: Int
    let charityId: Int
    let issuerId: Int
    let number: String
}
