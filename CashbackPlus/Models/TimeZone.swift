//
//  TimeZone.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 23/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct TimeZone: Codable {
    let group: String
    let name: String
    let label: String
}
