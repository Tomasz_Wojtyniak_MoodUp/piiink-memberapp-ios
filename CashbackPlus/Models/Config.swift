//
//  Config.swift
//  CashbackPlus
//
//  Created by Piotr Gomoła on 11/09/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

class Config: Decodable {
    
    let newApi: String?
    let wordpressApi: String?
    let registrationForm: String?
    let librariesFileName: String?
    
    private enum CodingKeys : String, CodingKey {
        case newApi = "new_api",
        wordpressApi = "wordpress_api",
        registrationForm = "registration_form",
        librariesFileName = "libraries_file_name"
    }
}
