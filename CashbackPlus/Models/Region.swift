//
//  Region.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 02/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct Region: Codable {
    let name: String
    let slug: String
    
    static func == (lhs: Region, rhs: Region) -> Bool {
        return lhs.slug == rhs.slug
    }
}
