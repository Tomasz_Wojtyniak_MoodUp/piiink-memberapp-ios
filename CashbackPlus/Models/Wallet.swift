//
//  Wallet.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 21/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import Foundation

struct Wallet: Codable {
    let id: Int
    let merchantId: Int
    let cardId: Int
    let points: Float
    let updatedAt: String?
}
