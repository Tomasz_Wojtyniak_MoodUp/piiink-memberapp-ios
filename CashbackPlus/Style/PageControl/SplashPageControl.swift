//
//  AppPageControl.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 29/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

@IBDesignable
final class SplashPageControl: UIPageControl {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
    }
    
    private func setup() {
        self.tintColor = StyleConstants.SplashPageControl.activeColor
        self.pageIndicatorTintColor = StyleConstants.SplashPageControl.unactiveColor
        self.currentPageIndicatorTintColor = StyleConstants.SplashPageControl.activeColor
        
        let scale = StyleConstants.SplashPageControl.scale
        self.transform = CGAffineTransform(scaleX: scale, y: scale)
    }

}
