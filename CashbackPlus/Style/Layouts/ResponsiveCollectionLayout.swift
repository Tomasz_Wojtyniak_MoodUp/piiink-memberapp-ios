//
//  ResponsiveTableLayout.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 26/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

class ResponsiveCollectionLayout : NSObject, UICollectionViewDelegateFlowLayout {
    
    let columns: Int
    
    init(columns: Int) {
        self.columns = columns
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let layout = collectionViewLayout as? UICollectionViewFlowLayout
        let space =
            (layout?.minimumInteritemSpacing ?? 0.0) +
            (layout?.sectionInset.left ?? 0.0) +
            (layout?.sectionInset.right ?? 0.0)
        
        let size  = (collectionView.frame.size.width - space) / CGFloat(self.columns)
        return CGSize(width: size, height: StyleConstants.OfferBox.height)
    }
    
}
