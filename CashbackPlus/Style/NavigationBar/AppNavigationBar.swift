//
//  AppNavigationBar.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 30/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

@IBDesignable
class AppNavigationBar: UINavigationBar {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        self.backgroundColor = .clear
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.isTranslucent = true
        self.tintColor = StyleConstants.NavigationBar.titleColor
        
        if let font = StyleConstants.NavigationBar.titleFont {
            self.titleTextAttributes = [
                NSAttributedString.Key.font: font,
                NSAttributedString.Key.foregroundColor: StyleConstants.NavigationBar.titleColor
            ]
        }
        
        self.backIndicatorImage = Asset.chevronLeft.image
        self.backIndicatorTransitionMaskImage = Asset.chevronLeft.image
    }

}
