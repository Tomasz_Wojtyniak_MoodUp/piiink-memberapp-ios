//
//  RoundEffect.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 23/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

final class RoundEffect : BaseEffect {
    
    var radius: CGFloat = StyleConstants.RoundCorners.radius
    
    func add(to view: UIView) {
        view.layer.cornerRadius = self.radius
        view.clipsToBounds = true
    }
    
}
