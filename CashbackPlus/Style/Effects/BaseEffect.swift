//
//  BaseEffect.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 23/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

protocol BaseEffect {
    
    func add(to view: UIView)
    
}
