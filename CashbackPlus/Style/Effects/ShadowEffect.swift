//
//  ShadowEffect.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 23/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

enum PathType {
    case rect
    case roundRect
}

final class ShadowEffect : BaseEffect {
    
    var offset: CGSize = StyleConstants.Shadow.shadowOffset
    var pathType: PathType = .rect
    
    func add(to view: UIView) {
        view.layer.shadowColor = StyleConstants.Shadow.shadowColor
        view.layer.shadowOpacity = StyleConstants.Shadow.shadowOpacity
        view.layer.shadowOffset = self.offset
        view.layer.shadowRadius = StyleConstants.Shadow.shadowRadius
        
        view.layer.shadowPath = self.getShadowPath(view)
        
        view.layer.shouldRasterize = true
        view.layer.rasterizationScale = UIScreen.main.scale
        
        view.clipsToBounds = false
        view.layer.masksToBounds = false
    }
    
    private func getShadowPath(_ view: UIView) -> CGPath {
        switch self.pathType {
        case .rect:
            return UIBezierPath(rect: view.bounds).cgPath
        case .roundRect:
            return UIBezierPath(roundedRect: view.bounds, cornerRadius: StyleConstants.RoundCorners.radius).cgPath
        }
    }
}
