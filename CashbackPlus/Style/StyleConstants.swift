//
//  StyleConstants.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 22/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

enum StyleConstants {
    
    enum SectionShadow {
        static let shadowColor: CGColor = Color.black.cgColor
        static let shadowOpacity: CGFloat = 0.05
        static let shadowRadius: CGFloat = 10
    }
    
    enum Shadow {
        static let shadowColor: CGColor = Color.black.cgColor
        static let shadowOpacity: Float = 0.05
        static let shadowOffset = CGSize(width: 0, height: 2)
        static let shadowRadius: CGFloat = 3
    }
    
    enum RoundCorners {
        static let radius: CGFloat = 15
    }
    
    enum Buttons {
        enum Primary {
            static let roundRadius: CGFloat = 10
            static let height: CGFloat = 50
            static let font = UIFont(font: FontFamily.Lato.medium, size: 18)
            static let backgroundColor = ColorName.globalTint.color
            static let unctiveOpacity: CGFloat = 0.5
        }
    }
    
    enum OfferBox {
        static let width: CGFloat = 165
        static let height: CGFloat = 204
    }
    
    enum Animations {
        static let viewReplacingDuration: TimeInterval = 0.5
    }
    
    enum TabBar {
        static let unselectedColor = UIColor.black.withAlphaComponent(0.3)
        static let selectedColor = Asset.appTextColor.color
        static let font = UIFont(font: FontFamily.Lato.regular, size: 10)
    }
    
    enum Inputs {
        static let placeholderTextColor = UIColor.black.withAlphaComponent(0.3)
    }
    
    enum SplashPageControl {
        static let scale: CGFloat = 2
        static let activeColor = ColorName.splashPageControlActive.color
        static let unactiveColor = ColorName.splashPageControlUnactive.color
    }
    
    enum Splashes {
        static let normalFont = UIFont(font: FontFamily.Lato.regular, size: 16)
        static let boldFont = UIFont(font: FontFamily.Lato.bold, size: 16)
        static let fadeAnimationDuration = 0.5
    }
    
    enum SettingsMenu {
        static let font = UIFont(font: FontFamily.Lato.regular, size: 20)
        static let textColor = Color.black
    }
    
    enum NavigationBar {
        static let titleFont = UIFont(font: FontFamily.Lato.semibold, size: 18)
        static let titleColor = ColorName.defaultTextColor.color
    }
    
    enum LocationTableHeader {
        enum Fonts {
            static let normal = UIFont(font: FontFamily.Lato.regular, size: 16)
            static let bold = UIFont(font: FontFamily.Lato.semibold, size: 16)
        }
    }
    
    enum LocalizationTableHeader {
        static let backgroundColor = Color.white.cgColor
        static let shadowColor = Color.gray.cgColor
        static let offset = CGSize(width: 0.0, height: 0.5)
    }
    
    enum PolicyPrivacy {
        static let headerFont = UIFont(font: FontFamily.Lato.bold, size: 20)
    }
    
    enum BorderedBoxView {
        static let borderWidth: CGFloat = 2.0
    }
    
    enum PullBarView {
        static let backgroundColor = UIColor.black.withAlphaComponent(0.3)
        static let borderRadius: CGFloat = 2
    }
    
    enum CardViewModal {
        static let animationDuration: TimeInterval = 0.5
        static let maxBlurOpacity: CGFloat = 0.97
        static let breakpointHeightDivider: CGFloat = 3
    }
    
    enum Modal {
        static let overlayOpacity: CGFloat = 0.3
        static let textColor = UIColor.black.withAlphaComponent(0.54)
        static let semiboldFont = UIFont(font: FontFamily.Lato.semibold, size: 16)
        static let regularFont = UIFont(font: FontFamily.Lato.regular, size: 16)
    }
    
    enum InfinityScrolling {
        static let distance: CGFloat = 250.0
    }
}
