//
//  PrimaryButton.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 26/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

@IBDesignable
final class PrimaryButton: UIButton {
    
    @IBInspectable
    var active: Bool = true {
        didSet {
            if self.active {
                self.alpha = 1
            } else {
                self.alpha = StyleConstants.Buttons.Primary.unctiveOpacity
            }
        }
    }
    
    let roundEffect = RoundEffect()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
    }
    
    private func setup() {
        self.backgroundColor = StyleConstants.Buttons.Primary.backgroundColor
        self.setTitleColor(.white, for: .normal)
        self.titleLabel?.font = StyleConstants.Buttons.Primary.font
        
        self.setupHeight(StyleConstants.Buttons.Primary.height)
        
        self.roundEffect.radius = StyleConstants.Buttons.Primary.roundRadius
        self.roundEffect.add(to: self)
    }
    
    private func setupHeight(_ height: CGFloat) {
        let constraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: height)
        
        self.addConstraint(constraint)
    }
    
}
