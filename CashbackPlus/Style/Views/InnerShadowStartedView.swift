//
//  ShadowEndedView.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 22/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

@IBDesignable
final class InnerShadowStartedView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupShadow()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupShadow()
    }
    
    private func setupShadow() {
        let gradientLayer = CAGradientLayer()
        let color = UIColor(cgColor: StyleConstants.SectionShadow.shadowColor)
        
        gradientLayer.frame = CGRect(x: self.bounds.minX, y: self.bounds.minY, width: self.bounds.width, height: StyleConstants.SectionShadow.shadowRadius)
        gradientLayer.colors = [color.withAlphaComponent(StyleConstants.SectionShadow.shadowOpacity).cgColor, UIColor.clear.cgColor]
        
        self.layer.addSublayer(gradientLayer)
    }

}
