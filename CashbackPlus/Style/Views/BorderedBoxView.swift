//
//  BorderedBoxView.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 06/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

@IBDesignable
final class BorderedBoxView: UIView {
    
    @IBInspectable
    var borderColor: UIColor = .white
    
    @IBInspectable
    var borderWidth: CGFloat = StyleConstants.BorderedBoxView.borderWidth
    
    let roundEffect = RoundEffect()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
    }
    
    private func setup() {
        self.layer.borderWidth = self.borderWidth
        self.layer.borderColor = self.borderColor.cgColor
        
        self.roundEffect.add(to: self)
    }
    
}
