//
//  PullBarView.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 07/08/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

@IBDesignable
final class PullBarView: UIView {
    
    let roundEffect = RoundEffect()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
    }
    
    private func setup() {
        self.backgroundColor = StyleConstants.PullBarView.backgroundColor
        
        self.roundEffect.radius = StyleConstants.PullBarView.borderRadius
        self.roundEffect.add(to: self)
    }
    
}
