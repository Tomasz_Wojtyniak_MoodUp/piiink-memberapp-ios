//
//  ShadowEndedView.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 22/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

@IBDesignable
final class GradientView: UIView {
    
    @IBInspectable
    var from: UIColor = .clear
    
    @IBInspectable
    var to: UIColor = .clear
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
    }
    
    private func setup() {
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = CGRect(x: self.bounds.minX, y: self.bounds.minY, width: self.bounds.width, height: self.bounds.height)
        gradientLayer.colors = [self.from.cgColor, self.to.cgColor]
        
        self.layer.addSublayer(gradientLayer)
    }

}
