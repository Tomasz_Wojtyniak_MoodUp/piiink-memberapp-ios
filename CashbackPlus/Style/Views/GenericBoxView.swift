//
//  ShadowedBox.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 26/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

@IBDesignable
final class GenericBoxView: UIView {
    
    let roundEffect = RoundEffect()
    let shadowEffect = ShadowEffect()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
    }
    
    private func setup() {
        self.backgroundColor = .white
        
        self.roundEffect.add(to: self)
        self.shadowEffect.add(to: self)
    }
    
}
