//
//  HomeTabBar.swift
//  CashbackPlus
//
//  Created by Maciej Olejnik on 23/07/2019.
//  Copyright © 2019 Maciej Olejnik. All rights reserved.
//

import UIKit

@IBDesignable
final class HomeTabBar: UITabBar {
    
    let shadowEffect = ShadowEffect()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setup()
    }
    
    private func setup() {
        self.setupShadow()
        self.setupItemsColors()
        self.setupFont()
    }
    
    private func setupFont() {
        guard let font = StyleConstants.TabBar.font else { return }
        
        let appearance = UITabBarItem.appearance()
        let attributes = [NSAttributedString.Key.font: font]

        appearance.setTitleTextAttributes(attributes, for: .normal)
        appearance.setTitleTextAttributes(attributes, for: .selected)
    }
    
    private func setupShadow() {
        self.backgroundColor = .white
        self.barTintColor = .white
        self.barStyle = .blackOpaque
        
        self.shadowEffect.offset = CGSize(width: 0, height: -2)
        self.shadowEffect.add(to: self)
    }
    
    private func setupItemsColors() {
        self.unselectedItemTintColor = StyleConstants.TabBar.unselectedColor
        self.tintColor = StyleConstants.TabBar.selectedColor
    }

}
