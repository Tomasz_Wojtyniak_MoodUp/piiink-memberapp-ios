// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let accountCard = ImageAsset(name: "Account_card")
  internal static let appBackgroundColor = ColorAsset(name: "AppBackgroundColor")
  internal static let appTextColor = ColorAsset(name: "AppTextColor")
  internal static let award = ImageAsset(name: "Award")
  internal enum Balloons {
    internal static let balloons0 = ImageAsset(name: "Balloons/balloons-0")
    internal static let balloons1 = ImageAsset(name: "Balloons/balloons-1")
    internal static let balloons10 = ImageAsset(name: "Balloons/balloons-10")
    internal static let balloons100 = ImageAsset(name: "Balloons/balloons-100")
    internal static let balloons101 = ImageAsset(name: "Balloons/balloons-101")
    internal static let balloons102 = ImageAsset(name: "Balloons/balloons-102")
    internal static let balloons103 = ImageAsset(name: "Balloons/balloons-103")
    internal static let balloons104 = ImageAsset(name: "Balloons/balloons-104")
    internal static let balloons105 = ImageAsset(name: "Balloons/balloons-105")
    internal static let balloons106 = ImageAsset(name: "Balloons/balloons-106")
    internal static let balloons107 = ImageAsset(name: "Balloons/balloons-107")
    internal static let balloons108 = ImageAsset(name: "Balloons/balloons-108")
    internal static let balloons109 = ImageAsset(name: "Balloons/balloons-109")
    internal static let balloons11 = ImageAsset(name: "Balloons/balloons-11")
    internal static let balloons110 = ImageAsset(name: "Balloons/balloons-110")
    internal static let balloons111 = ImageAsset(name: "Balloons/balloons-111")
    internal static let balloons112 = ImageAsset(name: "Balloons/balloons-112")
    internal static let balloons113 = ImageAsset(name: "Balloons/balloons-113")
    internal static let balloons114 = ImageAsset(name: "Balloons/balloons-114")
    internal static let balloons115 = ImageAsset(name: "Balloons/balloons-115")
    internal static let balloons116 = ImageAsset(name: "Balloons/balloons-116")
    internal static let balloons117 = ImageAsset(name: "Balloons/balloons-117")
    internal static let balloons118 = ImageAsset(name: "Balloons/balloons-118")
    internal static let balloons119 = ImageAsset(name: "Balloons/balloons-119")
    internal static let balloons12 = ImageAsset(name: "Balloons/balloons-12")
    internal static let balloons120 = ImageAsset(name: "Balloons/balloons-120")
    internal static let balloons121 = ImageAsset(name: "Balloons/balloons-121")
    internal static let balloons122 = ImageAsset(name: "Balloons/balloons-122")
    internal static let balloons123 = ImageAsset(name: "Balloons/balloons-123")
    internal static let balloons124 = ImageAsset(name: "Balloons/balloons-124")
    internal static let balloons125 = ImageAsset(name: "Balloons/balloons-125")
    internal static let balloons126 = ImageAsset(name: "Balloons/balloons-126")
    internal static let balloons127 = ImageAsset(name: "Balloons/balloons-127")
    internal static let balloons128 = ImageAsset(name: "Balloons/balloons-128")
    internal static let balloons129 = ImageAsset(name: "Balloons/balloons-129")
    internal static let balloons13 = ImageAsset(name: "Balloons/balloons-13")
    internal static let balloons130 = ImageAsset(name: "Balloons/balloons-130")
    internal static let balloons131 = ImageAsset(name: "Balloons/balloons-131")
    internal static let balloons132 = ImageAsset(name: "Balloons/balloons-132")
    internal static let balloons133 = ImageAsset(name: "Balloons/balloons-133")
    internal static let balloons14 = ImageAsset(name: "Balloons/balloons-14")
    internal static let balloons15 = ImageAsset(name: "Balloons/balloons-15")
    internal static let balloons16 = ImageAsset(name: "Balloons/balloons-16")
    internal static let balloons17 = ImageAsset(name: "Balloons/balloons-17")
    internal static let balloons18 = ImageAsset(name: "Balloons/balloons-18")
    internal static let balloons19 = ImageAsset(name: "Balloons/balloons-19")
    internal static let balloons2 = ImageAsset(name: "Balloons/balloons-2")
    internal static let balloons20 = ImageAsset(name: "Balloons/balloons-20")
    internal static let balloons21 = ImageAsset(name: "Balloons/balloons-21")
    internal static let balloons22 = ImageAsset(name: "Balloons/balloons-22")
    internal static let balloons23 = ImageAsset(name: "Balloons/balloons-23")
    internal static let balloons24 = ImageAsset(name: "Balloons/balloons-24")
    internal static let balloons25 = ImageAsset(name: "Balloons/balloons-25")
    internal static let balloons26 = ImageAsset(name: "Balloons/balloons-26")
    internal static let balloons27 = ImageAsset(name: "Balloons/balloons-27")
    internal static let balloons28 = ImageAsset(name: "Balloons/balloons-28")
    internal static let balloons29 = ImageAsset(name: "Balloons/balloons-29")
    internal static let balloons3 = ImageAsset(name: "Balloons/balloons-3")
    internal static let balloons30 = ImageAsset(name: "Balloons/balloons-30")
    internal static let balloons31 = ImageAsset(name: "Balloons/balloons-31")
    internal static let balloons32 = ImageAsset(name: "Balloons/balloons-32")
    internal static let balloons33 = ImageAsset(name: "Balloons/balloons-33")
    internal static let balloons34 = ImageAsset(name: "Balloons/balloons-34")
    internal static let balloons35 = ImageAsset(name: "Balloons/balloons-35")
    internal static let balloons36 = ImageAsset(name: "Balloons/balloons-36")
    internal static let balloons37 = ImageAsset(name: "Balloons/balloons-37")
    internal static let balloons38 = ImageAsset(name: "Balloons/balloons-38")
    internal static let balloons39 = ImageAsset(name: "Balloons/balloons-39")
    internal static let balloons4 = ImageAsset(name: "Balloons/balloons-4")
    internal static let balloons40 = ImageAsset(name: "Balloons/balloons-40")
    internal static let balloons41 = ImageAsset(name: "Balloons/balloons-41")
    internal static let balloons42 = ImageAsset(name: "Balloons/balloons-42")
    internal static let balloons43 = ImageAsset(name: "Balloons/balloons-43")
    internal static let balloons44 = ImageAsset(name: "Balloons/balloons-44")
    internal static let balloons45 = ImageAsset(name: "Balloons/balloons-45")
    internal static let balloons46 = ImageAsset(name: "Balloons/balloons-46")
    internal static let balloons47 = ImageAsset(name: "Balloons/balloons-47")
    internal static let balloons48 = ImageAsset(name: "Balloons/balloons-48")
    internal static let balloons49 = ImageAsset(name: "Balloons/balloons-49")
    internal static let balloons5 = ImageAsset(name: "Balloons/balloons-5")
    internal static let balloons50 = ImageAsset(name: "Balloons/balloons-50")
    internal static let balloons51 = ImageAsset(name: "Balloons/balloons-51")
    internal static let balloons52 = ImageAsset(name: "Balloons/balloons-52")
    internal static let balloons53 = ImageAsset(name: "Balloons/balloons-53")
    internal static let balloons54 = ImageAsset(name: "Balloons/balloons-54")
    internal static let balloons55 = ImageAsset(name: "Balloons/balloons-55")
    internal static let balloons56 = ImageAsset(name: "Balloons/balloons-56")
    internal static let balloons57 = ImageAsset(name: "Balloons/balloons-57")
    internal static let balloons58 = ImageAsset(name: "Balloons/balloons-58")
    internal static let balloons59 = ImageAsset(name: "Balloons/balloons-59")
    internal static let balloons6 = ImageAsset(name: "Balloons/balloons-6")
    internal static let balloons60 = ImageAsset(name: "Balloons/balloons-60")
    internal static let balloons61 = ImageAsset(name: "Balloons/balloons-61")
    internal static let balloons62 = ImageAsset(name: "Balloons/balloons-62")
    internal static let balloons63 = ImageAsset(name: "Balloons/balloons-63")
    internal static let balloons64 = ImageAsset(name: "Balloons/balloons-64")
    internal static let balloons65 = ImageAsset(name: "Balloons/balloons-65")
    internal static let balloons66 = ImageAsset(name: "Balloons/balloons-66")
    internal static let balloons67 = ImageAsset(name: "Balloons/balloons-67")
    internal static let balloons68 = ImageAsset(name: "Balloons/balloons-68")
    internal static let balloons69 = ImageAsset(name: "Balloons/balloons-69")
    internal static let balloons7 = ImageAsset(name: "Balloons/balloons-7")
    internal static let balloons70 = ImageAsset(name: "Balloons/balloons-70")
    internal static let balloons71 = ImageAsset(name: "Balloons/balloons-71")
    internal static let balloons72 = ImageAsset(name: "Balloons/balloons-72")
    internal static let balloons73 = ImageAsset(name: "Balloons/balloons-73")
    internal static let balloons74 = ImageAsset(name: "Balloons/balloons-74")
    internal static let balloons75 = ImageAsset(name: "Balloons/balloons-75")
    internal static let balloons76 = ImageAsset(name: "Balloons/balloons-76")
    internal static let balloons77 = ImageAsset(name: "Balloons/balloons-77")
    internal static let balloons78 = ImageAsset(name: "Balloons/balloons-78")
    internal static let balloons79 = ImageAsset(name: "Balloons/balloons-79")
    internal static let balloons8 = ImageAsset(name: "Balloons/balloons-8")
    internal static let balloons80 = ImageAsset(name: "Balloons/balloons-80")
    internal static let balloons81 = ImageAsset(name: "Balloons/balloons-81")
    internal static let balloons82 = ImageAsset(name: "Balloons/balloons-82")
    internal static let balloons83 = ImageAsset(name: "Balloons/balloons-83")
    internal static let balloons84 = ImageAsset(name: "Balloons/balloons-84")
    internal static let balloons85 = ImageAsset(name: "Balloons/balloons-85")
    internal static let balloons86 = ImageAsset(name: "Balloons/balloons-86")
    internal static let balloons87 = ImageAsset(name: "Balloons/balloons-87")
    internal static let balloons88 = ImageAsset(name: "Balloons/balloons-88")
    internal static let balloons89 = ImageAsset(name: "Balloons/balloons-89")
    internal static let balloons9 = ImageAsset(name: "Balloons/balloons-9")
    internal static let balloons90 = ImageAsset(name: "Balloons/balloons-90")
    internal static let balloons91 = ImageAsset(name: "Balloons/balloons-91")
    internal static let balloons92 = ImageAsset(name: "Balloons/balloons-92")
    internal static let balloons93 = ImageAsset(name: "Balloons/balloons-93")
    internal static let balloons94 = ImageAsset(name: "Balloons/balloons-94")
    internal static let balloons95 = ImageAsset(name: "Balloons/balloons-95")
    internal static let balloons96 = ImageAsset(name: "Balloons/balloons-96")
    internal static let balloons97 = ImageAsset(name: "Balloons/balloons-97")
    internal static let balloons98 = ImageAsset(name: "Balloons/balloons-98")
    internal static let balloons99 = ImageAsset(name: "Balloons/balloons-99")
  }
  internal static let bigCardIcon = ImageAsset(name: "BigCardIcon")
  internal static let blueBackgroundColor = ColorAsset(name: "BlueBackgroundColor")
  internal static let blueTextColor = ColorAsset(name: "BlueTextColor")
  internal static let cardBarButton = ImageAsset(name: "CardBarButton")
  internal static let accomodationIcon = ImageAsset(name: "Accomodation_icon")
  internal static let activityIcon = ImageAsset(name: "Activity_icon")
  internal static let allIcon = ImageAsset(name: "All_icon")
  internal static let bakery = ImageAsset(name: "Bakery")
  internal static let butcheryIcon = ImageAsset(name: "Butchery.icon")
  internal static let carIcon = ImageAsset(name: "Car_Icon")
  internal static let clothesIcon = ImageAsset(name: "Clothes_icon")
  internal static let clubsIcon = ImageAsset(name: "Clubs_icon")
  internal static let coffeeIcon = ImageAsset(name: "Coffee_icon")
  internal static let dentistIcon = ImageAsset(name: "Dentist_icon")
  internal static let educationIcon = ImageAsset(name: "Education_icon")
  internal static let electronicsIcon = ImageAsset(name: "Electronics_icon")
  internal static let financeIcon = ImageAsset(name: "Finance_icon")
  internal static let foodIcon = ImageAsset(name: "Food_icon")
  internal static let fruitIcon = ImageAsset(name: "Fruit_icon")
  internal static let hairdresserIcon = ImageAsset(name: "Hairdresser_Icon")
  internal static let hardwareIcon = ImageAsset(name: "Hardware_icon")
  internal static let healthIcon = ImageAsset(name: "Health_icon")
  internal static let hireIcon = ImageAsset(name: "Hire_icon")
  internal static let houseIcon = ImageAsset(name: "House_icon")
  internal static let itIcon = ImageAsset(name: "IT_icon")
  internal static let medicalIcon = ImageAsset(name: "Medical_icon")
  internal static let petsIcon = ImageAsset(name: "Pets_icon")
  internal static let printingIcon = ImageAsset(name: "Printing_icon")
  internal static let publicIcon = ImageAsset(name: "Public_icon")
  internal static let pubsIcon = ImageAsset(name: "Pubs_icon")
  internal static let recreationIcon = ImageAsset(name: "Recreation_icon")
  internal static let restaurantIcon = ImageAsset(name: "Restaurant_icon")
  internal static let retailIcon = ImageAsset(name: "Retail_icon")
  internal static let servicesIcon = ImageAsset(name: "Services_icon")
  internal static let signIcon = ImageAsset(name: "Sign_icon")
  internal static let sportIcon = ImageAsset(name: "Sport_icon")
  internal static let takeawayIcon = ImageAsset(name: "Takeaway_icon")
  internal static let tradiesIcon = ImageAsset(name: "Tradies_icon")
  internal static let change = ImageAsset(name: "Change")
  internal static let charityIcon = ImageAsset(name: "Charity_icon")
  internal static let charityIcon2 = ImageAsset(name: "Charity_icon_2")
  internal static let checkmarkEmptyIcon = ImageAsset(name: "Checkmark_empty_icon")
  internal static let checkmarkIcon = ImageAsset(name: "Checkmark_icon")
  internal static let chevronLeft = ImageAsset(name: "ChevronLeft")
  internal static let chevronRight = ImageAsset(name: "ChevronRight")
  internal static let confettiBackground = ImageAsset(name: "ConfettiBackground")
  internal static let creditCard = ImageAsset(name: "Credit-card")
  internal static let discount = ImageAsset(name: "Discount")
  internal static let email = ImageAsset(name: "Email")
  internal static let errorColor = ColorAsset(name: "ErrorColor")
  internal static let familyIcon = ImageAsset(name: "Family_icon")
  internal static let geotagIcon = ImageAsset(name: "Geotag_icon")
  internal static let giftIcon = ImageAsset(name: "Gift_icon")
  internal static let giftIconNavigation = ImageAsset(name: "Gift_icon_navigation")
  internal static let heartGift = ImageAsset(name: "HeartGift")
  internal static let hide = ImageAsset(name: "Hide")
  internal static let home = ImageAsset(name: "Home")
  internal static let homeBarIcon = ImageAsset(name: "HomeBarIcon")
  internal static let infoButton = ImageAsset(name: "InfoButton")
  internal static let infoButtonBlue = ImageAsset(name: "InfoButtonBlue")
  internal static let locationIcon = ImageAsset(name: "Location_icon")
  internal static let lockIcon = ImageAsset(name: "Lock-icon")
  internal static let logo = ImageAsset(name: "Logo")
  internal static let mail = ImageAsset(name: "Mail")
  internal static let map = ImageAsset(name: "Map")
  internal static let negativeVote = ImageAsset(name: "NegativeVote")
  internal static let phone = ImageAsset(name: "Phone")
  internal static let placeholder = ImageAsset(name: "Placeholder")
  internal static let positiveVote = ImageAsset(name: "PositiveVote")
  internal static let puzzle1 = ImageAsset(name: "Puzzle-1")
  internal static let puzzle = ImageAsset(name: "Puzzle")
  internal static let qrCode = ImageAsset(name: "QR code ")
  internal static let qrCodeIllu = ImageAsset(name: "QR code illu ")
  internal static let qrCodeInCircle = ImageAsset(name: "QR code in circle ")
  internal static let qrCodeIcon = ImageAsset(name: "QR_code_icon")
  internal static let researchIcon = ImageAsset(name: "ResearchIcon")
  internal static let searchIcon = ImageAsset(name: "SearchIcon")
  internal static let settings = ImageAsset(name: "Settings")
  internal static let shoppingIcon = ImageAsset(name: "Shopping_icon")
  internal static let show = ImageAsset(name: "Show")
  internal static let star = ImageAsset(name: "Star")
  internal static let statusIconCorrect = ImageAsset(name: "StatusIconCorrect")
  internal static let statusIconWrong = ImageAsset(name: "StatusIconWrong")
  internal static let tagDollar = ImageAsset(name: "TagDollar")
  internal static let tagRounded = ImageAsset(name: "TagRounded")
  internal static let universalPoints = ImageAsset(name: "UniversalPoints")
  internal static let userBarIcon = ImageAsset(name: "UserBarIcon")
  internal static let wasteBin = ImageAsset(name: "WasteBin")
  internal static let world = ImageAsset(name: "World")
  internal static let balloons = ImageAsset(name: "balloons")
  internal static let confetti = ImageAsset(name: "confetti")
  internal static let heartAndDollarIconInCircle = ImageAsset(name: "heart and dollar icon in circle")
  internal static let heartAndDollarIcon = ImageAsset(name: "heart and dollar icon")
  internal static let heartFill = ImageAsset(name: "heart fill ")
  internal static let heartStroke = ImageAsset(name: "heart stroke ")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = Color(asset: self)

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image named \(name).")
    }
    return result
  }
}

internal extension ImageAsset.Image {
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    Bundle(for: BundleToken.self)
  }()
}
// swiftlint:enable convenience_type
