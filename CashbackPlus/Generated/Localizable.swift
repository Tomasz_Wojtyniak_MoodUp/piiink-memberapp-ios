// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  /// \n@hIntroduction@h\n\nThis Privacy Statement explains in general terms how “ThankYou Club” collects, holds, uses and discloses your personal information under the Australian Privacy Principles.\n\nWe may amend this Privacy Statement as our business requirements or the law changes.  Any changes to this Privacy Statement will be updated on www.thankyouclub.org.\n\n\n@hInformation We Collect@h\n\nThe types of personal information that we collect may include:\n\n• Your name, date of birth, contact details – address, email, contact phone number.\n• Information regarding transactions made while using your “ThankYou Club” Membership with participating merchants.\n\n\n@hHow We Collect Information@h\n\nWe collect personal information about you when you register your details to activate your “ThankYou Club” Membership, enter into one of our competitions, visit our websites, complete one of our surveys or otherwise interact with us.  As well as collecting information directly from you, there may be occasions when we collect information about you from a third party, namely “ThankYou Club” participating merchants.\n\n@hHow We Use Your Personal Information@h\n\nWe may use your personal information for the following purposes:\n\n• To provide and administer our goods and services, including verifying your identity;\n• To provide and operate our competitions, promotions and events;\n• To distribute any newsletters and other communications either ourselves or with the assistance of third party services providers;\n• For customer support or responding to other enquiries or requests;\n• To conduct marketing activities for our products and services, or products and services of third parties, and to conduct market and other research to improve our products, services and marketing activities; and\n• To maintain records and comply with our legal obligations.\n\n\n@hDisclosure of Personal Information@h\n\nWe may also disclose your personal information to our suppliers and third parties that perform other services for us in connection with the sale and provision of all our goods and services, including third parties who:\n\n• Provide customer service;\n• Conduct market research and analysis;\n• Provide marketing services;\n• Provide services in the course of investigating a complaint;\n• To comply with our legal obligations.\n\n\n@hHow We Use Cookies@h\n\nCookies are small pieces of data stored on the web browser on your computer. This website and associated websites (including those you reach by clicking on advertising) may store cookies on your web browser.\n\nThe main reasons we store cookies are to:\n\n• Improve your website browsing experience. For example, to remember your search criteria so you don’t have to re-enter them to do multiple searches and to remember your site preferences for your next visit to the site;\n• Gather statistics on site usage. For example, page views and drop off points so we can monitor site usage and make improvements to site usability; and\n• Enable us to present customised and appropriate messages from “ThankYou Club” and third parties.\n\nWhen you are logged into your account, the information we collect via cookies may be linked with your personal information.\n\nYou can set up most web browsers so you are notified of when a cookie is received, so you can then either accept or reject it.  You can also check the cookies stored by your web browser and remove any that you do not want.\n\nIf you disable the use of cookies on your web browser or remove or reject specific cookies from this website or linked sites, then you may not be able to gain access to all the content and facilities of our website or linked sites.\n\n\n@hHow We Hold Information and Keep it Secure@h\n\nWe hold your personal information on electronic files stored on a secure server.\n\nSecure Sockets Layer (SSL) encrypts the information you send through this website.\n\n\n@hAccess & Correction of Information About You@h\n\nYou have access to your personal information we hold about you and you are able to update your profile information by logging into your account on www.thankyouclub.org\n\n\n@hPrivacy Complaints and Further Information@h\n\nIf you have a concern about your privacy or you have any query on how your personal information is collected or used please contact us using the details below. We will respond to your query or complaint within a reasonable time.\n\nIf you are not satisfied with our response, you may also contact the Office of the Australian Information Commissioner.\n
  internal static let license = L10n.tr("Localizable", "license")

  internal enum AddCard {
    internal enum Message {
      /// Your default pin is 888 888. Please enter it and then change it at a nearby ThankYou Club merchant.
      internal static let tip = L10n.tr("Localizable", "add_card.message.tip")
      internal enum Error {
        /// Sorry, forms are completed incorrectly. Please check both fields and try again.
        internal static let empty = L10n.tr("Localizable", "add_card.message.error.empty")
        /// Sorry, something went wrong. Please check both text fields and try again.
        internal static let generic = L10n.tr("Localizable", "add_card.message.error.generic")
      }
    }
  }

  internal enum Card {
    internal enum Registration {
      /// Continue
      internal static let errorDefaultAction = L10n.tr("Localizable", "card.registration.error_default_action")
      /// Sorry, but Membership registration process has failed. Try again later or contact our support.
      internal static let errorMsg = L10n.tr("Localizable", "card.registration.error_msg")
      /// Membership registration failed
      internal static let errorTitle = L10n.tr("Localizable", "card.registration.error_title")
      internal enum Result {
        internal enum Button {
          /// Continue
          internal static let success = L10n.tr("Localizable", "card.registration.result.button.success")
        }
        internal enum Description {
          /// Your Membership has been registered successfully. You can now choose the charity you want to support.
          internal static let success = L10n.tr("Localizable", "card.registration.result.description.success")
        }
        internal enum Header {
          /// Congratulations
          internal static let success = L10n.tr("Localizable", "card.registration.result.header.success")
        }
      }
      internal enum Web {
        /// Cancel
        internal static let alertCancelAction = L10n.tr("Localizable", "card.registration.web.alert_cancel_action")
        /// Confirm
        internal static let alertConfirmAction = L10n.tr("Localizable", "card.registration.web.alert_confirm_action")
        /// Continue
        internal static let alertDefaultAction = L10n.tr("Localizable", "card.registration.web.alert_default_action")
        /// Registration
        internal static let alertTitle = L10n.tr("Localizable", "card.registration.web.alert_title")
      }
    }
    internal enum Scanner {
      /// Cancel
      internal static let errorButtonCancel = L10n.tr("Localizable", "card.scanner.error_button_cancel")
      /// Settings
      internal static let errorButtonSettings = L10n.tr("Localizable", "card.scanner.error_button_settings")
      /// Sorry, but QR Code Scanner needs video camera access. You can add it manually in Application Settings.
      internal static let errorMsg = L10n.tr("Localizable", "card.scanner.error_msg")
      /// QR Code Scanner
      internal static let errorTitle = L10n.tr("Localizable", "card.scanner.error_title")
    }
    internal enum Settings {
      /// Cancel
      internal static let cancel = L10n.tr("Localizable", "card.settings.cancel")
      /// Update Membership Details
      internal static let changeCard = L10n.tr("Localizable", "card.settings.change_card")
      /// Hide Membership Credentials
      internal static let hideCardCredentails = L10n.tr("Localizable", "card.settings.hide_card_credentails")
      /// Remove Membership
      internal static let removeCard = L10n.tr("Localizable", "card.settings.remove_card")
      /// Show Membership Credentials
      internal static let showCardCredentails = L10n.tr("Localizable", "card.settings.show_card_credentails")
      internal enum RemoveCard {
        internal enum Alert {
          /// Are you sure you want to delete this Membership?
          internal static let desc = L10n.tr("Localizable", "card.settings.remove_card.alert.desc")
          /// Remove Membership
          internal static let title = L10n.tr("Localizable", "card.settings.remove_card.alert.title")
        }
      }
    }
  }

  internal enum CategoryCollection {
    /// All
    internal static let all = L10n.tr("Localizable", "category_collection.all")
  }

  internal enum Charity {
    internal enum Slide {
      internal enum Donate {
        /// Part of our mission is to “<b>perpetually contribute back to society</b> and make a difference to the lives of others.”\n\nThe charities nominated and the level of contributions made to each, is determined by you and the merchants you support.\n
        internal static let description = L10n.tr("Localizable", "charity.slide.donate.description")
        /// Donate TY Cash
        internal static let title = L10n.tr("Localizable", "charity.slide.donate.title")
      }
      internal enum Family {
        /// A percentage of TY Cash will be donated to the charity of your choice to make life easier for those that need it most.\n\nYou can see our list of charities and how much you are donating to these worthy causes by <a href="https://thankyouclub.org">visiting our website</a> and logging into your account.\n
        internal static let description = L10n.tr("Localizable", "charity.slide.family.description")
        /// Help Local Charities
        internal static let title = L10n.tr("Localizable", "charity.slide.family.title")
      }
      internal enum Register {
        /// Without registration you can not choose the charity you would like to support. Registration is a piece of cake and only takes 30 seconds.
        internal static let description = L10n.tr("Localizable", "charity.slide.register.description")
        /// Register Membership
        internal static let title = L10n.tr("Localizable", "charity.slide.register.title")
      }
    }
  }

  internal enum Common {
    internal enum Action {
      /// Cancel
      internal static let cancel = L10n.tr("Localizable", "common.action.cancel")
      /// No
      internal static let no = L10n.tr("Localizable", "common.action.no")
      /// Ok
      internal static let ok = L10n.tr("Localizable", "common.action.ok")
      /// Yes
      internal static let yes = L10n.tr("Localizable", "common.action.yes")
    }
  }

  internal enum Localization {
    internal enum Table {
      /// Your Location: %@
      internal static func header(_ p1: Any) -> String {
        return L10n.tr("Localizable", "localization.table.header", String(describing: p1))
      }
    }
  }

  internal enum MerchantDetails {
    /// Additional Information
    internal static let additionalInfo = L10n.tr("Localizable", "merchantDetails.additionalInfo")
    /// Contact
    internal static let contact = L10n.tr("Localizable", "merchantDetails.contact")
    /// More Offers
    internal static let moreOffers = L10n.tr("Localizable", "merchantDetails.moreOffers")
    /// No savings available
    internal static let noDiscount = L10n.tr("Localizable", "merchantDetails.noDiscount")
  }

  internal enum Merchants {
    /// Best Offers
    internal static let bestOffers = L10n.tr("Localizable", "merchants.bestOffers")
    /// Nearby Your Location
    internal static let nearBy = L10n.tr("Localizable", "merchants.nearBy")
    /// New Merchants
    internal static let newMerchants = L10n.tr("Localizable", "merchants.newMerchants")
    /// Places available for a high quality merchant in this category.\nRecommendations welcome <a>here!</a>
    internal static let noMerchants = L10n.tr("Localizable", "merchants.noMerchants")
    /// Other Merchants
    internal static let otherMerchants = L10n.tr("Localizable", "merchants.otherMerchants")
    internal enum NearBy {
      /// Enable Access
      internal static let enableButton = L10n.tr("Localizable", "merchants.nearBy.enableButton")
      /// We want to set your actual location to show you the best offers nearby.
      internal static let enableDescription = L10n.tr("Localizable", "merchants.nearBy.enableDescription")
    }
  }

  internal enum Modal {
    internal enum MemberInfoPoints {
      /// You can use your TY Cash and get a savings on another purchase.\n\n1 TYC CASH = 1 AUD\n\nUniversal TY Cash can be spent at any Merchant. Other TY Cash can be spent only at Merchant’s where they were earned. 
      internal static let description = L10n.tr("Localizable", "modal.member_info_points.description")
    }
  }

  internal enum MoreOffers {
    /// Friday
    internal static let friday = L10n.tr("Localizable", "moreOffers.friday")
    /// More Offers
    internal static let header = L10n.tr("Localizable", "moreOffers.header")
    /// Monday
    internal static let monday = L10n.tr("Localizable", "moreOffers.monday")
    /// Saturday
    internal static let saturday = L10n.tr("Localizable", "moreOffers.saturday")
    /// Sunday
    internal static let sunday = L10n.tr("Localizable", "moreOffers.sunday")
    /// Thursday
    internal static let thursday = L10n.tr("Localizable", "moreOffers.thursday")
    /// Merchant Offer
    internal static let title = L10n.tr("Localizable", "moreOffers.title")
    /// Tuesday
    internal static let tuesday = L10n.tr("Localizable", "moreOffers.tuesday")
    /// Wednesday
    internal static let wednesday = L10n.tr("Localizable", "moreOffers.wednesday")
  }

  internal enum Offerbox {
    /// up to %@%%
    internal static func discountText(_ p1: Any) -> String {
      return L10n.tr("Localizable", "offerbox.discountText", String(describing: p1))
    }
  }

  internal enum PrivacyPolicy {
    /// Privacy Policy
    internal static let title = L10n.tr("Localizable", "privacy_policy.title")
    /// Version: %@
    internal static func version(_ p1: Any) -> String {
      return L10n.tr("Localizable", "privacy_policy.version", String(describing: p1))
    }
  }

  internal enum PrizeInfo {
    /// Every time you support our merchants with your Thank You Club membership you are entered to win fantastic prizes.\n\nThe more transactions, the more chances to win!\nIt’s our way of saying “Thank You”!
    internal static let desc = L10n.tr("Localizable", "prizeInfo.desc")
    /// Prize Info
    internal static let title = L10n.tr("Localizable", "prizeInfo.title")
  }

  internal enum Profile {
    /// Profile
    internal static let title = L10n.tr("Localizable", "profile.title")
    internal enum Merchant {
      /// %@ TYC
      internal static func points(_ p1: Any) -> String {
        return L10n.tr("Localizable", "profile.merchant.points", String(describing: p1))
      }
      /// Universal TY Cash
      internal static let universalPoints = L10n.tr("Localizable", "profile.merchant.universal_points")
    }
    internal enum Options {
      /// Charity
      internal static let charity = L10n.tr("Localizable", "profile.options.charity")
      /// Libraries
      internal static let libraries = L10n.tr("Localizable", "profile.options.libraries")
      /// Privacy Policy
      internal static let privacyPolicy = L10n.tr("Localizable", "profile.options.privacyPolicy")
      /// Recommend New Merchant
      internal static let recommend = L10n.tr("Localizable", "profile.options.recommend")
    }
    internal enum Points {
      internal enum Unknown {
        /// Unknown
        internal static let merchant = L10n.tr("Localizable", "profile.points.unknown.merchant")
      }
    }
    internal enum PointsInfo {
      internal enum Description {
        /// You haven't added TY Membership yet. Add your TYC membership to login and see your TY Cash balances.
        internal static let noLogged = L10n.tr("Localizable", "profile.points_info.description.noLogged")
        /// Make your first transaction to see your TY Cash balance here.
        internal static let noPoints = L10n.tr("Localizable", "profile.points_info.description.noPoints")
      }
    }
  }

  internal enum Recommend {
    internal enum BackAlert {
      /// Your changes will be discarded.
      internal static let description = L10n.tr("Localizable", "recommend.back_alert.description")
      /// Are you sure you want to leave?
      internal static let title = L10n.tr("Localizable", "recommend.back_alert.title")
    }
    internal enum Email {
      /// \nHello,<br/>\nI am sending merchant’s details to join ThankYouClub.<br/>\n<br/>\nMerchants details:<br/>\n<br/>\nName:<br/>\n<b>%1$@</b><br/>\nType:<br/>\n<b>%2$@</b><br/>\nAddress:<br/>\n<b>%3$@</b><br/>\nName of contact person:<br/>\n<b>%4$@</b><br/>\nPhone:<br/>\n<b>%5$@</b><br/>\nEmail:<br/>\n<b>%6$@</b><br/>\n<br/>\nMember Number:<br/>\n<b>%7$@</b><br/>\n
      internal static func body(_ p1: Any, _ p2: Any, _ p3: Any, _ p4: Any, _ p5: Any, _ p6: Any, _ p7: Any) -> String {
        return L10n.tr("Localizable", "recommend.email.body", String(describing: p1), String(describing: p2), String(describing: p3), String(describing: p4), String(describing: p5), String(describing: p6), String(describing: p7))
      }
      /// Merchant has been nominated
      internal static let subject = L10n.tr("Localizable", "recommend.email.subject")
    }
    internal enum Form {
      /// Inncorrect value
      internal static let errorInvalidField = L10n.tr("Localizable", "recommend.form.error_invalid_field")
    }
    internal enum MailFail {
      /// Mail service is unavailable.
      internal static let description = L10n.tr("Localizable", "recommend.mail_fail.description")
      /// Unable to send mail
      internal static let title = L10n.tr("Localizable", "recommend.mail_fail.title")
    }
  }

  internal enum Regions {
    /// Choose region from the list below to filter offers and Merchants.
    internal static let header = L10n.tr("Localizable", "regions.header")
    /// Current Region:
    internal static let `prefix` = L10n.tr("Localizable", "regions.prefix")
    /// Region
    internal static let title = L10n.tr("Localizable", "regions.title")
  }

  internal enum Search {
    /// Back to Home
    internal static let back = L10n.tr("Localizable", "search.back")
    /// Search for merchants
    internal static let hint = L10n.tr("Localizable", "search.hint")
    /// We’re sorry, we could not find results for “%@” - check the spelling and try typing again.
    internal static func notFound(_ p1: Any) -> String {
      return L10n.tr("Localizable", "search.notFound", String(describing: p1))
    }
    /// Search Results
    internal static let resultsHeader = L10n.tr("Localizable", "search.resultsHeader")
  }

  internal enum Splash {
    internal enum Charity {
      /// Every time you shop our merchants will donate to a charity of your choice
      internal static let description = L10n.tr("Localizable", "splash.charity.description")
      /// Donate to charity
      internal static let title = L10n.tr("Localizable", "splash.charity.title")
    }
    internal enum Shoping {
      /// Shop at ThankYou Club merchants displaying the @b“ThankYou Club welcome here”@b sign.
      internal static let description = L10n.tr("Localizable", "splash.shoping.description")
      /// Go shoping
      internal static let title = L10n.tr("Localizable", "splash.shoping.title")
    }
    internal enum Welcome {
      /// The most innovative and society friendly community program
      internal static let description = L10n.tr("Localizable", "splash.welcome.description")
      /// Welcome!
      internal static let title = L10n.tr("Localizable", "splash.welcome.title")
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle = Bundle(for: BundleToken.self)
}
// swiftlint:enable convenience_type
