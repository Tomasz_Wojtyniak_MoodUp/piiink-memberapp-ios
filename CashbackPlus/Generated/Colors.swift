// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSColor
  internal typealias Color = NSColor
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  internal typealias Color = UIColor
#endif

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Colors

// swiftlint:disable identifier_name line_length type_body_length
internal struct ColorName {
  internal let rgbaValue: UInt32
  internal var color: Color { return Color(named: self) }

  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#f6f6f6"></span>
  /// Alpha: 100% <br/> (0xf6f6f6ff)
  internal static let appBackground = ColorName(rgbaValue: 0xf6f6f6ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#3b3b3b"></span>
  /// Alpha: 100% <br/> (0x3b3b3bff)
  internal static let defaultTextColor = ColorName(rgbaValue: 0x3b3b3bff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#f04929"></span>
  /// Alpha: 100% <br/> (0xf04929ff)
  internal static let errorTextColor = ColorName(rgbaValue: 0xf04929ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#3c8bd5"></span>
  /// Alpha: 100% <br/> (0x3c8bd5ff)
  internal static let globalTint = ColorName(rgbaValue: 0x3c8bd5ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#885fa8"></span>
  /// Alpha: 100% <br/> (0x885fa8ff)
  internal static let splashPageControlActive = ColorName(rgbaValue: 0x885fa8ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#000000"></span>
  /// Alpha: 30% <br/> (0x0000004d)
  internal static let splashPageControlUnactive = ColorName(rgbaValue: 0x0000004d)
}
// swiftlint:enable identifier_name line_length type_body_length

// MARK: - Implementation Details

internal extension Color {
  convenience init(rgbaValue: UInt32) {
    let components = RGBAComponents(rgbaValue: rgbaValue).normalized
    self.init(red: components[0], green: components[1], blue: components[2], alpha: components[3])
  }
}

private struct RGBAComponents {
  let rgbaValue: UInt32

  private var shifts: [UInt32] {
    [
      rgbaValue >> 24, // red
      rgbaValue >> 16, // green
      rgbaValue >> 8,  // blue
      rgbaValue        // alpha
    ]
  }

  private var components: [CGFloat] {
    shifts.map {
      CGFloat($0 & 0xff)
    }
  }

  var normalized: [CGFloat] {
    components.map { $0 / 255.0 }
  }
}

internal extension Color {
  convenience init(named color: ColorName) {
    self.init(rgbaValue: color.rgbaValue)
  }
}
