module ProjectInfo
    WORKSPACE_NAME = "CashbackPlus.xcworkspace"
    XCODEPROJ_NAME = "CashbackPlus.xcodeproj"
    MATCH_BRANCH = "cashbackplus"
    APP_ICON_PATH = "CashbackPlus/SupportingFiles/Assets.xcassets/AppIcon.appiconset"
    TEAM_ID = "5G89KSC622"
  end
  
  module Crashlytics
    API_TOKEN = "AIzaSyBFmmYPTeJqYX4sIRsxYfezAX_PtfiiL00"
  end
  
  module ExportMethod
    AD_HOC = "ad-hoc"
    APP_STORE = "app-store"
  end
  
  module Environment
    BuildEnvironment = Struct.new(:name, :target, :app_identifier, :ipa_file)
  
    class BuildEnvironment
      def provisioning_profile_name(export_method, identifier)
        profile_type = ""
  
        case export_method
        when ExportMethod::AD_HOC
          profile_type = "AdHoc"
        when ExportMethod::APP_STORE
          profile_type = "AppStore"
        end
  
        return "match #{profile_type} #{identifier}"
      end
  
      def export_options(export_method)
        return {
          method: export_method,
          provisioningProfiles: {
            "#{app_identifier}" => provisioning_profile_name(export_method, app_identifier),
            "#{app_identifier}.NotificationServiceExtension" => provisioning_profile_name(export_method, "#{app_identifier}.NotificationServiceExtension")
          }
        }
      end
  
      def certificate_type(export_method)
        case export_method
        when ExportMethod::AD_HOC
          return "adhoc"
        when ExportMethod::APP_STORE
          return "appstore"
        else
          return "development"
        end
      end
    end
    
    PRODUCTION = BuildEnvironment.new(
      "prod",
      "CashbackPlus",
      "com.moodup.team.CashbackPlus",
      "CashbackPlus.ipa"
    )
    DEVELOPMENT = BuildEnvironment.new(
      "dev",
      "CashbackPlus Development",
      "com.moodup.team.CashbackPlus-Development",
      "CashbackPlus Development.ipa"
    )
  
    def self.fromName(name)
      case name
      when Environment::PRODUCTION.name
        return PRODUCTION
      when Environment::DEVELOPMENT.name
        return DEVELOPMENT
      else
        return DEVELOPMENT
      end
    end
  end
  
  
  
